# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

from lib.Log import Log
from lib.ToolDockWidget import ToolDockWidget
#endregion Modules

# Module Info
name = "Test Input Widgets"
description = "Testing automatic save/restore of input widget content."
version = 1.0
tags = [
    "input",
    "widgets",
    "save",
    "restore",
]

# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class TestInputWidgets(ToolDockWidget):
    """
    Test module for the various input widgets, and automatically saving/restoring their values.
    """

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        """
        __init__ [summary]

        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
        """
        self.help_url = "http://intranet/dept/arttools/{}".format(self.__class__.__name__)
        super(TestInputWidgets, self).__init__(parent=parent, objectName=objectName)
        output_text = []
        output_text.append("")

        # Load UI
        # self.setObjectName(objectName)
        # self.setWindowTitle(objectName)
        self.load_ui(__file__)
        self.setWidget(self.widget)

        # Connect events
        if self.widget:
            pass

        # Restore any stored settings
        self.ReadSettings()

        if verbose:
            Log(output_text)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #

    #endregion UI Events

    # ----------------------------------------------------------------------------------------------------------------------
    #region Functions
    #

    #endregion Functions

#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    widget = TestInputWidgets(parent=parent, objectName=name)
    return widget
#endregion Core Widget Functions
