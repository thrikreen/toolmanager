"""Manage Modules"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import importlib

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

import lib.settings as settings
from lib.Log import Log
from lib.ToolDockWidget import ToolDockWidget
#endregion Modules

# Module Info
name = "Manage Modules"
description = "Manage active tool modules and search for more tool modules to add."
version = 1.1
tags = [
    "search", "find",
    "manage",
    "module", "modules",
    "update"
]

# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class ManageModules(ToolDockWidget):
    """
    Manages the modules that the tool manager loads, or removing them from being loaded.
    Also searching for a tool module from the module folder.

    Args:
        parent ([type], optional): [description]. Defaults to None.
        objectName ([type], optional): [description]. Defaults to None.
        verbose (bool, optional): [description]. Defaults to True.

    """

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        output_text = []
        self.help_url = "http://intranet/dept/arttools/{}".format(self.__class__.__name__)
        if sys.version_info[0] == 2:
            super(ManageModules, self).__init__(parent=parent, objectName=objectName)
        else:
            super().__init__(parent=parent, objectName=objectName)

        # Load UI
        # self.setObjectName(objectName)
        # self.setWindowTitle(objectName)
        self.load_ui(__file__)
        self.setWidget(self.widget)

        # Adjust the table properties
        style = "::section { background-color: #dadada; }"
        self.widget.table_active_modules.horizontalHeader().setStyleSheet(style)
        header = self.widget.table_active_modules.horizontalHeader()
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

        self.widget.table_found_modules.horizontalHeader().setStyleSheet(style)
        header = self.widget.table_found_modules.horizontalHeader()
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

        # Connect events
        if self.widget:
            self.widget.button_refresh.clicked.connect(self.PopulateActiveModules)
            self.widget.button_search.clicked.connect(self.on_search)
            self.widget.button_clear.clicked.connect(self.on_clear)
            # self.widget.lineedit_search.textChanged.connect(self.on_text_changed)
            self.widget.lineedit_search.returnPressed.connect(self.on_search)

            # Item Clicked
            # self.widget.table_found_modules.itemClicked.connect(self.on_item_clicked)
            self.widget.table_found_modules.itemDoubleClicked.connect(self.on_item_doubleclicked)

            # Context Menu on items
            self.widget.table_active_modules.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.widget.table_active_modules.customContextMenuRequested.connect(self.menuContextActiveModules)
            self.widget.table_found_modules.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.widget.table_found_modules.customContextMenuRequested.connect(self.menuContextFoundModules)

        # Restore any stored settings
        self.ReadSettings()

        if verbose:
            Log(output_text)
    #endregion Init

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI
    #
    def on_search(self):
        """Button on Search, passes the contents of the search text to the function."""
        text = self.widget.lineedit_search.text()
        self.SearchModules(text)

    def on_clear(self):
        """Clears the contents of the search field."""
        self.widget.lineedit_search.setText("")

    def on_text_changed(self):
        """When the text of the search field has been modified."""
        # output_text = []
        # text = self.widget.lineedit_search.text()
        # output_text.append(text)
        # Log(output_text)
        pass #pylint: disable=unnecessary-pass

    def on_item_clicked(self):
        """Item in the table has been clicked on."""
        output_text = []
        Log(output_text)

    def on_item_doubleclicked(self, item):
        """Item in the table has been double clicked on."""
        output_text = []

        table = self.widget.table_active_modules
        row = table.currentRow()
        col = table.currentColumn()

        if hasattr(item, "module_path"):
            output_text.append("Item.module_path: {}".format(item.module_path))

            # Check if it is already loaded
            found = False
            for window in self.parent().GetWindows():
                for w in window.widgets:
                    if w.GetModulePath() == item.module_path:
                        found = True
                        break

            # No, load it
            if not found:
                # get the dock area of this widget and dock it over this one
                dockarea = self.parent().dockWidgetArea(self)
                output_text.append("Widget Dock Area: {}".format(dockarea))
                self.parent().AddToolWidget(item.module_path, dockarea=dockarea, dockwidget=self)

                self.PopulateActiveModules()
            else:
                output_text.append("Module already loaded, skipping.")

        Log(output_text, item.text(), row, col)
    #endregion UI

    # ----------------------------------------------------------------------------------------------------------------------
    #region Functions
    #
    def PopulateActiveModules(self, verbose=False):
        """
        Populates the Active Modules table with the currently loaded tool modules.
        """
        output_text = []

        table = self.widget.table_active_modules
        table.clearContents()
        table.setRowCount(0)
        table.setSortingEnabled(False)

        # Get the master ToolDockWindow
        parent = self.parent().GetMasterParent()
        output_text.append("- Master Window: {}".format(parent.objectName()))

        # Get the windows
        windows = parent.GetWindows()
        for w in windows:
            output_text.append("- {}".format(w.objectName()))

            # Process the active tool widgets
            widgets = w.GetWidgets()
            for widget in widgets:
                module_path = widget.GetModulePath() + ".py"
                abs_module_filepath = settings.sanitize_path(os.path.join(settings.MODULE_PATH, module_path))
                output_text.append("\t- {}: {} -> {}".format(widget.objectName(), module_path, abs_module_filepath))
                module = settings.LoadModule(abs_module_filepath)

                # Create the QTableWidgetItem
                if module:
                    row = table.rowCount()
                    table.setRowCount(row+1)
                    c = 0

                    if hasattr(module, "name"):
                        item = QtWidgets.QTableWidgetItem(module.name)
                        item.setToolTip(module.name)
                        item.widget = widget
                        table.setItem(row, c, item)

                        item = QtWidgets.QTableWidgetItem(module.description)
                        item.setToolTip(module.description)
                        item.widget = widget
                        table.setItem(row, c+1, item)

                        item = QtWidgets.QTableWidgetItem(str(module.version))
                        item.setToolTip(str(module.version))
                        item.widget = widget
                        table.setItem(row, c+2, item)

                    elif hasattr(module, "__name__"):
                        item = QtWidgets.QTableWidgetItem(module.__name__)
                        item.setToolTip(module.__name__)
                        item.widget = widget
                        table.setItem(row, c, item)

                        item = QtWidgets.QTableWidgetItem(module.__description__)
                        item.setToolTip(module.__description__)
                        item.widget = widget
                        table.setItem(row, c+1, item)

                        item = QtWidgets.QTableWidgetItem(str(module.__version__))
                        item.setToolTip(str(module.__version__))
                        item.widget = widget
                        table.setItem(row, c+2, item)



                    item = QtWidgets.QTableWidgetItem(w.objectName())
                    item.setToolTip(w.objectName())
                    item.widget = widget
                    table.setItem(row, c+3, item)

        # Update the table
        table.setSortingEnabled(True)
        table.resizeColumnToContents(0) # Name
        # table.resizeColumnToContents(1) # Desc
        table.resizeColumnToContents(2) # Version
        table.resizeColumnToContents(3) # Parent Window

        if verbose:
            Log(output_text)

    def SearchModules(self, search_terms, verbose=False):
        """
        Searches the set modules path for modules that have tags that match the search terms.

        Args:
            search_terms (list): List of string search terms to look for. Need to add fuzzy logic to the search
        """
        output_text = []

        # Clear the table
        table = self.widget.table_found_modules
        table.clearContents()
        table.setRowCount(0)
        table.setSortingEnabled(False)

        # Split the search terms into a list for processing
        search_tags = search_terms.lower().split(" ")
        search_tags = [x for x in search_tags if x] # filter out empty elements

        # Get all the modules in the module path
        module_paths = settings.GetModulePaths()
        output_text.append("- Found {} module paths:".format(len(module_paths)))

        for module_path in module_paths:
            module_name = settings.GetModuleName(module_path)
            output_text.append("- {} -> {}".format(module_path, module_name))
            module = settings.LoadModule(module_path)

            # Load the module and see if its search tags have any matches
            if module and (hasattr(module, "__tags__") or hasattr(module, "tags")):
                display_module = False if search_tags else True # init displaying the module to False if search_tags has values, True if blank

                module_tags = getattr(module, "__tags__") if hasattr(module, "__tags__") else getattr(module, "tags")
                output_text.append("\t- Module Tags: {}".format(module_tags))

                # See if and module tags match
                # To Do: convert to support partial matches
                for tag in search_tags:
                    if tag in module_tags:
                        display_module = True
                        break

                # Add to the list
                if display_module:
                    row = table.rowCount()
                    table.setRowCount(row+1)
                    c = 0

                    if hasattr(module, "name"):
                        item = QtWidgets.QTableWidgetItem(module.name)
                        item.setToolTip(module.name)
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c, item)

                        item = QtWidgets.QTableWidgetItem(module.description)
                        item.setToolTip(module.description)
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c+1, item)

                        item = QtWidgets.QTableWidgetItem(str(module.version))
                        item.setToolTip(str(module.version))
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c+2, item)
                    else:
                        item = QtWidgets.QTableWidgetItem(module.__name__)
                        item.setToolTip(module.__name__)
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c, item)

                        item = QtWidgets.QTableWidgetItem(module.__description__)
                        item.setToolTip(module.__description__)
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c+1, item)

                        item = QtWidgets.QTableWidgetItem(str(module.__version__))
                        item.setToolTip(str(module.__version__))
                        item.module_path = settings.GetRelativeModulePath(module_path)
                        table.setItem(row, c+2, item)

        # Update the table
        table.setSortingEnabled(True)
        table.resizeColumnToContents(0) # Name
        # table.resizeColumnToContents(1) # Desc
        table.resizeColumnToContents(2) # Version

        if verbose:
            Log(output_text, search_tags)
    #endregion Functions

    # ----------------------------------------------------------------------------------------------------------------------
    #region Context Menu
    #
    def menuContextActiveModules(self, point, verbose=False):
        """
        Display a context menu for the active modules.

        Args:
            point (QPoint): The X,Y coordinates of the mouse cursor for displaying the context menu at.
        """
        output_text = []

        table = self.widget.table_active_modules
        row = table.currentRow()
        col = table.currentColumn()

        # Info about the node selected.
        item = table.itemAt(point)

        # Build the menu
        menu = QtWidgets.QMenu()

        if item.widget and item.widget == self:
            action_unload = menu.addAction("Can't unload this tool, required.")
        else:
            action_unload = menu.addAction("Unload")
            # menu.addSeparator()
            # action_2 = menu.addAction("")

            # Connect Actions, pass the item
            action_unload.triggered.connect(lambda: self.UnloadModule(item))

        # Display the context menu
        menu.exec_(table.mapToGlobal(point))

        if verbose:
            Log(output_text, item.text(), row, col)

    def UnloadModule(self, item, verbose=False):
        """
        Unloads the selected module from the active modules table.

        Args:
            item ([type]): [description]
        """
        output_text = []

        table = self.widget.table_active_modules
        row = table.currentRow()
        col = table.currentColumn()

        if item.widget == self:
            output_text.append("[ERROR] Cannot remove this module, required!")
        else:
            item.widget.parent().RemoveToolDockWidget(item.widget)
            table.removeRow(row) # Remove that item row from the table

        if verbose:
            Log(output_text, row, col)

    def menuContextFoundModules(self, point, verbose=False):
        """Context Menu for the Found Modules table widget."""
        output_text = []

        table = self.widget.table_found_modules
        row = table.currentRow()
        col = table.currentColumn()

        # Infos about the node selected.
        item = table.itemAt(point)

        # We build the menu.
        menu = QtWidgets.QMenu()
        action_add_module = menu.addAction("Add Module")

        # Connect Actions, pass the item
        action_add_module.triggered.connect(lambda: self.on_item_doubleclicked(item))

        menu.exec_(table.mapToGlobal(point))

        if verbose:
            Log(output_text, row, col)
    #endregion Context Menu

#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    """Creates the ToolDockWidget and returns it."""
    widget = ManageModules(parent=parent, objectName="Manage Modules")
    return widget

#endregion Core Widget Functions
