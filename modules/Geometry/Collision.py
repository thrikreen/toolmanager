"""Collision Generation"""

# ----------------------------------------------------------------------------------------------------------------------
# Module Info
# __name__ = "collision"
__description__ = "Speed up collision generation and testing."
__version__ = 1.0
__author__ = 'Brian Chung'
__email__ = 'brian.chung@ubisoft.com'
__tags__ = [
    "3d",
    "geometry", "geo", "geom",

    "collision", "collisions", "physic", "physics", "physx", "havok", "havoc",
    "sphere", "box", "capsule", "cylinder", "convex", "hull", "kdop", "mesh",

    "3dsmax"
]

# name = "Collision Generator"
# description = "Tools to speed up collision generation."
# version = 1.0
# tags = [
#     "geometry", "geo", "geom"
#     "mesh",
#     "3d",

#     "collision", "physic", "physics",
#     "sphere", "box", "capsule", "cylinder", "convex", "hull",

#     "3dsmax"
# ]

#endregion Module Info

# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import random

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

import lib.settings as settings
from lib.Log import Log
from lib.ToolDockWidget import ToolDockWidget
#endregion Modules



# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class Collision(ToolDockWidget):
    """
    Collision Generation Tools
    """

    # NAMING_STYLES = ['custom', 'physx', 'havok']
    NAMING_STYLES = [0, 1, 2]
    NAMING_STYLE_CUSTOM, NAMING_STYLE_PHYSX, NAMING_STYLE_HAVOK = NAMING_STYLES

    # NAMINGMODES = ['objectroot', 'objectname', 'collisiontype']
    NAMING_MODES = [0, 1, 2, 3]
    NAMING_MODE_NONE, NAMING_MODE_ROOT, NAMING_MODE_OBJECT, NAMING_MODE_COLLISIONTYPE = NAMING_MODES

    COLLISION_TYPES = ['sphere', 'capsule', 'cylinder', 'box', 'convexhull', 'mesh', 'kdop10x', 'kdop10y', 'kdop10z', 'kdop18', 'kdop26']
    COLLISION_TYPE_SPHERE, COLLISION_TYPE_CAPSULE, COLLISION_TYPE_CYLINDER, COLLISION_TYPE_BOX, COLLISION_TYPE_CONVEXHULL, COLLISION_TYPE_MESH, COLLISION_TYPE_KDOP10X, COLLISION_TYPE_KDOP10Y, COLLISION_TYPE_KDOP10Z, COLLISION_TYPE_KDOP18, COLLISION_TYPE_KDOP26 = COLLISION_TYPES

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        """
        __init__ [summary]

        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
        """
        super(Collision, self).__init__(parent=parent, objectName=objectName)
        output_text = []
        output_text.append("Collision.py")

        self.help_url = "http://www.thrikreen.com/gamedev/collision-and-physics/"

        # Load UI
        # self.setObjectName(objectName)
        # self.setWindowTitle(objectName)
        self.load_ui(__file__)
        self.setWidget(self.widget)

        # Connect events
        if self.widget:
            # Load button icons
            self.LoadButtonIcon(self.widget.buttonSphere, "col_sphere.png")
            self.LoadButtonIcon(self.widget.buttonBox, "col_box.png")
            self.LoadButtonIcon(self.widget.buttonCapsule, "col_capsule.png")
            self.LoadButtonIcon(self.widget.buttonCylinder, "col_cylinder.png")
            self.LoadButtonIcon(self.widget.buttonConvexHull, "col_convexhull.png")
            self.LoadButtonIcon(self.widget.buttonKDOP, "col_kdop26.png")
            self.LoadButtonIcon(self.widget.buttonMesh, "col_mesh.png")


            # Collision Colour Swatch
            self.widget.toolColour_all.clicked.connect(lambda: self.UpdateColourSwatch(self.widget.toolColour_all))
            self.widget.pushButton_resetcolour.clicked.connect(self.OnResetCollisionColour)

            # Swap icon on KDOP button
            self.widget.radioKDOP10X.toggled.connect(lambda: self.SwapKDOPIcon(self.widget.radioKDOP10X))
            self.widget.radioKDOP10Y.toggled.connect(lambda: self.SwapKDOPIcon(self.widget.radioKDOP10Y))
            self.widget.radioKDOP10Z.toggled.connect(lambda: self.SwapKDOPIcon(self.widget.radioKDOP10Z))
            self.widget.radioKDOP18.toggled.connect(lambda: self.SwapKDOPIcon(self.widget.radioKDOP18))
            self.widget.radioKDOP26.toggled.connect(lambda: self.SwapKDOPIcon(self.widget.radioKDOP26))

            # Connect events
            self.widget.comboBox_naming_style.currentIndexChanged.connect(self.OnNamingSelectionChanged)
            self.widget.spinIndexDigits.valueChanged.connect(self.OnNameIndexDigitsChanged)

            self.widget.buttonSphere.clicked.connect(self.MakeSphere)
            self.widget.buttonBox.clicked.connect(self.MakeBox)
            # self.widget.buttonCapsule.clicked.connect(self.MakeCapsule)
            # self.widget.buttonCylinder.clicked.connect(self.MakeCylinder)
            # self.widget.buttonConvexHull.clicked.connect(self.MakeConvexHull)
            # self.widget.buttonKDOP.clicked.connect(self.MakeKDOP)
            # self.widget.buttonMesh.clicked.connect(self.MakeMesh)
            # self.widget.buttonCheckCollision.clicked.connect(self.check_collision)

        # Restore any stored settings
        self.ReadSettings()

        if verbose:
            Log(output_text)
    #endregion

    # Canadian Notation
    # (\.|def\s|bool\s|\s)[iI]s(.[\w\d]+)
    # $1$2Eh
    # Python
    # def IsSomething(self, args):
    # def isSomethingElse(self, args):
    # obj.IsSomething(args)
    # obj.isSomethingElse(args)
    # obj.isSomething or obj.IsSomethingElse and IsSomething() and not IsSomethingElse()
    # C#
    # bool IsWhatever(self, args) { }
    # bool isWhatever(self, args) { }
    #
    #

    # ----------------------------------------------------------------------------------------------------------------------
    #region Save/Restore
    #
    def ReadSettings(self, verbose=False):
        """
        Override the parent Save/ReadSettings() method to support saving and restoring the stored QToolButton background colours for colour swatches.

        Args:
            verbose (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        output = []

        for child in self.GetAllChildrenWidgets(self.widget, widget_filter=[QtWidgets.QToolButton]):
            if isinstance(child, QtWidgets.QToolButton):
                content = settings.GetSettings(self, child)
                stored_color = content[child.objectName()] if content[child.objectName()] else "#ffaaff"
                output += ["\t- Widget {}: {}".format(child.objectName(), stored_color)]
                child.setStyleSheet("background-color: {}".format(stored_color))

        if verbose:
            Log(output, "<class>")
        return super(Collision, self).ReadSettings(verbose=verbose)

    def SaveSettings(self, verbose=False):
        """
        Override the parent Save/ReadSettings() method to support saving and restoring the stored QToolButton background colours for colour swatches.

        Args:
            verbose (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        output = []

        for child in self.GetAllChildrenWidgets(self.widget, widget_filter=[QtWidgets.QToolButton]):
            if isinstance(child, QtWidgets.QToolButton):
                current_colour = QtGui.QColor(child.styleSheet().split(" ")[1])
                settings.SetSettings(self, child, current_colour.name())

        if verbose:
            Log(output, "<class>")
        return super(Collision, self).SaveSettings(verbose=verbose)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #
    def LoadButtonIcon(self, widget, iconfilename):
        """
        Loads the specified image as a button icon.

        Args:
            widget (QPushButton): The button to load the image into.
            iconfilename (str): Filename.ext of the image to load, looks in the current modulepath/collision_icons/ folder for their location.
        """
        icon = QtGui.QIcon()
        abs_icon_path = settings.sanitize_path(os.path.join(settings.MODULE_PATH, "Geometry", "collision_icons", iconfilename))
        icon.addPixmap(QtGui.QPixmap(abs_icon_path))
        widget.setIcon(icon)

    def UpdateColourSwatch(self, widget, verbose=False):
        """
        Updates the colour assigned for a collision type. Also saves the updated colour to the settings.

        Args:
            widget (QToolButton): The tool button widget used for the colour swatch.
        """
        output = []

        output += ["- Object: {}".format(widget.objectName())]

        # Get current colour (stored via the QToolButton stylesheet, convert to QColor format
        # https://doc.qt.io/qtforpython-5/PySide2/QtGui/QColor.html#PySide2.QtGui.PySide2.QtGui.QColor.name
        # Returns the name of the color in the format "#RRGGBB"; i.e. a "#" character followed by three two-digit hexadecimal numbers.
        old_colour = QtGui.QColor(widget.styleSheet().split(" ")[1])
        output += ["- Old Colour: {}".format(old_colour)]

        # https://doc.qt.io/qt-5/qcolordialog.html
        new_colour = QtWidgets.QColorDialog.getColor(old_colour)
        if new_colour.isValid():
            # Apply colour to the tool button background via stylesheet
            output += ["- New Colour: {}".format(new_colour)]
            widget.setStyleSheet("background-color: {}".format(new_colour.name()))
        else:
            output += ["- Cancel QColorDialog"]

        if verbose:
            Log(output, widget.objectName())

    def OnResetCollisionColour(self):
        """
        Reset the default collision object colour back to pink.
        """
        self.widget.toolColour_all.setStyleSheet("background-color: #ffaaff")

    def SwapKDOPIcon(self, widget, verbose=False):
        """
        Swaps the icon of KDOP button based on the selected radio.

        Args:
            widget (QRadioButton): The radio button that was toggled.
        """
        output = []
        icon_filename = "col_kdop26.png"
        if widget.isChecked():
            if widget.objectName() == "radioKDOP10X":
                icon_filename = "col_kdop10x.png"
            if widget.objectName() == "radioKDOP10Y":
                icon_filename = "col_kdop10y.png"
            if widget.objectName() == "radioKDOP10Z":
                icon_filename = "col_kdop10z.png"
            if widget.objectName() == "radioKDOP18":
                icon_filename = "col_kdop18.png"
            if widget.objectName() == "radioKDOP26":
                icon_filename = "col_kdop26.png"
            output += ["- Filename: {}".format(icon_filename)]
            self.LoadButtonIcon(self.widget.buttonKDOP, icon_filename)

        if verbose:
            Log(output, widget.objectName(), widget.isChecked())

    def OnNamingSelectionChanged(self, index):
        """
        When the naming style combobox has changed. Shows or hides the custom naming field.

        Args:
            index (int): The index of the selected item from the combo box.
        """
        output = []

        if index == 0: # Custom
            self.widget.widget_naming_custom.setVisible(True)
        else: # PhysX, Havok
            self.widget.widget_naming_custom.setVisible(False)

        Log(output, index)

    def OnNameIndexDigitsChanged(self, value):
        """
        Changes the example

        Args:
            value (int): Value from the spin box.
        """

        self.widget.labelIndex.setText("_{}".format("0" * value))

    def GetCollisionObjectName(self, obj, collision_type, verbose=True):
        """
        style:
        default custom
        physx
        havok

        prefix root/obj/coltype suffix countindex

        Args:
            collision_type ([type]): [description]
            verbose (bool, optional): [description]. Defaults to True.

        Returns:
            [type]: [description]
        """
        output = []
        colname = ""
        prefix = self.widget.linePrefix.text() if self.widget.linePrefix.text() else None
        objname = None
        objname2 = None
        suffix = self.widget.lineSuffix.text() if self.widget.lineSuffix.text() else None
        naming_style_name = None
        naming_mode_name = None

        naming_style = self.widget.comboBox_naming_style.currentIndex()
        if naming_style == Collision.NAMING_STYLE_CUSTOM:
            naming_style_name = "Default Custom"

            naming_mode = self.widget.comboBox_name_mode.currentIndex()
            if naming_mode == Collision.NAMING_MODE_NONE:
                naming_mode_name = "None"
                objname = None
            elif naming_mode == Collision.NAMING_MODE_ROOT:
                # Get the hierarchy root object name
                naming_mode_name = "Object Root"
                objname = "ObjectRoot"
            elif naming_mode == Collision.NAMING_MODE_OBJECT:
                # Get the selected object name
                naming_mode_name = "Object Name"
                objname = "ObjectName"
            elif naming_mode == Collision.NAMING_MODE_COLLISIONTYPE:
                naming_mode_name = "Collision Type"
                objname = collision_type

            naming_mode_2 = self.widget.comboBox_name_mode_2.currentIndex()
            if naming_mode_2 == Collision.NAMING_MODE_NONE:
                naming_mode_name_2 = "None"
                objname2 = None
            elif naming_mode_2 == Collision.NAMING_MODE_ROOT:
                # Get the hierarchy root object name
                naming_mode_name_2 = "Object Root"
                objname2 = "ObjectRoot"
            elif naming_mode_2 == Collision.NAMING_MODE_OBJECT:
                # Get the selected object name
                naming_mode_name_2 = "Object Name"
                objname2 = "ObjectName"
            elif naming_mode_2 == Collision.NAMING_MODE_COLLISIONTYPE:
                naming_mode_name_2 = "Collision Type"
                objname2 = collision_type


        elif naming_style == Collision.NAMING_STYLE_PHYSX:
            naming_style_name = "PhysX/Unreal"
            # [UBX/USP/UCP/UCX]_objname_##
            if collision_type == Collision.COLLISION_TYPE_SPHERE:
                prefix = "USP_"
            elif collision_type == Collision.COLLISION_TYPE_CAPSULE:
                prefix = "UCP_"
            elif collision_type == Collision.COLLISION_TYPE_BOX:
                prefix = "UBX_"
            elif collision_type == Collision.COLLISION_TYPE_CONVEXHULL:
                prefix = "UCX_"
            suffix = None

        elif naming_style == Collision.NAMING_STYLE_HAVOK:
            naming_style_name = "HavoK"

        output += ["- Naming Style: {}, {}".format(naming_style, naming_style_name)]
        output += ["- Collision Type: {}".format(collision_type)]
        output += ["- linePrefix: {}".format(prefix)]
        if naming_style == Collision.NAMING_STYLE_CUSTOM:
            naming_mode = self.widget.comboBox_name_mode.currentIndex()
            output += ["- comboBox_name_mode: {}".format(naming_mode_name)]
            output += ["- comboBox_name_mode_2: {}".format(naming_mode_name_2)]
        output += ["- lineSuffix: {}".format(suffix)]

        indexcount = self.widget.spinIndexDigits.value()
        index = 0
        indexstr = "{1:0{0}}".format(indexcount, index)
        output += ["- Index Size: {}, {} -> {}".format(indexcount, index, indexstr)]
        colname = "{}{}{}{}{}_{}".format(prefix if prefix else "", objname if objname else "", "_" if objname and objname2 else "", objname2 if objname2 else "", suffix if suffix else "", indexstr)

        # See if an existing obj exists by name, increment index and try again
        # found = self.GetSceneObject("colname", obj)
        found = True
        while found:
            indexstr = "{1:0{0}}".format(indexcount, index)
            output += ["- Index Size: {}, {} -> {}".format(indexcount, index, indexstr)]
            colname = "{}{}{}{}{}_{}".format(prefix if prefix else "", objname if objname else "", "_" if objname and objname2 else "", objname2 if objname2 else "", suffix if suffix else "", indexstr)


            if random.randrange(0, 10) == 0:
                found = False
            else:
                index += 1

        if verbose:
            Log(output, obj, collision_type)
        return colname
    #endregion UI Events

    # ----------------------------------------------------------------------------------------------------------------------
    #region Functions Collision
    #
    def MakeSphere(self):
        """
        Make a sphere collision object.
        """
        output = []

        # Check what environment this is running under
        if settings.DCCPLATFORM == settings.MAX:
            output += ["3dsmax"]

        elif settings.DCCPLATFORM == settings.MAYA:
            output += ["Maya"]

        elif settings.DCCPLATFORM == settings.BLENDER:
            output += ["Blender"]

        else:
            output += ["Standalone, nothing to operate on!"]
            self.parent().MessageBox("Oops!", "Not running under a recognized DCC application!\n\nDetected environment: {}\n\nPlease run this ToolManager under a DCC environment for this tool to work.".format(settings.DCCPLATFORM))

        objname = self.GetCollisionObjectName(None, Collision.COLLISION_TYPE_SPHERE)
        output += ["Object Name: {}".format(objname)]
        Log(output)

    def MakeBox(self):
        """box"""
        output = []

        objname = self.GetCollisionObjectName(None, Collision.COLLISION_TYPE_BOX)
        output += ["Object Name: {}".format(objname)]

        Log(output)

    def MakeCapsule(self):
        """capsule"""
        output = []
        Log(output)

    def MakeCylinder(self):
        """cylinder"""
        output = []
        Log(output)

    def MakeConvexHull(self):
        """Make a convex hull, best fit to the geometry."""
        output = []
        Log(output)

    def MakeKDOP(self):
        """Make KDOP collision"""
        output = []


        Log(output)

    def MakeMesh(self):
        """Make a mesh collision object, duplicating the obj's mesh geometry."""
        output = []
        Log(output)
    #endregion Functions Collision

#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    """Creates the ToolDockWidget and returns it."""
    widget = Collision(parent=parent, objectName="Collision")
    return widget
#endregion Core Widget Functions
