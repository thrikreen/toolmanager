"""Desparkle"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

import lib.settings as settings
from lib.Log import Log
from lib.ToolDockWidget import ToolDockWidget

import lib.dcc as dcc
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Module Info
name = "Desparkle"
description = "Snaps transforms and mesh vertices to specified precision, helps removes miniscule gaps when lined up for kit/tile pieces."
version = 1.0
tags = [
    "geometry", "geo", "geom"
    "mesh",
    "3d",

    "position", "pos",
    "rotation", "rot",
    "scale",

    "desparkle",
    "snap", "snaps",
    "vert", "verts", "vertex", "vertices",
    "pivot",
    "tile",
    "kit",

    "3dsmax",
    "maya",
    "houdini",
    "blender",
]
#endregion Module Info

# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class Desparkle(ToolDockWidget):
    """
    Desparkles a mesh

    Arguments:
        ToolDockWidget {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        """
        __init__ [summary]

        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
        """
        super(Desparkle, self).__init__(parent=parent, objectName=objectName)
        output_text = []
        output_text.append("Desparkle.py")

        self.help_url = "http://www.thrikreen.com/gamedev/{}".format(self.__class__.__name__)

        # Load UI
        # self.setObjectName(objectName)
        # self.setWindowTitle(objectName)
        self.load_ui(__file__)
        self.setWidget(self.widget)

        # Connect events
        if self.widget:
            self.widget.buttonDesparkle.clicked.connect(self.desparkle)
            self.widget.check_position.clicked.connect(self.ToggleFields)
            self.widget.check_rotation.clicked.connect(self.ToggleFields)
            self.widget.check_scale.clicked.connect(self.ToggleFields)
            self.widget.check_snapverts.clicked.connect(self.ToggleFields)

        # Restore any stored settings
        self.ReadSettings()

        # Update state of the checkboxes
        self.ToggleFields()

        if verbose:
            Log(output_text, objectName)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #
    def desparkle(self):
        """
        Process objects and desparkle them
        """
        output = []

        # Check what environment this is running under
        if settings.DCCPLATFORM == settings.MAX:
            output += ["- 3dsmax"]
        elif settings.DCCPLATFORM == settings.MAYA:
            output += ["- Maya"]
        elif settings.DCCPLATFORM == settings.BLENDER:
            output += ["- Blender"]
        else:
            output += ["- Standalone, nothing to operate on!"]
            # self.parent().MessageBox("Oops!", "Not running under a recognized DCC application!\n\nDetected environment: {}\n\nPlease run this ToolManager under a DCC environment for this tool to work.".format(settings.DCCPLATFORM))

        # Check Options
        if self.widget.check_position.checkState():
            output += ["- Check Position: {}".format(self.widget.spin_position.value())]
        if self.widget.check_rotation.checkState():
            output += ["- Check Rotation: {}".format(self.widget.spin_rotation.value())]
        if self.widget.check_scale.checkState():
            output += ["- Check Scale: {}".format(self.widget.spin_scale.value())]
        if self.widget.check_snapverts.checkState():
            output += ["- Check Verts: {}".format(self.widget.spin_snapverts.value())]
        output += ["- Using {}".format("Meters" if self.widget.check_usemeters.checkState() else "Centimeters")]

        # Collect selected valid objects to operate on
        selection = dcc.common.GetSelection()
        output += ["- Selected Objects: {}".format(len(selection))]

        for obj in selection:
            if self.widget.check_snapverts.checkState():
                vertex_count = dcc.geometry.vertex.GetVertexCount(obj)
                output += ["\t- {}: {}".format(obj, vertex_count)]



        verbose = True
        if verbose:
            Log(output)

    def ToggleFields(self, verbose=False):
        """
        Enables or disables the associated spinner field based on the checkbox's state.
        """
        self.widget.spin_position.setEnabled(self.widget.check_position.isChecked())
        self.widget.spin_rotation.setEnabled(self.widget.check_rotation.isChecked())
        self.widget.spin_scale.setEnabled(self.widget.check_scale.isChecked())
        self.widget.spin_snapverts.setEnabled(self.widget.check_snapverts.isChecked())


    #endregion UI Events

    # ----------------------------------------------------------------------------------------------------------------------
    #region Functions
    #

    #endregion Functions

#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    """Creates the ToolDockWidget and returns it."""
    widget = Desparkle(parent=parent, objectName="Desparkle")
    return widget
#endregion Core Widget Functions
