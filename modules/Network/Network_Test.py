"""Testing out network comm"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
import time
from datetime import datetime
import socket

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat, QtNetwork #pylint: disable=no-name-in-module

from lib.Log import Log
from lib.ToolDockWidget import ToolDockWidget
#endregion Modules

# Module Info
name = "Network Test"
description = "Testing out QNetwork sending and receiving."
version = 1.0
tags = ["network"]

SIZEOF_UINT32 = 4

# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class Network_Test(ToolDockWidget):
    """
    Search [summary]

    Arguments:
        ToolDockWidget {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        """
        Creates an instance of the Network_Test QWidget.

        Args:
            parent (QWidget, optional): [description]. Defaults to None.
            objectName (str, optional): [description]. Defaults to None.
            verbose (bool, optional): [description]. Defaults to False.
        """
        self.help_url = "http://intranet/dept/arttools/{}".format(self.__class__.__name__)
        super(Network_Test, self).__init__(parent=parent, objectName=objectName)

        output_text = []
        output_text.append("Network_Test.py")

        self.debug = True

        # Networking - Server
        self.tcp_server = None
        self.clients = []
        self.update_timer = QtCore.QTimer()
        self.update_timer.setInterval(1000) # update every second
        self.update_timer.timeout.connect(self.on_update_timer)
        self.update_start_time = None

        # Networking - Client
        self.socket = QtNetwork.QTcpSocket()
        if self.socket: # Connect client events - only need to do it once
            self.socket.readyRead.connect(lambda: self.on_client_receive(self.socket))
            self.socket.connected.connect(lambda: self.on_client_connect(self.socket))
            self.socket.disconnected.connect(lambda: self.on_client_disconnect(self.socket))
            self.socket.error.connect(lambda: self.on_client_error(self.socket))

        # Load UI
        # self.setObjectName(objectName)
        # self.setWindowTitle(objectName)
        self.load_ui(__file__)
        self.setWidget(self.widget)

        # Connect events
        if self.widget:
            self.widget.button_server.clicked.connect(self.on_start_server)
            self.widget.button_clear.clicked.connect(self.on_clear_log_window)
            self.widget.button_check_port.clicked.connect(self.on_check_port)
            self.widget.button_disconnect.clicked.connect(lambda: self.on_disconnect_server(self.socket))
            self.widget.button_send.clicked.connect(self.on_send)

        # Restore any stored settings
        self.ReadSettings()

        if verbose:
            Log(output_text)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events - General
    #
    def closeEvent(self, event, verbose=False):
        """closeEvent(event)"""
        # Clear the output window contents
        self.widget.textedit_output.setText("")
        super(Network_Test, self).closeEvent(event)

    def write(self, message, colour=None):
        """
        Writes the message to the textbox.
        Colour sets the text colour, use QColor(r,g,b). Defaults to black.
        """
        if self.widget.textedit_output:
            # Jump to the end of the text box
            self.widget.textedit_output.moveCursor(QtGui.QTextCursor.End)

            # Set text colour if defined, otherwise default to black.
            self.widget.textedit_output.setTextColor(QtGui.QColor(0, 0, 0))
            if colour is not None:
                self.widget.textedit_output.setTextColor(colour)

            # Write the text
            self.widget.textedit_output.insertPlainText(message + "\n")

    def on_update_timer(self):
        """
        Updates the uptime status label if the server is active.
        """
        if self.tcp_server and self.update_start_time:
            elapsed_time = datetime.now() - self.update_start_time
            display_time = "{:02d}:{:02d}:{:02d}".format(int(elapsed_time.seconds/3600), int((elapsed_time.seconds/60)%60), int(elapsed_time.seconds%60))
            self.widget.label_uptime.setText("Uptime: {}".format(display_time))
    #endregion UI Events - General

    # ----------------------------------------------------------------------------------------------------------------------
    #region Network Commands
    #
    def on_clear_log_window(self):
        """
        Clears the log window
        """
        self.widget.textedit_output.setText("")

    def on_check_port(self):
        """
        Checks if the port is currently bound to a service.
        """

        output_text = []

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ip = self.widget.edit_ip.text()
        port = self.widget.spinbox_port.value()
        result = sock.connect_ex((ip, port))
        output_text.append("Port {} is {}.".format(port, "closed" if result == 0 else "open"))
        sock.close()

        self.write(Log(output_text))

    def on_start_server(self):
        """
        Starts the server, listening on the specified port for incoming messages.
        """
        output_text = []

        if self.tcp_server is None:
            # Create new server and listen on the specified IP and port
            self.tcp_server = QtNetwork.QTcpServer(self)
            if not self.tcp_server.listen(QtNetwork.QHostAddress(self.widget.edit_ip.text()), self.widget.spinbox_port.value()):
                # Failed to start server (port in use?)
                output_text.append("Unable to start server: {}".format(self.tcp_server.errorString()))
                self.tcp_server = None

            else: # Server successfully created, init events
                self.tcp_server.newConnection.connect(self.on_server_connect)
                output_text.append("Starting server, binding to {}:{}".format(self.widget.edit_ip.text(), self.widget.spinbox_port.value()))
                self.widget.button_server.setText("Stop")
                self.update_timer.start()
                self.update_start_time = datetime.now()

        else: # Server is already started
            for client in self.clients: # close existing clients
                client.close()

            self.tcp_server.close()
            self.tcp_server = None
            self.update_timer.stop()
            output_text.append("Stopping server...")
            self.widget.button_server.setText("Start")

        self.write(Log(output_text))

    def on_server_connect(self):
        """
        Runs whenever a client connects to this server instance.
        """
        output = ""

        # New Connection
        client = self.tcp_server.nextPendingConnection()
        output += "{}:{}".format(client.peerAddress().toString(), client.peerPort())

        # Connect events
        client.readyRead.connect(self.on_server_receive_message)
        client.disconnected.connect(self.on_server_disconnect)
        client.error.connect(self.on_server_error)

        client.nextBlockSize = 0 # for datastreams
        self.clients.append(client)

        self.write(Log(output, len(self.clients)))

    def on_server_disconnect(self):
        """
        Runs when a client disconnects from the server.
        """

        output = ""
        output = "\n"

        clientsToRemove = []
        for client in self.clients:
            output += "{}:{} : ".format(client.peerAddress().toString(), client.peerPort())
            if client.state() == QtNetwork.QAbstractSocket.SocketState.UnconnectedState:
                output += "Removing client."
                clientsToRemove.append(client)
            output += "\n"

        for client in clientsToRemove:
            self.clients.remove(client)

        self.write(Log(output, len(self.clients)))

    def on_server_error(self):
        """
        When the server encounters an error.
        """
        output = ""
        output = "\n"

        for client in self.clients:
            output += "{}:{} : ".format(client.peerAddress().toString(), client.peerPort())
            output += "{}".format(client.errorString())
            output += "\n"

        self.write(Log(output, len(self.clients)))

    def on_server_receive_message(self):
        """
        When the server receives a message.
        """
        output = ""
        output += "\n"

        # Loop connected clients for pending data
        for client in self.clients:
            output += "{}:{} : ".format(client.peerAddress().toString(), client.peerPort())
            if client.bytesAvailable() > 0:

                data = self.readBlock(client)
                output += "\nReceived:\n{}\n".format(data)

                # Send Response Back
                if data == "CHECK":
                    #
                    start_time = datetime.now()

                    # DO STUFF
                    time.sleep(1)

                    # Report how long it took
                    elapsed_time = datetime.now() - start_time
                    display_time = "{:02d}:{:02d}:{:02d}".format(int(elapsed_time.seconds/3600), int((elapsed_time.seconds/60)%60), int(elapsed_time.seconds%60))

                    # Send Response Back
                    message = ""
                    message += "PASSED\n"
                    message += "CHECK COMPLETED, took {}".format(display_time)
                    output += "\nSent:\n{}\n".format(message)
                    block = self.writeBlock(message)
                    client.write(block)

                # Disconnect client
                client.close()
                client.waitForDisconnected()

            output += "\n"

        self.write(Log(output, len(self.clients)))
    #endregion Network Commands

    # ----------------------------------------------------------------------------------------------------------------------
    #region Network Events
    #
    def on_send(self):
        """
        When the SEND button is pressed, send the contents of the message box to the target IP:port.
        """
        output = ""

        if self.socket:
            self.socket.nextBlockSize = 0

            # Disconnect if still connected
            if self.socket.state == QtNetwork.QAbstractSocket.SocketState.ConnectedState:
                output += "Still connected, disconnecting.\n"
                self.socket.disconnectFromHost()

            # Connect to IP
            self.socket.connectToHost(QtNetwork.QHostAddress(self.widget.edit_ip.text()), self.widget.spinbox_port.value())
            output += "Connecting to {}:{}\n".format(self.widget.edit_ip.text(), self.widget.spinbox_port.value())
        else:
            output += "No socket"

        self.write(Log(output))

    def on_client_receive(self, client):
        """
        When the client receives a message from the server.

        Args:
            client ([type]): [description]
        """
        output = ""
        output += "\n"

        data = self.readBlock(client)
        output += "Received:\n{}\n".format(data)

        self.write(Log(output))

    def on_client_connect(self, client):
        """
        Runs when the client has connected to the server.

        Args:
            client ([type]): [description]
        """
        output = ""

        message = self.widget.lineedit_send.text()
        block = self.writeBlock(message)

        # Send message
        output += "Sending:\n{}\n".format(message)
        bytesSent = client.write(block)
        output += "Sent {} bytes.\n".format(bytesSent)

        self.write(Log(output))

    def on_client_disconnect(self, client):
        """
        Runs when the client has disconnected from the server.

        Args:
            client ([type]): [description]
        """

        output = ""
        output += "{}:{}".format(client.peerAddress().toString(), client.peerPort())
        self.write(Log(output))

    def on_client_error(self, client):
        """
        Client has encountered an error.

        Args:
            client ([type]): [description]
        """
        output = ""
        output += client.errorString()
        self.write(Log(output))

    def on_disconnect_server(self, client):
        """
        on_disconnect_server [summary]

        Args:
            client ([type]): [description]
        """
        output = ""

        # Disconnect if still connected
        if self.socket and self.socket.state == QtNetwork.QAbstractSocket.SocketState.ConnectedState:
            output += "Still connected, disconnecting {0}\n".format(client)
            self.socket.disconnectFromHost()

        self.write(Log(output))
    #endregion Network Events

    # ----------------------------------------------------------------------------------------------------------------------
    #region Network Functions
    #
    @staticmethod
    def writeBlock(data):
        """
        writeBlock [summary]

        Args:
            data ([type]): [description]

        Returns:
            [type]: [description]
        """
        # Create byte array
        block = QtCore.QByteArray()
        stream = QtCore.QDataStream(block, QtCore.QIODevice.WriteOnly)
        stream.setVersion(QtCore.QDataStream.Qt_4_8)

        # Set initial size of packet (32 bit value, 4 bytes)
        stream.writeUInt32(0)

         # Write the data to the datablock
        stream.writeQString(data)

        # Update the block with the size of the packet
        stream.device().seek(0)
        stream.writeUInt32(block.count() - SIZEOF_UINT32)

        return block

    @staticmethod
    def readBlock(clientSocket):
        """
        readBlock [summary]

        Args:
            clientSocket ([type]): [description]

        Returns:
            [type]: [description]
        """
        stream = QtCore.QDataStream(clientSocket)
        stream.setVersion(QtCore.QDataStream.Qt_4_8)

        # Check the size the packet (first value is a 32-bit, so 4 bytes)
        # if the size of the packet is less than 4, then it is empty or an invalid data packet.
        if clientSocket.nextBlockSize == 0:
            if clientSocket.bytesAvailable() < SIZEOF_UINT32:
                return None
            clientSocket.nextBlockSize = stream.readUInt32()
            if clientSocket.bytesAvailable() < clientSocket.nextBlockSize:
                return None

        data = stream.readQString()
        clientSocket.nextBlockSize = 0
        return data
    #endregion Network Events

#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    """get_widget"""
    widget = Network_Test(parent=parent, objectName="Network Test")
    return widget
#endregion Core Widget Functions
