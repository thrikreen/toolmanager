"""ToolManager"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import imp
import importlib
# import json
import webbrowser

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

import lib.Log
import lib.Vector
import lib.settings as settings
import lib.ToolDockWidget
import lib.DockOverlay

from lib.Log import Log
from lib.Vector import Vector
from lib.ToolDockWidget import ToolDockWidget
from lib.DockOverlay import DockOverlay
#endregion Modules


# ----------------------------------------------------------------------------------------------------------------------
# region ToolManager
#
class ToolManager(QtWidgets.QMainWindow):
    """
    Manages the tool launcher, creating child windows for grouping tool modules together.
    """
    # https://doc.qt.io/archives/qtforpython-5.12/PySide2/QtWidgets/QMainWindow.html

    # ----------------------------------------------------------------------------------------------------------------------
    #region __init__
    #
    def __init__(self, parent=None, objectName="dockwindow00", position=None, size=None, load_widgets=True, verbose=False):
        """
        Creates a ToolManager dock window.

        Args:
            parent (QWidget, optional): If set, creates a child ToolManager window under the parent. Defaults to None.
            objectName (str, optional): The object name to use for this window. Defaults to "dockwindow00".

            pos (Vector, optional): The screen x, y position to place the window. Defaults to None.
            size (Vector, optional): The window width and height. Defaults to None.

            root (string, optional): Application root folder. Used for finding the tool modules to load. Defaults to None.
            args (argparse, optional): The commandline args that were used to launch this program. Defaults to None.

            dccplatform (string, optional): Application root folder. Used for finding the tool modules to load. Defaults to None.

            load_widgets (bool, optional): Load the stored module widgets or not. Defaults to True.
        """
        if sys.version_info[0] == 2:
            super(ToolManager, self).__init__(parent=parent)
        else:
            super().__init__(parent=parent)


        output = []

        # ----------------------------------------------------------------------
        # Initialize Properties
        #
        self._parent = parent
        self.setObjectName(objectName)
        self._first_launch = None

        # Windows
        self.windows = [] # Child Windows
        self.window_names = []
        self.hover_widget = None

        # Modules
        self.widgets = [] # ToolDockWidgets loaded into this window
        self.widget_names = []

        output += ["Parent: {}".format(self.parent().objectName() if self.parent() else None)]
        output += ["Master Parent: {}".format(self.GetMasterParent().objectName() if self.GetMasterParent() else None)]
        output += ["DCC Platform: {}".format(settings.DCCPLATFORM)]

        self.load_workspaces = []
        self.remove_workspaces = []

        # ----------------------------------------------------------------------
        # Settings
        #
        self.ReadSettings(verbose=False)

        # ----------------------------------------------------------------------
        # UI
        #
        # Create child Windows
        for window_name in self.window_names:
            self.CreateWindow(name=window_name, load_widgets=True)

        self.init_ui(position=position, size=size)

        # Load Modules
        if load_widgets:
            output += ["- Widgets: {}".format(len(self.widget_names))]
            if not self.widget_names: # No modules, auto add the module manager!
                self.widget_names.append("General/manage_modules")
            for widget_name in self.widget_names:
                output += ["    - {}".format(widget_name)]
                self.AddToolWidget(widget_name)

            # Restore any QDockWidget states, if any exist
            contents = settings.GetSettings(self.objectName(), ["windowState"])
            if "windowState" in contents:
                self.restoreState(contents["windowState"])

        if verbose:
            Log(output, self.objectName())

    def init_ui(self, position=None, size=None, verbose=False):
        """
        Initializes the UI for this QMainWindow
        """
        output = []

        # Window Features
        self.setWindowTitle("ToolManager - {}".format(self.objectName()))
        self.setTabPosition(QtCore.Qt.AllDockWidgetAreas, QtWidgets.QTabWidget.West)
        # self.setTabShape(QtWidgets.QTabWidget.Triangular) # QtWidgets.QTabWidget.[Rounded|Triangular]
        self.setDockNestingEnabled(True) # QtWidgets.QMainWindow.AllowNestedDocks | QtWidgets.QMainWindow.AllowTabbedDocks)

        if self.GetMasterParent() == self: # Master is itself, main ToolManager Window
            self.init_menubar()
            self._first_launch = datetime.now()
        else: # Is a child window
            # Disable the close window button, as creating/removing the child windows are handled via moving DockWidgets
            self.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)

        # Set Window Position and Size
        if position and size: # if set on creation, use that
            output += ["Set on creation"]
            output += ["Position: {}, {}".format(position.x(), position.y())]
            output += ["Size: {}, {}".format(size.width(), size.height())]
            self.setGeometry(position.x(), position.y(), size.width(), size.height())
        else: # Otherwise load from settings
            contents = settings.GetSettings(self.objectName(), ["geometry", "windowState"])
            # output += ["Position: {}, {}".format(contents["geometry"].x(), position.y())]
            # output += ["Size: {}, {}".format(size.width(), size.height())]
            if "geometry" in contents and "windowState" in contents:
                output += ["Restore from settings"]
                self.restoreGeometry(contents["geometry"])
                self.restoreState(contents["windowState"])
            else: # Not set, default to center of active screen
                output += ["No settings found, defaulting to center of screen"]
                self.CenterWindow()
            output += ["Position: {}, {}".format(self.geometry().x(), self.geometry().y())]
            output += ["Size: {}, {}".format(self.geometry().width(), self.geometry().height())]

        # Check if it is on a valid screen position, in case of monitor changes

        if verbose:
            Log(output, self.objectName())

    def init_menubar(self):
        """
        Create and initializes the menubar.
        """
        menubar = self.menuBar()
        menubar.clear()

        # File
        menu_file = menubar.addMenu("File")
        menu_file_new = menu_file.addAction("New")
        menu_file_save = menu_file.addAction("Save")
        menu_file_quit = menu_file.addAction("Quit")

        self.init_menubar_workspaces(menubar)

        # Windows
        menu_windows = menubar.addMenu("Windows")
        menu_windows_reset = menu_windows.addAction("Reset to Center")
        menu_windows_add = menu_windows.addAction("Add Child")
        menu_windows_remove = menu_windows.addAction("Remove Child")

        # Options
        menu_options = menubar.addMenu("Options")
        menu_options_something = menu_options.addAction("Some Option")

        # Help
        menu_help = menubar.addAction("Help")

        # Connect menu commands to functions
        # File
        menu_file_new.triggered.connect(self.menu_not_implemented_yet)
        menu_file_save.triggered.connect(self.menu_not_implemented_yet)
        menu_file_quit.triggered.connect(self.menu_not_implemented_yet)

        # Window
        menu_windows_reset.triggered.connect(self.CenterWindow)
        menu_windows_add.triggered.connect(self.menu_windows_add)
        menu_windows_remove.triggered.connect(self.menu_windows_remove)

        # Options
        menu_options_something.triggered.connect(self.menu_not_implemented_yet)

        # Help
        menu_help.triggered.connect(self.menu_help)

    def init_menubar_workspaces(self, menubar):
        """
        init_menubar_workspaces [summary]

        Args:
            menubar ([type]): [description]

        Returns:
            [type]: [description]
        """
        workspaces = []
        self.load_workspaces = []
        self.remove_workspaces = []
        workspace_count = 0

        # Collect all the stored workspace names from the settings
        workspace_count = int(settings.GetSettings(self, "workspaces", default=0)["workspaces"])
        for i in range(workspace_count):
            workspace_element = settings.GetSettings("workspace_{}".format(i), "workspace_name", default="Saved Config {}".format(i))
            workspaces.append(workspace_element["workspace_name"])

        # Create the Workspace menus
        menu_workspace = menubar.addMenu("Workspaces")
        menu_workspace_save = menu_workspace.addAction("Save Current")

        # Load
        menu_workspace.addSeparator()

        for i in range(workspace_count):
            workspaceloadaction = menu_workspace.addAction("Load {}".format(workspaces[i]))
            # workspaceloadaction.setCheckable(True)
            self.load_workspaces.append(workspaceloadaction)

        # Remove - Submenu
        menu_remove_workspace = menu_workspace.addMenu("Remove Saved Workspace")
        for i in range(workspace_count):
            workspaceremoveaction = menu_remove_workspace.addAction("Remove {}".format(workspaces[i]))
            self.remove_workspaces.append(workspaceremoveaction)

        # Connect menu commands to functions
        menu_workspace_save.triggered.connect(self.OnMenuSaveWorkspace)

        for i in range(workspace_count):
            self.load_workspaces[i].triggered.connect(lambda checked=self.load_workspaces[i].isChecked(), index=i: self.OnMenuLoadWorkspace(checked, index))
            self.remove_workspaces[i].triggered.connect(lambda checked=self.remove_workspaces[i].isChecked(), index=i: self.OnMenuRemoveWorkspace(checked, index))

        return menu_workspace

    #endregion __init__

    # ----------------------------------------------------------------------------------------------------------------------
    #region Magic Methods
    #
    def __eq__(self, other): # self == other
        """__eq__"""
        if isinstance(other, self.__class__):
            if id(other) == id(self): # Same Object
                return True
            if other.objectName() == self.objectName(): # Same Object Name
                return True
        elif isinstance(other, str):
            if other == self.objectName():
                return True
        return False
    #endregion Magic Methods

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI - Menu Bar
    #
    def menu_not_implemented_yet(self):
        """Placeholder for menu commands"""
        self.MessageBox("Oops!", "Not implemented yet!")

    # Windows
    def menu_windows_add(self):
        """Create a window as a child of this one."""
        window = self.CreateWindow()
        window.CenterWindow() # auto center
        Log("Adding window", self.objectName())

    def menu_windows_remove(self):
        """Remove the most recent window from the list."""
        output = []
        if self.windows:
            window = self.windows[-1]
            self.RemoveWindow(window)
            output += ["Removed."]
            Log(output, window.objectName(), self.objectName())

    # Help
    def menu_help(self):
        """
        Open the Help menu for this tool.
        """
        webbrowser.open("http://www.google.ca/")
        Log("")

    # Workspaces
    def OnMenuSaveWorkspace(self, verbose=True):
        output = []

        # Get Current Workspace count
        workspace_count = settings.GetSettings(self, "workspaces", default=0, verbose=True)
        if "workspaces" in workspace_count:
            workspace_count = int(workspace_count["workspaces"])
        output += ["- Workspaces: {}".format(workspace_count)]

        workspacename, ok = QtWidgets.QInputDialog.getText(self, 'Workspace Name', 'Save')
        if ok:
            # Save current settings (window, geometry, modules) into the workspace_# group instead
            self.SaveSettings(overridegroup="workspace_{}".format(workspace_count), verbose=True)
            settings.SetSettings("workspace_{}".format(workspace_count), "workspace_name", workspacename)

            # Update the workspace count
            settings.SetSettings(self, "workspaces", workspace_count+1)

            # Refresh the menubar
            self.menuBar().clear()
            self.init_menubar()

        if verbose:
            Log(output)

    def OnMenuLoadWorkspace(self, checked, index):
        output = []
        Log(output, checked, index)

    def OnMenuRemoveWorkspace(self, checked, index):
        output = []

        workspaces = []

        # Get workspace count
        workspace_count = settings.GetSettings(self, "workspaces", default=0, verbose=True)
        if "workspaces" in workspace_count:
            workspace_count = int(workspace_count["workspaces"])

        # Load all workspaces in order
        for i in range(workspace_count):
            workspace_element = settings.GetSettings("workspace_{}".format(i))
            workspaces.append(workspace_element)

            # And clear from the .INI file?
            settings.SETTINGS.remove("workspace_{}".format(i))

        # Remove the specified workspace from the list
        del workspaces[index]
        workspace_count -= 1

        # Resave the workspaces, overriding the existing ones
        for i in range(workspace_count):
            settings.SETTINGS.beginGroup("workspace_{}".format(i))
            for k,v in workspaces[i].items():
                settings.SETTINGS.setValue(k, v)
            settings.SETTINGS.endGroup()

        # Update the workspace count
        settings.SetSettings(self, "workspaces", workspace_count)

        # Refresh the menubar
        self.menuBar().clear()
        self.init_menubar()

        Log(output, checked, index)

    #endregion UI - Menu Bar

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities - General
    #
    KEYMAP = {}
    MODMAP = {}
    def keyevent_to_string(self, event):
        """
        Convert the QKeyEvent code to a human readable string. I.e. 32 converts to Space.

        Args:
            event (QKeyEvent): The QKeyEvent to process.

        Returns:
            [str]: Human readable verison of the key event.
        """
        # Populates the mappings if not filled yet. Stored on the class
        if len(ToolManager.KEYMAP.keys()) == 0 or len(ToolManager.MODMAP.keys()) == 0:
            for key, value in vars(QtCore.Qt).items():
                if isinstance(value, QtCore.Qt.Key):
                    ToolManager.KEYMAP[value] = key.partition('_')[2]

            ToolManager.MODMAP = {
                QtCore.Qt.ControlModifier: ToolManager.KEYMAP[QtCore.Qt.Key_Control],
                QtCore.Qt.AltModifier: ToolManager.KEYMAP[QtCore.Qt.Key_Alt],
                QtCore.Qt.ShiftModifier: ToolManager.KEYMAP[QtCore.Qt.Key_Shift],
                QtCore.Qt.MetaModifier: ToolManager.KEYMAP[QtCore.Qt.Key_Meta],
                QtCore.Qt.GroupSwitchModifier: ToolManager.KEYMAP[QtCore.Qt.Key_AltGr],
                QtCore.Qt.KeypadModifier: ToolManager.KEYMAP[QtCore.Qt.Key_NumLock],
                }

        sequence = []
        for modifier, text in ToolManager.MODMAP.items():
            if event.modifiers() & modifier:
                sequence.append(text)

        key = ToolManager.KEYMAP.get(event.key(), event.text())
        if key not in sequence:
            sequence.append(key)
        return '+'.join(sequence)

    def SaveSettings(self, overridegroup=None, verbose=False):
        """
        Save the toolmanager window settings. Stores window position, modules, and child windows.

        Args:
            verbose (bool, optional): If True, will display the contents to be written to the INI file. Defaults to False.
        """
        output_text = []

        if not settings.SETTINGS:
            verbose = True
            output_text += ["[ERROR] No QSettings object!"]
            Log(output_text)
            return False

        # ----------------------------------------------------------------------------------------------------------------------
        #region Settings Group
        #
        groupname = overridegroup if overridegroup else self.objectName()
        settings.SETTINGS.beginGroup(groupname)

        # Window Settings
        output_text.append("- Window: pos: ({}, {}), size:({}, {})".format(self.geometry().x(), self.geometry().y(), self.geometry().width(), self.geometry().height()))
        settings.SETTINGS.setValue('geometry', self.saveGeometry())
        settings.SETTINGS.setValue('windowState', self.saveState())

        # Modules
        settings.SETTINGS.remove("widgets") # Completely remove old entries
        if self.widgets:
            output_text.append("- widgets: {}".format(len(self.widgets)))
            settings.SETTINGS.beginWriteArray("widgets")
            for i in range(len(self.widgets)):
                settings.SETTINGS.setArrayIndex(i)
                settings.SETTINGS.setValue("widgets", self.widgets[i].GetModulePath())
                output_text.append("    - widgets/{}: {}".format(i, self.widgets[i]))
            settings.SETTINGS.endArray()

        # Child Windows
        settings.SETTINGS.remove("windows") # Completely remove old entries
        if self.windows:
            output_text.append("- Windows: {}".format(len(self.windows)))
            settings.SETTINGS.beginWriteArray("windows")
            for i in range(len(self.windows)):
                settings.SETTINGS.setArrayIndex(i)
                settings.SETTINGS.setValue("windows", self.windows[i].objectName())
                output_text.append("    - window/{}: {}".format(i, self.windows[i].objectName()))
            settings.SETTINGS.endArray()

        settings.SETTINGS.endGroup()
        #endregion Settings Group

        if verbose:
            Log(output_text, groupname)

    def ReadSettings(self, verbose=False):
        """
        Reads the toolmanager window settings. Restores window position, modules, and child windows.

        Args:
            verbose (bool, optional): If True, will display the contents read from the INI file. Defaults to False.
        """
        output_text = []

        # Child Windows
        self.window_names = settings.GetSettingsArray(self.objectName(), "windows")
        if self.window_names:
            output_text.append("Windows:")
            for window_name in self.window_names:
                output_text.append("- {}".format(window_name))

        # Modules
        self.widget_names = settings.GetSettingsArray(self.objectName(), "widgets")
        if self.widget_names:
            output_text.append("Widgets:")
            for widget_name in self.widget_names:
                output_text.append("- {}".format(widget_name))

        if verbose:
            Log(output_text, self.objectName())

    def GetNextWindowIndex(self):
        """
        Gets the next available index counter for the child windows.

        Returns:
            [int]: The index of the next available dock window.
        """
        i = 0

        for i in range(100):
            new_window_name = "dockwindow{:02d}".format(i)
            if new_window_name not in self.windows and new_window_name != self.objectName():
                return i

        return i
    #endregion Utilities - General

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities - Window Management
    #
    def parent(self):
        """
        Returns the parent object of this QWindow

        Returns:
            [obj]: [description]
        """
        return self._parent

    def GetMasterParent(self, verbose=False):
        """
        Returns the top-most ToolManager window (or itself if no other parent)

        Returns:
            [ToolManager]: The top-most ToolManager widget. If running under a DCC, it will stop just before the DCC QMainWindow object.
        """
        output_text = []
        output_text.append("- Class: {}".format(self.__class__))

        parent = self

        if not parent.parent(): # No Parent, return itself as it's the top-most window object
            output_text.append("- {} -> {}, no parent".format(self.objectName(), self.parent().objectName() if self.parent() else "None"))
        else: # has a parent of type ToolManager (so when operating under a DCC, it will stop before that)
            count = 0
            while parent:
                output_text.append("- {} -> {}, {}".format(parent.objectName(), parent.parent().objectName() if parent.parent() else "None", parent.parent()))
                if parent.parent():
                    if isinstance(parent.parent(), self.__class__): # Is of the same ToolManager class
                        output_text.append("\t- Parent Class: {}".format(isinstance(parent.parent(), self.__class__)))
                        parent = parent.parent()
                    else: # Not a ToolManager class, hit the DCC window
                        output_text.append("> Not a ToolManager ")
                        break
                else: # No parent
                    break

                count += 1
                if count > 10:
                    break

        if verbose:
            Log(output_text)
        return parent

    def GetWindows(self, ignore_self=False, verbose=False):
        """
        Gets all the child windows of this window widget (including itself if asked).

        Args:
            ignore_self (bool, optional): If set to true, then it will not add itself to the list. Defaults to False.
            verbose (bool, optional): [description]. Defaults to True.

        Returns:
            [list]: Returns a list of ToolManager child window widgets.
        """
        output_text = []
        windows = []

        if not ignore_self:
            windows.append(self) # add itself

        for child in self.children():
            if isinstance(child, ToolManager) and child not in windows:
                output_text.append("+ {}".format(child.objectName()))
                windows.append(child)

        if verbose:
            Log(output_text, self.objectName())

        return windows

    def CenterWindow(self, screen=None, scale=0.3, verbose=False):
        """
        Centers the window to the specified screen display, scaled to % of the screen size.

        Args:
            screen (int, QScreen, optional): The screen (by index or QScreen object) to center this window to. Defaults to None for the current screen this widget is on.
            scale (float, optional): The scale factor to resize the window as a percentage of the display's. Defaults to 0.3 (for 30%).
        """
        output_text = []
        output_text.append("Window Position: {}, {}".format(self.pos().x(), self.pos().y()))
        size = None
        center = None

        if settings.DCCPLATFORM == settings.MAX: # 3dsMax PySide2 does not have a QMainWindow.screen() function
            desktop = QtWidgets.QApplication.desktop()

        # Not specified, use the screen this widget is on
        if screen is None:
            if settings.DCCPLATFORM == settings.MAX:
                screen = desktop.screenNumber(self.pos())
            elif settings.DCCPLATFORM == settings.STANDALONE:
                try:
                    screen = self.screen()
                except AttributeError: # No Attribute 'screen'
                    screen = 0

        # Specified screen index
        if isinstance(screen, int):
            if settings.DCCPLATFORM == settings.MAX:
                try:
                    screen = desktop.screen(screen)
                except IndexError: # Display doesn't exist, default to the primary screen
                    screen = desktop.screen[0]

                rect = desktop.screenGeometry(screen)
                center = rect.center()
                size = QtCore.QSize(rect.width(), rect.height()) # Convert from QRect to QSize
            elif settings.DCCPLATFORM == settings.STANDALONE:
                try:
                    screen = settings.QApplication().screens()[screen]
                except IndexError: # Display doesn't exist, default to the primary screen
                    screen = settings.QApplication().screens()[0]

        # Specified QtGui.QScreen
        if hasattr(QtGui, "QScreen") and isinstance(screen, QtGui.QScreen):
            center = screen.geometry().center()
            size = screen.availableGeometry().size()

        # Scale to (scale)% of the display's size
        # Move to target screen's center, offset by the window size / 2
        if size:
            size = size * scale
            self.move(center.x() - (size.width()/2), center.y() - (size.height()/2))
            self.resize(size)

        if verbose:
            Log(output_text, self.objectName())

    def CreateWindow(self, name=None, position=None, size=None, widgets=None, load_widgets=False, verbose=False):
        """
        Creates a new ToolManager window as a child of this one.

        Args:
            position (Vector, optional): Screen Position of the new window. Defaults to None.
            size (Vector, optional): Width and Height of the new window. Defaults to None.
            widgets (list, optional): List of widgets to move to this window (removing them from their current parent). Defaults to [].
            load_widgets (bool, optional): Toggle if the created window should load it's stored module widgets or not. Defaults to off.

        Returns:
            ToolMananger: The newly created ToolManager window object.
        """
        output_text = []

        # Check if this is the master ToolManager window object, or defer to it for window creation
        if self != self.GetMasterParent():
            window = self.GetMasterParent().CreateWindow(name=name, position=position, size=size, widgets=widgets, load_widgets=load_widgets, verbose=verbose)
            return window

        # Create a new ToolManager window with the position and size of the bottom ToolDockWidget,
        # Toggle to not load modules associated with that window from settings (fresh)
        new_window_name = name if name else "dockwindow{:02d}".format(self.GetNextWindowIndex())
        window = ToolManager(parent=self, objectName=new_window_name, position=position, size=size, load_widgets=load_widgets)
        output_text.append("New: {} @ ({}, {}), {}x{}".format(window.objectName(), window.pos().x(), window.pos().y(), window.size().width(), window.size().height()))
        window.show()
        self.windows.append(window) # Add to the list of child windows

        # Migrate widgets to new ToolManager window
        if widgets and isinstance(widgets, list):
            new_first_widget = None
            for widget in widgets:
                output_text.append("Moving \"{}\" from {} to {}".format(widget.objectName(), widget.parent().objectName(), window.objectName()))
                if not new_first_widget: # Move the first widget and store the newly
                    new_first_widget = self.MoveWidget(widget, window) # Move recreates the ToolDockWidget under the new parent, so we store the new object for later
                else: # Move the next one to be on top of the new widget
                    widget = self.MoveWidget(widget, window, dockwidget=new_first_widget)
                output_text.append("- Added to {}.widgets: {}".format(window.objectName(), window.GetWidgetNames()))

        if verbose:
            Log(output_text, new_window_name, position, size)
        return window

    def RemoveWindow(self, window, verbose=False):
        """
        Removes the specified window.

        Args:
            window (ToolManager): The ToolManager window to remove.

        Returns:
            bool: True if the window was found and removed, otherwise False.
        """
        output_text = []
        result = False

        output_text.append("Removing '{}' from '{}'.".format(window.objectName(), self.objectName()))
        if window in self.windows:
            self.windows.remove(window)
            window.deleteLater()
            result = True
            output_text.append("Window removed.")
        else:
            output_text.append("Window not found.")

        if verbose:
            Log(output_text, window.objectName(), self.objectName())
        return result

    def MessageBox(self, title, message):
        """
        Displays a message box to the user

        Args:
            title (str): The title of the window.
            message (str): The message to display.
        """
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Information)
        msg.setWindowTitle(title)
        msg.setText(message)
        msg.setStandardButtons(QtWidgets.QMessageBox.Close)
        msg.exec_()
    #endregion Utilities - Window Management

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities - Window Hover Highlight
    #
    def SetHoverOverlay(self, widget):
        """
        Create a hover widget to highlight the target widget.

        Args:
            widget (QWidget): The widget to apply the highlight over. If none, will hide the highlight widget object.
        """
        if not self.hover_widget:
            self.hover_widget = DockOverlay(parent=self.GetMasterParent())

        if widget: # Match the target widget's window position and size and show it
            geometry = widget.geometry()
            if isinstance(widget, ToolDockWidget) and not widget.isTopLevel(): # Docked, position will be relative to the parent's
                geometry.setX(widget.parent().geometry().x() + widget.geometry().x()) # pad for the tab?
                geometry.setY(widget.parent().geometry().y() + widget.geometry().y())
                geometry.setWidth(widget.geometry().width())
                geometry.setHeight(widget.geometry().height())

            self.hover_widget.setGeometry(geometry)
            self.hover_widget.show()
        else: # Not set, hide the widget.
            self.hover_widget.hide()

    #endregion Utilities - Window Hover Highlight


    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities - Module Management
    #
    def AddToolWidget(self, module_path, dockarea=QtCore.Qt.RightDockWidgetArea, dockwidget=None, verbose=False):
        """
        Loads and adds the module to this ToolManager window, either to the default right side dock area, or
        on top of an existing ToolDockWidget if specified.

        Args:
            module_path (str): Path of the module to load
            dockarea (, optional): Which QtDockArea to place the ToolDockWidget. Defaults to QtCore.Qt.RightDockWidgetArea.
            dockwidget (ToolDockWidget, optional): If set, places the ToolDockWidget to be moved on top of it. Defaults to None.

        Returns:
            [ToolDockWidget]: The widget that was added to this ToolManager window.
        """
        output_text = []

        if not dockarea:
            dockarea = QtCore.Qt.RightDockWidgetArea

        # Create a new widget object of the module and add to new parent window
        widget = settings.GetModuleWidget(self, module_path)
        if widget:
            output_text.append("- Adding {} to {}, {}".format(widget.objectName(), self.objectName(), dockarea))

            # https://doc.qt.io/archives/qtforpython-5.12/PySide2/QtWidgets/QMainWindow.html#PySide2.QtWidgets.PySide2.QtWidgets.QMainWindow.addDockWidget
            self.addDockWidget(dockarea, widget)

            if dockwidget: # Dock on top of existing widget (tabified)

                # Get the target widget's dockWidgetArea value
                dockarea = dockwidget.parent().dockWidgetArea(dockwidget)
                if dockarea == QtCore.Qt.DockWidgetArea.NoDockWidgetArea:
                    dockarea = QtCore.Qt.DockWidgetArea.RightDockWidgetArea

                output_text.append("- Tabifying over {}: {}".format(dockwidget.objectName(), dockarea))
                self.tabifyDockWidget(dockwidget, widget)

            widget.show()
            widget.update()

            # Update this ToolManager's dockwidget list
            self.widgets.append(widget)
            output_text.append("- Added to {}.widgets: {}".format(self.objectName(), self.GetWidgetNames()))

        else:
            output_text += ["[ERROR] {}: Unable to load widget!".format(module_path)]

        if verbose:
            Log(output_text, module_path, dockarea, dockwidget.objectName() if dockwidget else "None")
        return widget

    def RemoveToolDockWidget(self, widget, verbose=False):
        """
        Removes the ToolDockWidget from its parent window.

        Args:
            widget (ToolDockWidget): The widget to remove.
            verbose (bool, optional): Display output. Defaults to False.
        """
        output_text = []

        parent = widget.parent()

        if parent == self:
            output_text.append("- Removing from {}.widgets: {}".format(self.objectName(), self.GetWidgetNames()))
            widget.SaveSettings()
            self.widgets.remove(widget)
            self.removeDockWidget(widget)
            widget.deleteLater()
            output_text.append("- Removed from {}.widgets: {}".format(self.objectName(), self.GetWidgetNames()))
        else:
            output_text.append("[ ERROR ] Widget \"{}\" is not a child of {}".format(widget.objectName(), self.objectName()))

        if verbose:
            Log(output_text, widget.objectName())

    def MoveWidget(self, widget, window, dockarea=None, dockwidget=None, verbose=False):
        """
        Moves the widget from it's parent window to the target parent window.

        Args:
            widget (ToolDockWidget): [description]
            window (ToolManager): [description]
            dockarea ([type], optional): Side of the ToolManager window to dock to. Defaults to None.
            dockwidget ([type], optional): If set, will be tabified over the target widget. Defaults to None.

        Returns:
            [ToolDockWidget]: The widget that was moved.
        """
        output_text = []

        # module path
        module_path = widget.GetModulePath()

        # I'm sure you're wondering why am I not just using widget.setParent(newParent) and doing this
        # much more convoluted method of removing and adding the QDockWidget anew, basically I could
        # never get it working, it would result in a blank window or

        # Remove from old parent
        parent = widget.parent()
        parent.RemoveToolDockWidget(widget)
        # Add to new window
        widget = window.AddToolWidget(module_path, dockarea=dockarea, dockwidget=dockwidget)

        # widget.setParent(window)
        # widget.setFloating(False)

        # If old parent window has 1 or no more widgets, remove it (if not the main parent)
        if len(parent.widgets) <= 1 and parent != self.GetMasterParent():
            # if parent.parent():
            #     master_parent = parent.parent()

            if len(parent.widgets) == 1: # Refloat the last widget
                widget = parent.widgets[0]
                # Store it's geometry
                pos = parent.pos()
                size = parent.size()
                output_text.append("- Refloating {}: {}, {} (from parent)".format(widget.objectName(), pos, size))

                parent.RemoveToolDockWidget(widget)
                # Revert it's floating state position
                widget = window.AddToolWidget(widget.GetModulePath())
                widget.setFloating(True)
                widget.setGeometry(pos.x(), pos.y(), size.width(), size.height())

            # Remove Window
            output_text.append("- {}.widgets: {} removing. Master: {}".format(parent.objectName(), parent.GetWidgetNames(), self.GetMasterParent().objectName()))
            self.GetMasterParent().RemoveWindow(parent)
        else:
            output_text.append("- {}.widgets: {}, master window, skipping".format(parent.objectName(), parent.GetWidgetNames()))

        if verbose:
            Log(output_text, widget.objectName(), window.objectName())
        return widget

    def GetWidgets(self):
        """Return all the widgets for this ToolManager window."""
        return self.widgets

    def GetWidgetNames(self):
        """Returns all the widget names."""
        output = []

        for widget in self.widgets:
            output += [widget.objectName()]

        return ", ".join(output)
    #endregion Utilities - Module Management

    # ----------------------------------------------------------------------------------------------------------------------
    #region Events
    #
    def closeEvent(self, event, verbose=False):
        """
        Runs when the window has closed.

        Arguments:
            event {QEvent} -- [description]
        """
        output_text = []

        self.SaveSettings()

        if self.GetMasterParent() == self:
            output_text += ["Is Master Parent Window"]

        # Signal the ToolDockWidgets attached to this window, so they can save their settings too
        # This is after closing the group, so the tool module will have it's own block in the INI file.
        if self.widgets:
            output_text += ["Modules: {}".format(len(self.widgets))]
            for widget in self.widgets:
                output_text += ["- {}".format(widget.objectName())]
                if hasattr(widget, "closeEvent"):
                    widget.closeEvent(event)

        # Collect the windows
        if self.windows:
            output_text += ["Windows: {}".format(len(self.windows))]
            windows = []
            for window in self.windows:
                windows.append(window)
            for window in windows:
                output_text += ["- {}".format(window.objectName())]
                if hasattr(window, "closeEvent"):
                    window.close()

        if self.GetMasterParent() != self and isinstance(self.GetMasterParent(), self.__class__):
            self.GetMasterParent().RemoveWindow(self)

        if verbose:
            Log(output_text, self.objectName())

    def keyReleaseEvent(self, event):
        """keyReleaseEvent(event)"""
        output = []
        verbose = False

        if self._first_launch:
            if event.key() == QtCore.Qt.Key_Shift:
                output += ["-Shift"]
                # Compare elapsed time from first launch to current time, if under 5 seconds, then allow centering the window
                elapsed_time = datetime.now() - self._first_launch
                if elapsed_time.seconds < 5:
                    output += ["Resetting Window Position and Size"]
                    verbose = True
                    self.CenterWindow()
                self._first_launch = None

        if verbose:
            k = self.keyevent_to_string(event)
            Log(output, event.key(), k)
    #endregion Events

# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
if __name__ in ["__main__"]:
    # If this is run directly, run the launcher
    exec(open("launch_toolmanager.py").read()) # pylint: disable=exec-used
#endregion Main
