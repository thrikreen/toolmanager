macroScript launch_toolmanager
    category:"External Tools"
    buttonText:"Launch ToolManager"
    toolTip:"Launch ToolManager"
(
	-- Place in %LOCALAPPDATA%\Autodesk\3dsMax\2020 - 64bit\ENU\usermacros\
    clearListener()

    python_file = @"C:\\Workspaces\\WindowManager\\launch_toolmanager.py"

    /*
    Arguments to pass to the tool:
    -config=path/filename - Path and filename of the prefs .INI file to use (Defaults to c:\users\currentusername\company\toolmanager\toolmanager.ini)
    -module_path=path - Path to the location of the modules to load (Defaults to the same directory as the ToolManager)

    Example:
    args = #(
        @"-config=C:\\Users\\username\\toolmanager\\toolmanager_max.ini
        @"-module_path=C:\\Users\\username\\toolmanager\\modules\\"
    )
    */
    args = #()
    --append args @"-config=C:\\Users\\<username>\\toolmanager\\toolmanager_max.ini"
    --append args @"-module_path=C:\\Users\\<username>\\toolmanager\\modules"

    module_filename = getFilenameFile python_file
    script = StringStream ""

    format "import os\n" to:script
    format "import sys\n" to:script

    format "python_file = \"%\"\n" python_file to:script
    format "python_dir = os.path.dirname(python_file)\n" to:script

    format "print(\"- python_file = {}\".format(python_file))\n" to:script
    format "print(\"- python_dir = {}\".format(python_dir))\n" to:script
    format "print(\"- module_filename = %\")\n" module_filename to:script
    if args.count > 0 then format "print(\"- Args: %\")\n" args.count to:script
    for arg in args do format "print(\"    %\")\n" arg to:script

    format "if python_dir not in sys.path:\n" to:script
    format "    sys.path.append(python_dir)\n" to:script
    format "    print(\"Not found in sys.path, adding...\")\n" to:script
    format "    print(\"- sys.path: {}\".format(sys.path))\n" to:script

    format "sys.argv = [python_file]\n" to:script
    for arg in args do format "sys.argv.append(\"%\")\n" arg to:script
    format "print(\"- sys.argv[{}]: {}\".format(len(sys.argv), sys.argv))\n" to:script
    format "print(\"\")\n" to:script

    format "if '%' in sys.modules:\n" module_filename to:script
    format "    print(\"- reload %\\n\")\n" module_filename to:script
    format "    reload(%)\n" module_filename to:script
    format "else:\n" to:script
    format "    print(\"- import %\\n\")\n" module_filename to:script
    format "    import %\n" module_filename to:script

    script = script as string
    format ">>> Script:\n%\n" script
    format ">>> Executing: %\n" module_filename

    python.execute(script)
)