"""
Launches the ToolManager.

Parsing any commandline parameters, for overriding the default config file and module location.

Detects if it is running under a DCC (Digital Content Creator), and storing it for the ToolManager to check against.

Organization: https://trello.com/b/0gm7vXtI/toolmanager

Purpose: adds to the default QMainWindow/QDockWidget behaviour:
- Allow for floating QDockWidgets to dock to each other (creating a new QMainWindow, or phasing it out as needed)
- Highlight

To Do:
- Snapped QDockWidgets move with the window it was snapped to.

- Remote Debugging with 3dsmax:
    http://help.autodesk.com/view/MAXDEV/2021/ENU/?guid=Max_Python_API_tutorials_attach_debugger_html

"""

# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
import time
from datetime import datetime
from inspect import isclass

sys.dont_write_bytecode = True

# Get the current working directory, adding it to the sys.path if not found.
# if sys.version_info[0] == 2:
#     print("__file__: {}".format(__file__))
#     cwd = os.path.dirname(__file__)
#     if cwd not in sys.path:
#         print("[TM] Added to sys.path: \"{}\"".format(cwd))
#         sys.path.append(cwd)

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module
print("[Qt] Qt.py v{} ({}): {} v{}".format(Qt.__version__, Qt.__file__, "PySide2" if Qt.IsPySide2 else "PyQt5" if Qt.IsPyQt5 else "PySide" if Qt.IsPySide else "PyQt4" if Qt.IsPyQt4 else "Unknown", QtCore.qVersion())) #pylint: disable=no-member

#pylint: disable=wrong-import-position,ungrouped-imports
import toolmanager
import lib.Log
import lib.settings as settings
# import lib.ToolDockWidget as ToolDockWidget

from lib.Log import Log, SetGlobalLogProperties, GetLogFilename
from toolmanager import ToolManager
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Global Variables
#
DEBUG = True

# Reload modules if required - when running under a DCC as it often keeps the modules loaded in memory so
# restarting the tool manager should force reloading of the modules
if DEBUG and settings.DCCPLATFORM != settings.STANDALONE:
    #pylint: disable=undefined-variable,import-outside-toplevel
    if sys.version_info[0] == 2:
        print("- Python v{}.{}.{}: Reloading modules".format(sys.version_info[0], sys.version_info[1], sys.version_info[2]))
        reload(lib.Log)
        reload(toolmanager)
        reload(lib.settings)
        reload(lib.ToolDockWidget)
        # from toolmanager import ToolManager
        # import lib.settings as settings
    elif sys.version_info[0] == 3: # Python 3 under VSCode, no need to reload
        print("- Python v{}.{}.{}: Reloading modules".format(sys.version_info[0], sys.version_info[1], sys.version_info[2]))
        # import importlib
        # importlib.reload(lib.Log)
        # importlib.reload(toolmanager)
        # importlib.reload(settings)
        # importlib.reload(lib.ToolDockWidget)
    else:
        print("- Python v{}.{}.{}: Unable to reload modules".format(sys.version_info[0], sys.version_info[1], sys.version_info[2]))

#endregion

# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def main(verbose=True):
    """Main"""
    output_text = []

    # App Window
    window = None
    dcc_window = None
    qapp = QtWidgets.QApplication.instance()
    output_text += ["- QApp: {}".format(qapp)]
    if qapp is None: # App is not running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)
        settings.DCCPLATFORM = settings.STANDALONE
    else: # is running under a DCC, get the window
        dcc_window = settings.GetDCCWindow()
    output_text += ["- dcc_window: {}".format(dcc_window)]
    output_text += ["- DCCPLATFORM: {}".format(settings.DCCPLATFORM)]

    # Check if the window is already active
    # if dcc_window:
    #     toolwidgets = []
    #     # toolwidgets = [x for x in dcc_window.children() if x.__class__
    #     for w in dcc_window.children():
    #         try:
    #             if w.__class__.__name__ == "ToolManager":
    #                 toolwidgets.append(w)
    #         except Exception as e:
    #             pass
    #     output_text += ["Found {} existing ToolManager windows".format(len(toolwidgets))]
    #     if len(toolwidgets) > 0:
    #         # Destroy the old ToolManager widgets
    #         for widget in toolwidgets:
    #             widget.deleteLater()

    if verbose:
        Log(output_text)

    # Display the window
    window = ToolManager(parent=dcc_window, verbose=False)
    window.show()

    if settings.DCCPLATFORM == settings.STANDALONE:
        sys.exit(qapp.exec_())

def test_settings():
    """Test settings"""
    # settings.Initialize()
    content = settings.GetSettings("dockwindow00")
    for key in content:
        print("- {}: {}".format(key, repr(content[key])))

    modules = settings.GetSettingsArray("dockwindow00", "modules", verbose=True)
    print("modules: {}".format(len(modules)))
    for m in modules:
        print("- {}".format(m))

    windows = settings.GetSettingsArray("dockwindow00", "windows")
    print("windows: {}".format(len(windows)))
    for w in windows:
        print("- {}".format(w))

def test_attributes():
    """test"""
    #pylint: disable=import-outside-toplevel
    output = []

    # import modules.General.filesort
    import importlib
    import inspect
    from lib.ToolDockWidget import ToolDockWidget

    module_name = "filesort"
    module_path = 'c:/Workspaces/WindowManager/modules/General/filesort.py'
    spec = importlib.util.spec_from_file_location(module_name, module_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    # for m in [getattr(module, x) for x in dir(module)]:
    # for attrib_name in dir(module):
    #     attrib = getattr(module, attrib_name)
    #     output += ["- {}: {} {}".format(attrib_name, type(attrib), "Callable" if callable(attrib_name) else "")]

    output += ["- Module: \"{}\"".format(module.__name__)]

    attribs = inspect.getmembers(module, inspect.isclass)
    for f in attribs:
        attrib_name = f[0] # class name
        attrib = f[1]
        if issubclass(attrib, ToolDockWidget) and attrib != ToolDockWidget:
            output += ["- {}: {}".format(attrib_name, attrib)]
            output += ["\t- inherits ToolDockWidget"]
        else:
            output += ["- {}: Not valid ToolDockWidget subclass".format(attrib_name)]

    Log(output)

if DEBUG:
    print("__name__: {}".format(__name__))
if __name__ in ["__main__", "<module>", "launch_toolmanager"]:
    launcher_start_time = datetime.now()
    # SetGlobalLogProperties("prefix", "    ")
    SetGlobalLogProperties("endnewline", True)
    # Log("Launching ToolManager")

    # Initialize and Load the app settings
    settings.Initialize(verbose=True)

    main()
    # test_attributes()

    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#")
    launcher_elapsed_time = datetime.now() - launcher_start_time
    print("Elapsed Time: {:02d}:{:02d}:{:02d}.{:02d}".format(int(launcher_elapsed_time.total_seconds()/3600), int((launcher_elapsed_time.total_seconds()/60)%60), int(launcher_elapsed_time.total_seconds()%60), int(launcher_elapsed_time.microseconds)))
    print("\n\n")

#endregion Main
