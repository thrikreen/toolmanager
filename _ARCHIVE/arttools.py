"""
All Purpose

On Start:

    PyQt
        What environment (DCC) it is running under if applicable

    Config
        Loads %userprofile%/ToolsLauncher/settings.ini (JSON format)
            Not found, create default

        Load Module List
            Module Order
            Favourites

    Look in target folder for modules to load:
        Module
            module.tags= [] for searching, filtering, etc.
            module.properties for the tool(s), passes them as parameters to the executed actions
            module.get_layout() for the UI creation
                layout events hooked to module functions that run the actual tool
            launcher open/save will save and restore the properties

    UI
        Load base UI
        Create Module UI in order
        Populate with default or saved settings
        Register to event system

    Global event system
        tools can listen to events
            might have need to run after another tool has run
        log metrics on usage

"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
from __future__ import print_function
import sys
import os
import importlib
from datetime import datetime
import random

# WindowManager
#pylint: disable=wrong-import-position
wmpath = "C:/Workspaces/WindowManager" #pylint: disable=invalid-name
if wmpath not in sys.path:
    sys.path.append(wmpath)

import windowmanager
print("Python: {}".format(sys.version_info[:2]))
if sys.version_info[:2] == (3, 6):
    #print("importlib.reload(windowmanager)")
    importlib.reload(windowmanager)
else:
    #print("reload(windowmanager)")
    reload(windowmanager) #pylint: disable=undefined-variable
from windowmanager import WindowManager

import config
if sys.version_info[:2] == (3, 6):
    importlib.reload(config)
else:
    reload(config) #pylint: disable=undefined-variable
from config import Config
#pylint: enable=wrong-import-position


# ----------------------------------------------------------------------------------------------------------------------
#region Qt
#
try:
    import Qt
    from Qt import QtGui, QtWidgets, QtCore
except ImportError as exception:
    try: # Qt5, Max2018+
        import PySide2 # Max 2018+, Maya
        from PySide2 import QtGui, QtWidgets, QtCore
    except ImportError as exception:
        try: # Qt4, Max 2015
            import PySide
            from PySide import QtGui, QtCore
            from PySide import QtGui as QtWidgets # Qt5 split them into QtGui and QtWidgets, remap so we don't have to check all the time
        except ImportError as exception:
            print("ERROR: {}".format(exception))
            exit()
#endregion Qt
#endregion Modules


# ------------------------------------------------------------------------------------------------------------------
#region ArtTools
#
class ArtTools(WindowManager, QtWidgets.QMainWindow):
    """[summary]"""

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utils

    def get_parents(self, widget):
        """
        Returns a list of all the parent QObjects for this QObject.

        Arguments:
            widget {QObject} -- The QObject/QWidget to get the parent hierarchy from.

        Returns:
            [type] -- [description]
        """
        parents = [widget.parent()]
        if widget.parent():
            parents.extend(self.get_parents(widget.parent()))
        if parents[-1] is None: # remove the last element if it is None
            del parents[-1]
        return parents

    def get_children(self, widget):
        if widget:
            children = widget.children()
            print("Parent: {}".format(widget))
            for child in children:
                print("- {}".format(child))

    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Initialize
    #
    def __init__(self, parent=None):
        super(ArtTools, self).__init__(parent=WindowManager.get_dcc_window() if parent is None else parent)
        output = ""
        self.debug = True
        self.root = os.getcwd()

        self._settings_path = os.path.join(os.path.expanduser("~"), "Ubisoft", self.dcc_platform.capitalize(), "WindowSettings.ini") # Set preferred settings location
        output += "- _settings_path = {}\n".format(self._settings_path)

        # Initialize other properties here
        userdir = os.path.join(os.path.expanduser("~"), "Ubisoft", self.dcc_platform.capitalize(), "arttools.cfg")
        self.config = config.Config(userdir, autoload=True, mysql=True)

        # #self.config.display("{}".format("Loaded" if self.config.load() else "Failed to load"))
        self.config["datetime"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # UI
        self.init_layout()

        # Add the window to the parent app window, restoring saved settings if they exist, or default to center of the screen
        self.add_window()
        self.restore_window()
        self.show()

        print("\t- {}.parent: {}".format(self, self.parent()))

        # self.log(output, parent)
        self.log(output)

    def init_layout(self):
        """Initializes this window's contents."""
        output = ""

        # Menu Bar
        self.init_menubar()
        # Status Bar - at the bottom
        self.statusBar()

        # Filter Field
        self.filter_label = QtWidgets.QLabel()
        self.filter_label.setText("Filter: ")
        self.filter_textfield = QtWidgets.QLineEdit()

        self.filter_layout = QtWidgets.QFormLayout()
        self.filter_layout.addRow(self.filter_label, self.filter_textfield)
        #self.filter_textfield.textChanged.connect(self.on_filter_changed) # on any text change
        self.filter_textfield.textEdited.connect(self.on_filter_edit) # only on user change (so QLineEdit.setText() will not trigger)

        # Main
        self.tab_layout = QtWidgets.QVBoxLayout()
        self.tab_widget = self.get_tab_widget(self.tab_layout)
        self.tab_layout.addWidget(self.tab_widget)

        # Main Window
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.main_layout.addLayout(self.filter_layout)
        self.main_layout.addLayout(self.tab_layout)

        self.main_widget = QtWidgets.QWidget()
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)

        # Window Settings
        self.setWindowTitle("Art Tools")
        self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        self.setMinimumSize(200, 200)
        self.setMaximumSize(1920, 1080)
        #self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

        # Funky
        #self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        # Debug - walk QObject heirarchy
        # for tab in self.tabs:
        #     print("Tab: {}".format(tab))
        #     parents = self.get_parents(tab)
        #     for parent in parents:
        #         print("\t- {}".format(parent))

        # self.get_children(WindowManager.get_dcc_window())

        self.log(output)

    def init_menubar(self):
        # Create Menu Action
        menu_action = QtWidgets.QAction("&Open File", self)
        menu_action.setShortcut("Ctrl+O")
        menu_action.setStatusTip('Load File')
        #menu_action.triggered.connect(self.close_application)

        # Menu Bar
        self.main_menu = self.menuBar()

        # Create Menu Category - File
        self.file_menu = self.main_menu.addMenu('&File')
        self.file_menu.addAction(menu_action)

    def get_tab_widget(self, parent=None, modules_path="modules"):

        # TabWidget
        tab_widget = QtWidgets.QTabWidget()
        tab_widget.setTabPosition(QtWidgets.QTabWidget.West)
        tab_widget.setTabShape(QtWidgets.QTabWidget.Triangular) # QtWidgets.QTabWidget.[Rounded|Triangular]
        # tab_widget.setStyleSheet("QTabBar::tab { background-color: grey; } QTabBar::tab::selected { background-color: green; }")
        # tab_widget.addTab(widget, icon, label)

        # p = r"F:\projects\square\windowmanager\arttools.py"
        root_path = os.path.dirname(__file__)
        root_modules_path = os.path.join(root_path, modules_path)

        print("[AT] root_path: {}".format(root_path))
        print("[AT] modules_pathname: {}".format(modules_path))
        print("[AT] root_modules_path: {}".format(root_modules_path))

        for root, dirs, files in os.walk(root_modules_path):
            dirs[:] = [d for d in dirs if not d[0] in ['.', '_']] # filter out hidden or system folders

            for d in dirs: # Create a tab for each folder
                subpath = os.path.join(root, d)
                print("- {}".format(subpath))

                # Create our scrollable tab area
                scrollarea = QtWidgets.QScrollArea()
                scrollarea.setWidgetResizable(True)
                # scrollarea.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
                scrollarea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
                scrollarea.setFrameShape(QtWidgets.QFrame.NoFrame) # NoFrame|Box|Panel|WinPanel|HLine|VLine|StyledPanel # https://doc.qt.io/qtforpython/PySide2/QtWidgets/QFrame.html
                tab_widget.addTab(scrollarea, d)

                widget = QtWidgets.QWidget()
                scrollarea.setWidget(widget)
                layout = QtWidgets.QVBoxLayout(widget)
                layout.setAlignment(QtCore.Qt.AlignTop)

                for subroot, subdirs, subfiles in os.walk(subpath): # add layouts for this tab from the modules in it
                    subfiles = [f for f in subfiles if not (f[0] in ['.', '_'] or os.path.splitext(f)[1].lower() == ".pyc")]
                    for f in subfiles:

                        module_filepath = os.path.join(subroot, f)
                        module_filepath = module_filepath.replace(root_path, "").replace("/", ".").replace("\\", ".") # strip prefix, replace \/ with .
                        module_path = os.path.splitext(module_filepath)[0]
                        if module_path[0] == ".":
                            module_path = module_path[1:]

                        print("\t- {}".format(module_path))

                        m = importlib.import_module(module_path)
                        if hasattr(m, "get_widget"):
                            layout.addWidget(m.get_widget())
                        elif hasattr(m, "get_layout"):
                            # Sub-Panel Frame for this module
                            subframe = QtWidgets.QFrame()
                            subframe.setFrameShape(QtWidgets.QFrame.Box) # NoFrame|Box|Panel|WinPanel|HLine|VLine|StyledPanel # https://doc.qt.io/qtforpython/PySide2/QtWidgets/QFrame.html
                            subframe.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)

                            # Get the layout contents from the module
                            subframe.setLayout(m.get_layout())

                            # Add this widget to the tab layout
                            layout.addWidget(subframe)


        return tab_widget

    def on_filter_edit(self, arg):
        output = ""
        self.log(output, arg)


    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Window Events
    def closeEvent(self, event): #pylint: disable=invalid-name
        """
        Runs when the window widget has closed.

        Arguments:
            event {[type]} -- [description]
        """
        super(ArtTools, self).closeEvent(event)

        output = ""
        output += "- Saving config: {}\n".format(self.config.save())

        if self.config.database:
            output += "- Closing database connections...\n"
            self.config.disconnect()

        self.log(output)

    #endregion

#endregion

# ------------------------------------------------------------------------------------------------------------------
#region __main__
#
def main():
    """main()"""
    qapp = QtWidgets.QApplication.instance()
    if qapp is not None: # App is running under another app environment, i.e. 3dsMax or Maya
        window = ArtTools() #pylint: disable=unused-variable
    else: # Standalone
        qapp = QtWidgets.QApplication(sys.argv)
        window = ArtTools()
        sys.exit(qapp.exec_())

if __name__ == "__main__":
    main()
#endregion main
