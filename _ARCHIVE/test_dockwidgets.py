"""

https://www.qtcentre.org/threads/41847-Dragging-QDockWidgets-between-QMainWindows

"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
import importlib
import math
from datetime import datetime
import json

try:
    import Qt
    from Qt import QtGui, QtWidgets, QtCore
    from Qt import QUiLoader
    print("[WM] import Qt")
except ImportError as e:
    try:
        import PySide2 # QT5, Max 2017+, Maya
        from PySide2 import QtGui, QtWidgets, QtCore
        from PySide2.QtUiTools import QUiLoader
        print("[WM] import PySide2")
    except ImportError as e:
        try:
            import PySide # QT4, Max 2015
            from PySide import QtGui, QtCore
            from PySide import QtGui as QtWidgets
            from PySide import QUiLoader
            print("[WM] import PySide")
        except ImportError as e: # Exit since no Qt libraries are available
            print("[WM] ERROR: {}".format(e))
            exit()
finally:
    try:
        loader = QUiLoader()
    except Exception as e:
        print("[WM] ERROR: {}".format(e))
        exit()

from lib.Log import Log
from lib.Vector import Vector
# import lib.search
# import modules.Network.Network_Test
# import modules.Geometry.Desparkle
#endregion Modules


# ----------------------------------------------------------------------------------------------------------------------
# region Dock Demo
#
class dockdemo(QtWidgets.QMainWindow):

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, objectName="dock01", parent=None):
        super(dockdemo, self).__init__(parent)

        output_text = []
        self.debug = True
        self.parent = parent

        self.setObjectName(objectName) # required to allow for window state save/restore

        self.dock_items = []
        self.dock_modules = []
        self.readSettings()


        # DockWidget Tab Style
        self.setTabPosition(QtCore.Qt.AllDockWidgetAreas, QtWidgets.QTabWidget.West)
        # self.setTabShape(QtWidgets.QTabWidget.Triangular) # QtWidgets.QTabWidget.[Rounded|Triangular]
        self.setDockNestingEnabled(QtWidgets.QMainWindow.AllowNestedDocks | QtWidgets.QMainWindow.AllowTabbedDocks)

        # Dockable Widgets
        if self.dock_modules:
            output_text.append("Loading {} module(s):".format(len(self.dock_modules)))
            for dock_module in self.dock_modules:
                output_text.append("- {}".format(dock_module))
                widget = self.get_module_widget(dock_module)
                self.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget)
                self.dock_items.append(widget)

        # Central Widget
        self.setWindowTitle("Dockable Widgets Demo - {}".format(objectName))
        # layout = QtWidgets.QHBoxLayout()
        # self.setLayout(layout)
        # self.setCentralWidget(QtWidgets.QLabel("  Put a company logo here?"))
        self.setCentralWidget(None)

        # Set various window feature flags depending if it's the main or child window
        if parent: # Only allow the close window on the main window, not the children
            # Qt.MSWindowsFixedSizeDialogHint
            # Qt.X11BypassWindowManagerHint
            # Qt.FramelessWindowHint
            # Qt.WindowTitleHint
            # Qt.WindowSystemMenuHint
            # Qt.WindowMinimizeButtonHint
            # Qt.WindowMaximizeButtonHint
            # Qt.WindowCloseButtonHint
            # Qt.WindowContextHelpButtonHint
            # Qt.WindowShadeButtonHint
            # Qt.WindowStaysOnTopHint
            # Qt.WindowStaysOnBottomHint
            # Qt.CustomizeWindowHint
            self.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)
            # self.setWindowFlag(QtCore.Qt.FramelessWindowHint, True)
        else:
            # UI
            self.init_menubar()
            # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint, True)
            pass

        # Restore the window geometry and state of the dockwidgets
        self.readSettings(restore_window=True)

        Log(output_text)

    def init_menubar(self):
        menubar = self.menuBar()
        menu_file = menubar.addMenu("File")
        menu_file.addAction("New")
        menu_file.addAction("Save")
        menu_file.addAction("Quit")
        menu_options = menubar.addMenu("Options")
        menu_options.addAction("Reset")
        menu_help = menubar.addAction("Help")

        # Connect menubar events

    #endregion Initialize

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities
    #
    @classmethod
    def GetSettings(cls):
        """
        Loads the QSettings file into a QSettings object and returns it.
        See: https://doc.qt.io/qtforpython/PySide2/QtCore/QSettings.html

        Returns:
            QSettings -- The QSettings object with the INI file loaded.
        """
        # User directory: c:/users/username/app/
        #userdir = os.path.join(os.path.expanduser("~"), "company", "application", "filename.ini")
        userdir = os.path.join(os.path.expanduser("~"), "Ubisoft", "dockwidgetdemo", "dockwidgetdemo.ini")
        settings = QtCore.QSettings(userdir, QtCore.QSettings.IniFormat)

        return settings

    def get_qsettings(self):
        """
        Gets the QSettings file
        See: https://doc.qt.io/qtforpython/PySide2/QtCore/QSettings.html

        Returns:
            QSettings -- QSettings object containing the values stored in the specified INI file.
        """
        # User directory: c:/users/username/app/
        #userdir = os.path.join(os.path.expanduser("~"), "company", "application", "filename.ini")
        userdir = os.path.join(os.path.expanduser("~"), "Ubisoft", "dockwidgetdemo", "dockwidgetdemo.ini")
        settings = QtCore.QSettings(userdir, QtCore.QSettings.IniFormat)

        return settings

    def readSettings(self, restore_window=False):
        output_text = []
        settings = self.get_qsettings()
        output_text.append("- QSettings: {}".format(settings.fileName()))

        # Restore the window settings
        settings.beginGroup(self.objectName())

        size = settings.beginReadArray("dock_modules")
        self.dock_modules = []
        for i in range(size):
            settings.setArrayIndex(i)
            module_path = settings.value("dock_module")
            self.dock_modules.append(module_path)
            output_text.append("\t\t- dock_module[{}]: {}".format(i, self.dock_modules[i]))
        settings.endArray()

        output_text.append("- Restoring Window Settings")
        self.restoreGeometry(settings.value("geometry"))
        if restore_window: # Do this after to restore all the QDockWidget states
            self.restoreState(settings.value("windowState"))

        settings.endGroup()

        Log(output_text)

    def restore_window_states(self):
        settings = self.get_qsettings()
        self.restoreGeometry(settings.value("geometry"))
        self.restoreState(settings.value("windowState"))

    def get_modules(self):
        output_text = []

        # p = r"F:\projects\square\windowmanager\arttools.py"
        module_root = r"C:/Workspaces/WindowManager/"
        root_path = os.path.dirname(module_root)
        modules_pathname = "modules"

        root_modules_path = os.path.join(root_path, modules_pathname)

        output_text.append("- root_path: {}".format(root_path))
        output_text.append("- modules_pathname: {}".format(modules_pathname))
        output_text.append("- root_modules_path: {}".format(root_modules_path))

        for root, dirs, files in os.walk(root_modules_path):
            dirs[:] = [d for d in dirs if not d[0] in ['.', '_']] # filter out hidden or system folders
            for d in dirs: # Create a tab for each folder
                subpath = os.path.join(root, d)
                output_text.append("\t- Sub Path: {}".format(subpath))

                for subroot, subdirs, subfiles in os.walk(subpath):
                    subfiles = [f for f in subfiles if not (f[0] in ['.', '_'] or not os.path.splitext(f)[1].lower() == ".py")] # collect files if not hidden or not a .py file
                    for f in subfiles:
                        module_filepath = os.path.join(subroot, f) # get the abs path of the module
                        module_filepath = module_filepath.replace(root_path, "").replace("/", ".").replace("\\", ".") # strip prefix, replace path slashes with .

                        module_path = os.path.splitext(module_filepath)[0] # Remove the trailing extension
                        if module_path[0] == ".": # remove the leading .
                            module_path = module_path[1:]
                        output_text.append("\t\t- {}".format(module_path))

                        # Load/Import the Module!
                        # module = importlib.import_module(module_path)

        Log(output_text)

    def get_module_widget(self, module_path):
        widget = None

        try:
            module = importlib.import_module(module_path)
            widget = module.get_widget(self)
            self.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget)
        except Exception as e:
            Log("[ERROR] 1 {}".format(e))

        return widget
    #endregion Utilities

    # ----------------------------------------------------------------------------------------------------------------------
    #region Events - Window
    #
    def closeEvent(self, event):
        output_text = []
        settings = self.get_qsettings()
        output_text.append("- QSettings: {}".format(settings.fileName()))

        # Save the window settings
        settings.beginGroup(self.objectName())
        output_text.append("- {}.{}".format(self.objectName(), self.__class__.__name__))

        # Window geometry and state
        output_text.append("- Saving Window Settings")
        settings.setValue('geometry', self.saveGeometry())
        settings.setValue('windowState', self.saveState())

        # The modules to load
        settings.beginWriteArray('dock_modules')
        for i in range(len(self.dock_modules)):
            settings.setArrayIndex(i)
            settings.setValue('dock_module', self.dock_modules[i])
            output_text.append("\t\t- dock_module[{}]: {}".format(i, self.dock_modules[i]))
        settings.endArray()

        settings.endGroup()

        # Signal the QDockWidgets, so they can save any settings
        for dock_item in self.dock_items:
            if hasattr(dock_item, "closeEvent"):
                dock_item.closeEvent(event)
            else:
                output_text.append("- {}.closeEvent(): not found".format(dock_item))

        Log(output_text)
        # Continue with the other closeEvents()
        super(dockdemo, self).closeEvent(event)

    # def moveEvent(self, event):
    #     output_text = []
    #     pos = Vector(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y())
    #     output_text.append("Pos: {}".format(pos))

    #     # widgets = []
    #     # widget_at = QtGui.qApp.widgetAt(pos)
    #     # while widget_at:
    #     #     widgets.append(widget_at)

    #     Log(output_text, event)
    #     event.ignore()
    #     return super(dockdemo, self).moveEvent(event)

    #endregion Events - Window

#endregion

def ParseModuleRoot():
    # p = r"F:\projects\square\windowmanager\arttools.py"
    root_path = os.path.dirname(__file__)
    modules_pathname = "modules"
    root_modules_path = os.path.join(root_path, modules_pathname)

    print("root_path: {}".format(root_path))
    print("modules_pathname: {}".format(modules_pathname))
    print("root_modules_path: {}".format(root_modules_path))

    for root, dirs, files in os.walk(root_modules_path):
        dirs[:] = [d for d in dirs if not d[0] in ['.', '_']] # filter out hidden or system folders
        for d in dirs: # Create a tab for each folder
            subpath = os.path.join(root, d)
            print("- {}".format(subpath))

            for subroot, subdirs, subfiles in os.walk(subpath): # add layouts for this tab from the modules in it
                subfiles = [f for f in subfiles if not (f[0] in ['.', '_'] or os.path.splitext(f)[1].lower() == ".pyc")]
                for f in subfiles:
                    module_filepath = os.path.join(subroot, f)
                    module_filepath = module_filepath.replace(root_path, "").replace("/", ".").replace("\\", ".") # strip prefix, replace \/ with .
                    module_path = os.path.splitext(module_filepath)[0]
                    if module_path[0] == ".":
                        module_path = module_path[1:]

                    print("\t- {}".format(module_path))


# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def main():
    # Load window list
    settings = dockdemo.GetSettings()
    settings.beginGroup("_Global")
    size = settings.beginReadArray("windows")
    window_names = []
    for i in range(size):
        settings.setArrayIndex(i)
        window_name = settings.value("window")
        window_names.append(window_name)
    settings.endArray()
    if len(window_names) == 0:
        window_names.append("dock01")
    settings.endGroup()

    # QApp
    qapp = QtWidgets.QApplication.instance()
    if qapp is None: # App is running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)
        standalone = True

    # Create all the dock windows
    windows = []
    main_window = None
    for window_name in window_names:
        window = dockdemo(objectName=window_name, parent=main_window) #pylint: disable=unused-variable
        if not main_window:
            main_window = window
        window.show()
        windows.append(window)

    # Save window list
    settings.beginGroup("_Global")
    size = settings.beginWriteArray("windows")
    for i in range(len(windows)):
        settings.setArrayIndex(i)
        settings.setValue("window", windows[i].objectName())
    settings.endArray()
    settings.endGroup()
    print("Saved to: {}".format(settings.fileName()))

    if standalone:
        sys.exit(qapp.exec_())

    # qapp = QtWidgets.QApplication.instance()
    # if qapp is not None: # App is running under another app environment, i.e. 3dsMax or Maya
    #     for window in windows:
    #         ex = dockdemo(objectName=window) #pylint: disable=unused-variable
    #         ex.show()
    # else: # Standalone
    #     qapp = QtWidgets.QApplication(sys.argv)
    #     for window in windows:
    #         ex = dockdemo(objectName=window) #pylint: disable=unused-variable
    #         ex.show()
    #     sys.exit(qapp.exec_())

if __name__ in ["__main__", "<module>"]:
    main()
    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")
#endregion
