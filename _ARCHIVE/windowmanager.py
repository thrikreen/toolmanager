"""
WindowManager

Manages window creation for tools in various 3D modeling packages. Handles saving and restoring window position and
size, ensuring only one window of that type exists.

Supported Platforms:
- 3D Studio Max: 2017
- Maya:

"""
# ----------------------------------------------------------------------------------------------------------------------
# Modules
#
from __future__ import print_function
import os
import sys
import inspect
from datetime import datetime

from lib.Log import Log

# ----------------------------------------------------------------------------------------------------------------------
#region Constants
#
DCCPLATFORMS = ['STANDALONE', 'MAX', 'MAYA']
STANDALONE, MAX, MAYA = DCCPLATFORMS
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region Qt
#
try:
    import Qt
    from Qt import QtGui, QtWidgets, QtCore
    #print("[WM] import Qt")
except ImportError as e:
    try:
        import PySide2 # QT5, Max 2017+, Maya
        from PySide2 import QtGui, QtWidgets, QtCore
        #print("[WM] import PySide2")
    except ImportError as e:
        try:
            import PySide # QT4, Max 2015
            from PySide import QtGui, QtCore
            from PySide import QtGui as QtWidgets
            #print("[WM] import PySide")
        except ImportError as e: # Exit since no Qt libraries are available
            print("[WM] ERROR: {}".format(e))
            exit()
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region WindowManager
#
class WindowManager(object):
    """
    [summary]
    """
    _override_dcc_window = None # for unit tests

    # ----------------------------------------------------------------------------------------------------------------------
    #region Initialize
    #
    def __init__(self, parent=None):
        super(WindowManager, self).__init__(parent=parent)
        output = ""
        self.debug = True
        self._parent = parent
        if WindowManager._override_dcc_window:
            self._parent = WindowManager._override_dcc_window
        self._default_size = [500, 500]
        self._settings_path = None # os.path.join(os.path.expanduser("~"), "Company", self.dcc_platform.capitalize(), "WindowSettings.ini") # Set preferred settings location

        # DCC Platform
        try: # 3D Studio Max
            import MaxPlus
            self.dcc_platform = MAX
            version_number = MaxPlus.Core.EvalMAXScript('maxVersion()').Get()[0].Get() # 10000 is Max 2008 (it is the successor to 3ds Max 9), 11000 is Max 2009 and so on
            self.dcc_version = (version_number / 1000) + 2008 - 10
        except ImportError:
            try:
                import maya
                from maya import OpenMayaUI, cmds
                self.dcc_platform = MAYA
                self.dcc_version = int(maya.cmds.about(version=True))
            except ImportError:
                self.dcc_platform = STANDALONE
                self.dcc_version = 0
        output += "DCC Platform: {}, {}\n".format(self.dcc_platform, self.dcc_version)

        # Add the window to the parent app window, restoring saved settings if they exist, or default to center of the screen
        # These should reside in the main class' init()
        #self.add_window()
        #self.restore_window()

        #self.log(output, parent, prefix="WM")
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities
    #
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Window Functions
    #
    def add_window(self):
        """Adds the window to the parent application's window list."""
        # output = ""

        # Check if there is a parent app, and if it has a window list
        if self._parent:
            self.remove_window() # remove if an instance exists

            # Add window - this is to prevent the window from being destroyed during garbage collection
            # Alternative method to the usual _GCProtector() method
            self._parent._windows.append(self) #pylint: disable=protected-access

        # Log(output, self)

    def remove_window(self):
        """Removes the window (by class name) from the parent application's window list."""
        output = ""

        if self._parent:
            # Check if that window class exists (by name, should be unique)
            # The problem is that each time the script is run and the window class is defined, it is seen as a different
            # class UID type. So we should use unique class names instead of a generic "class Window():..." - that way this
            # can compare by the class name. Does mean it is limited to one window of that type at a time though.
            remove_list = []
            for w in self._parent._windows: # pylint: disable=W0212
                output += "\t- {0}: {1} ".format(w, w.__class__.__name__)
                if w.__class__.__name__ == self.__class__.__name__:
                    output += "<- REMOVE"
                    remove_list.append(w)
                output += "\n"

            for w in remove_list:
                self._parent._windows.remove(w) # pylint: disable=W0212
                w.close()

        # Log(output, self)

    def restore_window(self):
        """Restores the saved window size and position."""
        output = ""

        # Default Window Size
        if hasattr(self, "defaultSize"):
            default_width = self._default_size[0]
            default_height = self._default_size[1]
        else:
            default_width = 500
            default_height = 500

        # Screen Resolution
        app = QtWidgets.QApplication.instance()
        # if app:
        #     screen_size = app.desktop().screenGeometry()
        #     default_x = (screen_size.width()/2) - (default_width/2)
        #     default_y = (screen_size.height()/2) - (default_height/2)
        # else:
        default_x = 1920
        default_y = 1080

        # Load saved window settings from prefs
        settings_file = WindowManager.get_settings_file(self)
        settings_obj = QtCore.QSettings(settings_file, QtCore.QSettings.IniFormat)

        # Load Window Settings, restoring window geometry
        settings_obj.beginGroup(self.__class__.__name__)
        width = int(settings_obj.value("width", default_width))
        height = int(settings_obj.value("height", default_height))
        x = int(settings_obj.value("x", default_x))
        y = int(settings_obj.value("y", default_y))
        output += "\t- Size: {0}, {1}\n".format(width, height)
        output += "\t- Position: {0}, {1}\n".format(x, y)

        # Other Window Setting
        if isinstance(self, QtWidgets.QDockWidget):
            floating = True if settings_obj.value("floating", False).lower() == "true" else False
            self.setFloating(floating) # pylint: disable=no-member
            output += "\t- Floating: {} = {}\n".format(floating, self.isFloating()) # pylint: disable=no-member

        # Close the settings file
        settings_obj.endGroup()

        self.setGeometry(x, y, width, height) # pylint: disable=no-member
        # Log(output, settings_file)

    def save_window(self):
        """Saves the window's size and position."""
        output = ""
        settings_file = WindowManager.get_settings_file(self)

        # See: https://doc.qt.io/archives/qt-4.8/qsettings.html#restoring-the-state-of-a-gui-application

        settings_obj = QtCore.QSettings(settings_file, QtCore.QSettings.IniFormat)
        settings_obj.beginGroup(self.__class__.__name__)
        settings_obj.setValue("width", self.geometry().width()) # pylint: disable=no-member
        settings_obj.setValue("height", self.geometry().height()) # pylint: disable=no-member
        settings_obj.setValue("x", self.geometry().x()) # pylint: disable=no-member
        settings_obj.setValue("y", self.geometry().y()) # pylint: disable=no-member
        output += "\t- Size: {0}, {1}\n".format(self.geometry().width(), self.geometry().height()) # pylint: disable=no-member
        output += "\t- Position: {0}, {1}\n".format(self.geometry().x(), self.geometry().y()) # pylint: disable=no-member

        # Other Window Settings - Docked etc.
        # output += "\t- Classes:\n"
        # for base in self.__class__.__bases__:
        #     output += "\t\t- {}\n".format(base.__name__)
        if isinstance(self, QtWidgets.QDockWidget):
            settings_obj.setValue("floating", self.isFloating()) # pylint: disable=no-member
            output += "\t- Floating: {}\n".format(self.isFloating()) # pylint: disable=no-member
            features = self.features() # pylint: disable=no-member
            help(features)
            #output += "\t- Dock Features: {}\n".format()

        # Close the settings file
        settings_obj.endGroup()

        # Log(output, settings_file)

    def reset_window(self):
        """Resets the window to its default size and centers the window to the primary screen."""
        output = ""

        # Default Window Size
        if hasattr(self, "defaultSize"):
            default_width = self._default_size[0]
            default_height = self._default_size[1]
        else:
            default_width = 500
            default_height = 500

        # Screen Resolution
        app = QtWidgets.QApplication.instance()
        if app:
            screen_size = app.desktop().screenGeometry()
            # Center the window based on its size
            default_x = (screen_size.width()/2) - (default_width/2)
            default_y = (screen_size.height()/2) - (default_height/2)
        else:
            default_x = 1920
            default_y = 1080

        output += "\t- Size: {0}, {1}\n".format(default_width, default_height)
        output += "\t- Position: {0}, {1}\n".format(default_x, default_y)

        self.setGeometry(default_x, default_y, default_width, default_height) # pylint: disable=no-member
        self.save_window() # Also save these settings
        Log(output)

    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Events - QWindows
    #
    def closeEvent(self, event): #pylint: disable=invalid-name
        """
        Inherited from QWidget, runs when the window has closed.

        Arguments:
            event {[type]} -- [description]
        """
        output = ""
        output += "Event: {}".format(event)

        # Save Window Settings and remove from the parent app window
        self.save_window()
        self.remove_window()

        #Log(output)

    def resizeEvent(self, event): #pylint: disable=invalid-name
        """
        Inherited from QWidget, runs when the window has been resized.

        Arguments:
            event {[type]} -- [description]
        """
        output = ""
        output += "{}x{}".format(event.size().width(), event.size().height())

        #Log(output)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Static Methods
    #
    @staticmethod
    def get_dcc_window():
        """
        Returns the handle of the applications's main QWidget window, if available.

        Returns:
            [type] -- The handle to the application's main QWidget window. If not found or in standalone mode, returns None.
        """
        dcc_window = None

        # For unit tests
        if WindowManager._override_dcc_window:
            return WindowManager._override_dcc_window

        try:
            import MaxPlus
            version = MaxPlus.Core.EvalMAXScript('maxVersion()').Get()[0].Get() # 10000 is Max 2008 (it is the successor to 3ds Max 9), 11000 is Max 2009 and so on
            version = (version / 1000) + 2008 - 10
            if version >= 2018:
                dcc_window = MaxPlus.GetQMaxMainWindow()
            elif version == 2017:
                dcc_window = MaxPlus.GetQMaxWindow()
            else: # <= 2016
                dcc_window = None
        except ImportError:
            try:
                import maya
                from maya import OpenMayaUI, cmds
                from shiboken2 import wrapInstance
                ptr = OpenMayaUI.MQtUtil.mainWindow()
                dcc_window = wrapInstance(int(ptr), QtWidgets.QWidget)
            except ImportError:
                dcc_window = None

        if dcc_window and not hasattr(dcc_window, "_windows"): # Does not have a window list, create one
            dcc_window._windows = [] # pylint: disable=W0212

        return dcc_window

    @staticmethod
    def get_settings_file(widget):
        """Get the preferred storage location for window settings."""
        output = ""

        app_name = "Python"
        if hasattr(widget, "DCCPLATFORM"):
            app_name = widget.DCCPLATFORM.capitalize()

        target_directory = os.path.expanduser("~")
        output += "\t- Home Directory: {0}\n".format(target_directory)

        target_file = os.path.join(target_directory, app_name, "WindowSettings.ini")
        output += "\t- Target Settings File: {0}\n".format(target_file)

        if hasattr(widget, "_settings_path"):
            return widget._settings_path # pylint: disable=W0212

        #print output
        return target_file

    @staticmethod
    def clear_windows():
        """Clears the current app of any child windows and closes them."""
        dcc_window = WindowManager.get_dcc_window()
        if dcc_window:
            output = ""

            # Store windows into a new list, as when we start removing them, the actual list order will change
            window_list = []
            for w in dcc_window._windows: # pylint: disable=W0212
                window_list.append(w)

            for w in window_list:
                output += "\t- {0}\n".format(w)
                w.close()
        else:
            output += " - Missing dcc_window reference."

    @staticmethod
    def get_windows():
        """Gets a list of all the active windows in the WindowManager."""
        dcc_window = WindowManager.get_dcc_window()
        if dcc_window:
            return dcc_window._windows # pylint: disable=W0212
        else:
            return []

    #endregion
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region WindowManager Manager
#
class WindowManagerManager(WindowManager, QtWidgets.QMainWindow):
    """Manages the window list within a DCC. Will exit in standalone mode."""

    # ------------------------------------------------------------------------------------------------------------------
    #region Initialize
    #
    def __init__(self, parent=None):
        super(WindowManagerManager, self).__init__(parent=WindowManager.get_dcc_window() if parent is None else parent)
        output = ""
        self.debug = True
        self._settings_path = os.path.join(os.path.expanduser("~"), "Ubisoft", self.dcc_platform.capitalize(), "WindowSettings.ini") # Set preferred settings location

        # Initialize other properties here
        self._default_size = [300, 500]

        self.item_context_menu = None
        self.all_items = None

        # UI
        self.init_layout()
        # Add the window to the parent app window, restoring saved settings if they exist, or default to center of the screen
        self.add_window()
        self.restore_window()
        self.show()

        self.log(output, parent)

    def init_layout(self):
        """Initializes this window's contents."""

        output = ""

        # Button Layout
        # self.open_all_button = QtWidgets.QPushButton(self.tr("&Open All"))
        # self.open_all_button.setMinimumWidth(50)
        # self.close_all_button = QtWidgets.QPushButton(self.tr("&Close All"))
        # self.close_all_button.setMinimumWidth(50)
        # self.minimize_all_button = QtWidgets.QPushButton(self.tr("&Minimize All"))
        # self.minimize_all_button.setMinimumWidth(50)
        # self.maximize_all_button = QtWidgets.QPushButton(self.tr("&Maximize All"))
        # self.maximize_all_button.setMinimumWidth(50)
        self.refresh_button = QtWidgets.QPushButton(self.tr("&Refresh"))
        self.refresh_button.setMinimumWidth(50)

        # self.open_all_button.clicked.connect(self.on_show_all)
        # self.close_all_button.clicked.connect(self.on_close_all)
        # self.minimize_all_button.clicked.connect(self.onMinAll)
        # self.maximize_all_button.clicked.connect(self.onMaxAll)
        self.refresh_button.clicked.connect(self.on_refresh)

        self.button_layout = QtWidgets.QHBoxLayout()
        # self.button_layout.addWidget(self.open_all_button)
        # self.button_layout.addSpacing(10)
        # self.button_layout.addWidget(self.close_all_button)
        # self.button_layout.addSpacing(10)
        # self.button_layout.addWidget(self.minimize_all_button)
        # self.button_layout.addSpacing(10)
        # self.button_layout.addWidget(self.maximize_all_button)
        # self.button_layout.addSpacing(10)
        self.button_layout.addWidget(self.refresh_button)

        # Window List
        self.window_list = QtWidgets.QListWidget()
        self.window_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.window_list.connect(self.window_list, QtCore.SIGNAL("customContextMenuRequested(QPoint)"), self.on_right_click)

        # Main Layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.main_layout.addSpacing(10)
        self.main_layout.addLayout(self.button_layout)
        self.main_layout.addSpacing(10)
        self.main_layout.addWidget(self.window_list)
        self.main_layout.addSpacing(10)

        self.main_widget = QtWidgets.QWidget()
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)

        # Window Settings
        self.setWindowTitle("Window Manager")
        self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        self.setMinimumSize(270, 300)
        self.setMaximumSize(500, 1000)
        #self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

        self.log(output)
    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Window Events
    #
    def resizeEvent(self, event):
        """Inherited from QWidget, handles when the window has been resized.

        Arguments:
            event {[type]} -- [description]
        """
        output = ""
        output += "{}, {}".format(event.size().width(), event.size().height())
        self.log(output)
    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Window Item List
    #
    def create_window_item(self, window_item):
        """Creates a QtWidgets.QListWidgetItem object for that window and returns it.

        Arguments:
            window_item {QtWidget.QWidget} -- The QWidget window class to create an item for the window list.

        Returns:
            QtWidgets.QListWidgetItem -- The QListWidgetItem to be added to a QList.
        """

        #https://doc.qt.io/qt-5/qlistwidgetitem.html
        #QListWidgetItem(const QString &text, QListWidget *parent = nullptr, int type = Type)

        window_name = window_item.__class__.__name__

        # Get state of window - The window state is a OR'ed combination of Qt::WindowState: Qt::WindowMinimized, Qt::WindowMaximized, Qt::WindowFullScreen, and Qt::WindowActive.
        window_state = window_item.windowState()
        states = []
        if window_state & QtCore.Qt.WindowNoState:
            states.append("nostate")
        if window_state & QtCore.Qt.WindowMinimized:
            states.append("minimized")
        if window_state & QtCore.Qt.WindowMaximized:
            states.append("maximized")
        if window_state & QtCore.Qt.WindowFullScreen:
            states.append("fullscreen")
        if window_state & QtCore.Qt.WindowActive:
            states.append("active")

        if states:
            window_name += " ({})".format(",".join(states))

        item = QtWidgets.QListWidgetItem(window_name)

        # Save a reference to this window to the item
        item.window = window_item

        return item
    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Context Menu
    #
    def on_right_click(self, position):
        """Brings up the right click context menu.

        Arguments:
            position {QtCore.QPoint} -- Vector2 of the screen position of the mouse click.
        """
        output = ""

        item = self.window_list.currentItem()
        output += "- Selected {}: {}\n".format(item.text(), item.window)

        self.item_context_menu = QtWidgets.QMenu()
        show_item = self.item_context_menu.addAction("Show Window")
        hide_item = self.item_context_menu.addAction("Hide Window")
        min_item = self.item_context_menu.addAction("Minimize Window")
        max_item = self.item_context_menu.addAction("Maximize Window")
        reset_item = self.item_context_menu.addAction("Reset Window")
        close_item = self.item_context_menu.addAction("Close Window")

        self.item_context_menu.addSeparator()
        self.all_items = self.item_context_menu.addMenu("All Windows")
        show_all_items = self.all_items.addAction("Show All")
        hide_all_items = self.all_items.addAction("Hide All")
        min_all_items = self.all_items.addAction("Minimize All")
        max_all_items = self.all_items.addAction("Maximize All")
        reset_all_items = self.all_items.addAction("Reset All")
        close_all_items = self.all_items.addAction("Close All")
        self.item_context_menu.addSeparator()
        refresh_items = self.item_context_menu.addAction("Refresh List")

        # Connect Events
        show_item.triggered.connect(self.on_show_item)
        hide_item.triggered.connect(self.on_hide_item)
        min_item.triggered.connect(self.on_minimize_item)
        max_item.triggered.connect(self.on_maximize_item)
        reset_item.triggered.connect(self.on_reset_item)
        close_item.triggered.connect(self.on_close_item)
        refresh_items.triggered.connect(self.on_refresh)

        show_all_items.triggered.connect(self.on_show_all)
        hide_all_items.triggered.connect(self.on_hide_all)
        min_all_items.triggered.connect(self.on_minimize_all)
        max_all_items.triggered.connect(self.on_maximize_all)
        reset_all_items.triggered.connect(self.on_reset_all)
        close_all_items.triggered.connect(self.on_close_all)

        refresh_items.triggered.connect(self.on_refresh)

        # Display the context menu at the mouse point
        self.item_context_menu.move(self.window_list.mapToGlobal(QtCore.QPoint(0, 0)) + position)
        self.item_context_menu.show()

        self.log(output, item.text())
    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Context Menu Events - Selected Window
    #
    def on_show_item(self):
        """Closes the currently selected item in the window list."""
        output = ""
        item = self.window_list.currentItem()
        item.window.setWindowState(QtCore.Qt.WindowNoState)
        item.window.show()
        self.log(output, item.text())

    def on_hide_item(self):
        """Hides the currently selected item in the window list."""
        output = ""
        item = self.window_list.currentItem()
        item.window.hide()
        self.log(output, item.text())

    def on_minimize_item(self):
        """Minimizes the currently selected item in the window list."""
        output = ""
        item = self.window_list.currentItem()
        item.window.setWindowState(QtCore.Qt.WindowMinimized)
        self.log(output, item.text())

    def on_maximize_item(self):
        """Maximizes the currently selected item in the window list."""
        output = ""
        item = self.window_list.currentItem()
        item.window.setWindowState(QtCore.Qt.WindowMaximized)
        self.log(output, item.text())

    def on_reset_item(self):
        """Resets the currently selected item in the window list so it is at it's initial size and positioned at the center of the screen."""
        output = ""
        item = self.window_list.currentItem()
        item.window.reset_window()
        self.log(output, item.text())

    def on_close_item(self):
        """Closes the currently selected item in the window list."""
        output = ""
        item = self.window_list.currentItem()
        item.window.close()
        self.log(output, item.text())
    #endregion

    # ------------------------------------------------------------------------------------------------------------------
    #region Context Menu Events - All Windows
    #
    def on_refresh(self):
        """Refreshes the window item list."""
        output = ""

        # Clear the item list
        self.window_list.clear()

        # Collect our windows, and sort the list
        windows = []
        if self._parent:
            for w in self._parent._windows: # pylint: disable=W0212
                output += "\t- {}\n".format(w.__class__.__name__)
                windows.append(w)

            def get_item_class_name(item):
                return item.__class__.__name__

            windows.sort(key=get_item_class_name)
            # Remove the manager and insert into the front
            windows.remove(self)
            windows.insert(0, self)

            for w in windows:
                item = self.create_window_item(w)
                self.window_list.addItem(item)

        self.log(output)

    def on_show_all(self):
        """Unhides and displays all windows."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            item.window.setWindowState(QtCore.Qt.WindowNoState)
            item.window.show()
        self.log(output)

    def on_hide_all(self):
        """Hides all windows, except itself."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            if item.window is self:
                continue
            item.window.hide()
        self.log(output)

    def on_minimize_all(self):
        """Minimizes all windows, except itself."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            if item.window is self:
                continue
            item.window.setWindowState(QtCore.Qt.WindowMinimized)
        self.log(output)

    def on_maximize_all(self):
        """Maximizes all windows, except itself."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            if item.window is self:
                continue
            item.window.setWindowState(QtCore.Qt.WindowMaximized)
        self.log(output)

    def on_reset_all(self):
        """Resets all windows so they're at their default size in the middle of the screen."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            item.window.reset_window()
        self.log(output)

    def on_close_all(self):
        """Closes all windows, except itself."""
        output = ""
        for i in range(0, self.window_list.count()):
            item = self.window_list.item(i)
            if item.window is self:
                continue
            item.window.close()
        self.log(output)

    #endregion

#endregion

# ------------------------------------------------------------------------------------------------------------------
#region __main__
#
def main():
    """main()"""
    qapp = QtWidgets.QApplication.instance()
    print("App: {}".format(qapp))
    if qapp is not None: # App is running under another app environment, i.e. 3dsMax or Maya
        window = WindowManagerManager() #pylint: disable=unused-variable
    else: # Standalone
        qapp = QtWidgets.QApplication(sys.argv)
        window = WindowManagerManager()
        sys.exit(qapp.exec_())

if __name__ == "__main__":
    main()

#endregion
