"""
Demo for QMainWindow/QDockWidget

Default behaviour:

- Can dock the QDockWidgets on the left, right, top,
  and bottom of the QMainWindow
- Can tabify when overlapping another QDockWidget
    - Displays a highlight overlay over the target dock area
- QDockWidgets can toggle floating or docked
- QDockWidgets can snap to the sides of its parent
  QMainWindow or sibling QDockWidgets

- Cannot dock/tabify over other floating QDockWidgets
- Snapped QDockWidgets don't move with the QMainWindow
  or QDockWidget it is snapped to
- Can't move QDockWidgets between QMainWindow parents
    - Won't

"""
import os
import sys

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

from lib.Vector import Vector

# ----------------------------------------------------------------------------------------------------------------------
# region Classes
#
class DemoQMainWindow(QtWidgets.QMainWindow):
    """
    https://doc.qt.io/qt-5/qmainwindow.html

    Args:
        QtWidgets ([type]): [description]
    """
    def __init__(self, parent=None, objectName="QMainWindow"):
        """init"""
        super(DemoQMainWindow, self).__init__(parent=parent)

        if objectName:
            self.setObjectName(objectName)
            self.setWindowTitle(objectName)

        self.init_ui()

        # Set window to center of screen
        center = self.screen().geometry().center()
        size = self.screen().availableGeometry().size()
        size.setWidth(size.width() * 0.3)
        size.setHeight(size.height() * 0.5)
        self.move(center.x() - (size.width()/2), center.y() - (size.height()/2))
        self.resize(size)

        self.CreateDockWidgets(10)

    def init_ui(self):
        """init_ui"""
        self.setObjectName("QMainWindow QDockWidget Demo")
        self.setTabPosition(QtCore.Qt.AllDockWidgetAreas, QtWidgets.QTabWidget.West)
        self.setDockNestingEnabled(True) # QtWidgets.QMainWindow.AllowNestedDocks | QtWidgets.QMainWindow.AllowTabbedDocks)
        self.init_menubar()

    def CreateDockWidgets(self, count):
        """CreateDockWidgets"""
        prev_widget = None
        dockarea = QtCore.Qt.RightDockWidgetArea

        for i in range(count):
            dockwidget = DemoQDockWidget(parent=self, objectName="QDockWidget {}".format(i))
            self.addDockWidget(dockarea, dockwidget)
            if not prev_widget:
                prev_widget = dockwidget
            else:
                self.tabifyDockWidget(prev_widget, dockwidget)

    def init_menubar(self):
        """
        Create and initializes the menubar.
        """
        #pylint: disable=unused-variable
        menubar = self.menuBar()

        # File
        menu_file = menubar.addMenu("File")
        menu_file_new = menu_file.addAction("New")
        menu_file_save = menu_file.addAction("Save")
        menu_file_quit = menu_file.addAction("Quit")

        # Windows
        menu_windows = menubar.addMenu("Windows")
        menu_windows_reset = menu_windows.addAction("Reset to Center")
        menu_windows_add = menu_windows.addAction("Add Child")
        menu_windows_remove = menu_windows.addAction("Remove Child")

        # Options
        menu_options = menubar.addMenu("Options")
        menu_options_something = menu_options.addAction("Some Option")

        # Help
        menu_help = menubar.addAction("Help")

class DemoQDockWidget(QtWidgets.QDockWidget):
    """
    https://doc.qt.io/qt-5/qdockwidget.html

    Args:
        QtWidgets ([type]): [description]
    """
    def __init__(self, parent=None, objectName="Demo QDockWidget"):
        """init"""
        super(DemoQDockWidget, self).__init__(parent=parent)

        self.setObjectName(objectName) # required to allow for window state save/restore
        self.setWindowTitle(objectName)
        self.init_ui()

    def init_ui(self):
        """init_ui"""

        self.textedit = QtWidgets.QTextEdit()
        self.textedit.setText(self.objectName())

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.textedit)

        self.widget = QtWidgets.QWidget()
        self.widget.setLayout(self.layout)
        self.setWidget(self.widget)
        # self.setGeometry(0, 0, 640, 480)

    # def resizeEvent(self, event):
    #     """resizeEvent()"""
    #     self.widget.setGeometry(0, 30, event.size().width(), event.size().height())

#endregion Classes

# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def main():
    """Main"""
    output_text = []

    # App Window
    qapp = QtWidgets.QApplication.instance()
    output_text += ["QApp: {}".format(qapp)]
    if qapp is None: # App is not running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)

    # Display the window
    window = DemoQMainWindow(objectName="QMainWindow 1")
    window.show()
    window1 = DemoQMainWindow(objectName="QMainWindow 2")
    window1.show()

    sys.exit(qapp.exec_())

# print("__name__: {}".format(__name__))
if __name__ in ["__main__", "<module>", "demo_dockwidgets"]:
    # main()

    source_map_worldoffset = Vector(-3072.0, 2048.0, 0)
    target_map_worldoffset = Vector(-512.0, -512.0, 0)

    hidShapePoints = "Count(4) -2364.14,2536.05,323.953;-2357.24,2535.27,323.953;-2356.72,2540.78,323.953;-2363.59,2541.3,323.953;"
    points_count, shape_points = hidShapePoints.split(" ", 2)
    new_hidShapePoints = "{} ".format(points_count)
    shape_points = shape_points.split(";")
    for shape_point in shape_points:
        if shape_point:
            shape_point = Vector(shape_point)
            point = (shape_point - source_map_worldoffset) + target_map_worldoffset
            point.rounded(2)

            print("Point: {} -> {}".format(shape_point, point))
            new_hidShapePoints += "{},{},{};".format(point.x, point.y, point.z)

    print("Converted: {}".format(new_hidShapePoints))

    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")


#endregion Main
