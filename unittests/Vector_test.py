"""
Unit tests for the Vector class
"""
# --------------------------------------------------------------------------------------------------
#region Modules
#
import unittest
from lib.Vector import Vector
#endregion


# --------------------------------------------------------------------------------------------------
#region Vector Unit Tests
#
class Test_Vector_Constructor(unittest.TestCase, Vector):
    """
    Testing constructing the Vector class.
    """
    #pylint: disable=missing-function-docstring

    def test_construct_1(self):
        v = Vector(1)
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 0.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_2(self):
        v = Vector(1, 2)
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_3(self):
        v = Vector(1, 2, 3)
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, None)

    def test_construct_4(self):
        v = Vector(1, 2, 3, 4)
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, 4.0)

    def test_construct_string_1(self):
        v = Vector("1")
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 0.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_string_2(self):
        v = Vector("1, 2")
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_string_3(self):
        v = Vector("1, 2, 3")
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, None)

    def test_construct_string_4(self):
        v = Vector("1, 2, 3, 4")
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, 4.0)

    def test_construct_list_1(self):
        v = Vector([1])
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 0.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_list_2(self):
        v = Vector([1, 2])
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

    def test_construct_list_3(self):
        v = Vector([1, 2, 3])
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, None)

    def test_construct_list_4(self):
        v = Vector([1, 2, 3, 4])
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, 4.0)

    def test_construct_vector_1(self):
        source = Vector(1)
        v = Vector(source)
        self.assertEqual(v.x, source.x)
        self.assertEqual(v.y, 0.0)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

        # And test the values are separate from the source (copy, not reference)
        v.x = 2
        v.y = 3
        self.assertNotEqual(v.x, source.x)
        self.assertNotEqual(v.y, source.y)

    def test_construct_vector_2(self):
        source = Vector(1, 2)
        v = Vector(source)
        self.assertEqual(v.x, source.x)
        self.assertEqual(v.y, source.y)
        self.assertEqual(v.z, None)
        self.assertEqual(v.w, None)

        # And test the values are separate from the source (copy, not reference)
        v.x = 2
        v.y = 3
        self.assertNotEqual(v.x, source.x)
        self.assertNotEqual(v.y, source.y)

    def test_construct_vector_3(self):
        source = Vector(1, 2, 3)
        v = Vector(source)
        self.assertEqual(v.x, source.x)
        self.assertEqual(v.y, source.y)
        self.assertEqual(v.z, source.z)
        self.assertEqual(v.w, None)

        # And test the values are separate from the source (copy, not reference)
        v.x = 2
        v.y = 3
        v.z = 4
        self.assertNotEqual(v.x, source.x)
        self.assertNotEqual(v.y, source.y)
        self.assertNotEqual(v.z, source.z)

    def test_construct_vector_4(self):
        source = Vector(1, 2, 3, 4)
        v = Vector(source)
        self.assertEqual(v.x, source.x)
        self.assertEqual(v.y, source.y)
        self.assertEqual(v.z, source.z)
        self.assertEqual(v.w, source.w)

        # And test the values are separate from the source (copy, not reference)
        v.x = 2
        v.y = 3
        v.z = 4
        v.w = 5
        self.assertNotEqual(v.x, source.x)
        self.assertNotEqual(v.y, source.y)
        self.assertNotEqual(v.z, source.z)
        self.assertNotEqual(v.w, source.w)

class Test_Vector_Operators(unittest.TestCase, Vector):
    """
    Testing operators: + - / * += -= /= */ // ** abs len
    """
    #pylint: disable=missing-function-docstring

    def test_math_op_vector(self):
        a = Vector(1, 2, 3, 4)
        b = Vector(1, 2, 3, 4)
        self.assertEqual(a + b, Vector(2, 4, 6, 8))
        self.assertEqual(a - b, Vector(0, 0, 0, 0))
        self.assertEqual(a * b, Vector(1, 4, 9, 16))
        self.assertEqual(a / b, Vector(1, 1, 1, 1))
        self.assertEqual(a // b, Vector(1, 1, 1, 1))
        self.assertEqual(a ** b, Vector(1, 4, 27, 256))
        self.assertEqual(a % b, Vector(0, 0, 0, 0))

        a = Vector(1, 2, 3, 4)
        a += b
        self.assertEqual(a, Vector(2, 4, 6, 8))

        a = Vector(1, 2, 3, 4)
        a -= b
        self.assertEqual(a, Vector(0, 0, 0, 0))

        a = Vector(1, 2, 3, 4)
        a /= b
        self.assertEqual(a, Vector(1, 1, 1, 1))

        a = Vector(1, 2, 3, 4)
        a *= b
        self.assertEqual(a, Vector(1, 4, 9, 16))

    def test_math_op_float(self):
        a = Vector(1, 2, 3, 4)
        b = 2.0
        self.assertEqual(a + b, Vector(3, 4, 5, 6))
        self.assertEqual(a - b, Vector(-1, 0, 1, 2))
        self.assertEqual(a * b, Vector(2, 4, 6, 8))
        self.assertEqual(a / b, Vector(0.5, 1, 1.5, 2))
        self.assertEqual(a // b, Vector(0, 1, 1, 2))
        self.assertEqual(a ** b, Vector(1, 4, 9, 16))
        self.assertEqual(a % b, Vector(1, 0, 1, 0))

    def test_math_op_int(self):
        a = Vector(1, 2, 3, 4)
        b = 2
        self.assertEqual(a + b, Vector(3, 4, 5, 6))
        self.assertEqual(a - b, Vector(-1, 0, 1, 2))
        self.assertEqual(a * b, Vector(2, 4, 6, 8))
        self.assertEqual(a / b, Vector(0.5, 1, 1.5, 2))
        self.assertEqual(a // b, Vector(0, 1, 1, 2))
        self.assertEqual(a ** b, Vector(1, 4, 9, 16))
        self.assertEqual(a % b, Vector(1, 0, 1, 0))

    def test_math_op_vector_fail(self):
        a = Vector(1, 2, 3, 4)
        b = Vector(1, 2, 3)
        v = None
        with self.assertRaises(ValueError):
            v = a + b
        with self.assertRaises(ValueError):
            v = a - b
        with self.assertRaises(ValueError):
            v = a * b
        with self.assertRaises(ValueError):
            v = a / b
        with self.assertRaises(ValueError):
            v = a // b
        with self.assertRaises(ValueError):
            v = a ** b
        with self.assertRaises(ValueError):
            v = a % b
        print("Result: {}".format(v))

    def test_math_op_string_fail(self):
        a = Vector(1, 2, 3, 4)
        b = "something"
        v = None
        with self.assertRaises(ValueError):
            v = a + b
        with self.assertRaises(ValueError):
            v = a - b
        with self.assertRaises(ValueError):
            v = a * b
        with self.assertRaises(ValueError):
            v = a / b
        with self.assertRaises(ValueError):
            v = a // b
        with self.assertRaises(ValueError):
            v = a ** b
        with self.assertRaises(ValueError):
            v = a % b
        print("Result: {}".format(v))

    def test_math_neg(self):
        v = Vector(1, 2, 3, 4)
        v = -v
        self.assertEqual(v.x, -1.0)
        self.assertEqual(v.y, -2.0)
        self.assertEqual(v.z, -3.0)
        self.assertEqual(v.w, -4.0)

    def test_math_abs(self):
        v = Vector(1, -2, 3, -4)
        v = abs(v)
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 2.0)
        self.assertEqual(v.z, 3.0)
        self.assertEqual(v.w, 4.0)

    def test_math_len(self):
        v = Vector(1)
        self.assertEqual(len(v), 2)
        v = Vector(1, 2)
        self.assertEqual(len(v), 2)
        v = Vector(1, 2, 3)
        self.assertEqual(len(v), 3)
        v = Vector(1, 2, 3, 4)
        self.assertEqual(len(v), 4)

class Test_Vector_Comparisons(unittest.TestCase, Vector):
    """
    Testing comparisons: < <= == != > >=
    """
    #pylint: disable=missing-function-docstring

    def test_oper_lt(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(2, 4, 6, 8)
        r = v1 < v2
        self.assertEqual(r, True)

    def test_oper_lt_fail(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(1, 2, 3, 4)
        r = v1 < v2
        self.assertNotEqual(r, True)

    def test_oper_le(self):
        v1 = Vector(1, 2, 1, 2)
        v2 = Vector(1, 2, 1, 2)
        r = v1 <= v2
        self.assertEqual(r, True)

    def test_oper_le_fail(self):
        v1 = Vector(5, 5, 5, 5)
        v2 = Vector(1, 1, 1, 1)
        r = v1 <= v2
        self.assertNotEqual(r, True)

    def test_oper_eq(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(1, 2, 3, 4)
        r = v1 == v2
        self.assertEqual(r, True)

    def test_oper_eq_fail(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(4, 3, 2, 1)
        r = v1 == v2
        self.assertNotEqual(r, True)

    def test_oper_ne(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(4, 3, 2, 1)
        r = v1 != v2
        self.assertEqual(r, True)

    def test_oper_ne_fail(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(1, 2, 3, 4)
        r = v1 != v2
        self.assertNotEqual(r, True)

    def test_oper_gt(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(.9, 1, 2, 3)
        r = v1 > v2
        self.assertEqual(r, True)

    def test_oper_gt_fail(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(2, 3, 3, 1)
        r = v1 > v2
        self.assertNotEqual(r, True)

    def test_oper_ge(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(1, 0, 3, 4)
        r = v1 >= v2
        self.assertEqual(r, True)

    def test_oper_ge_fail(self):
        v1 = Vector(1, 2, 3, 4)
        v2 = Vector(1, 5, 3, 6)
        r = v1 >= v2
        self.assertNotEqual(r, True)

class Test_Vector_Methods(unittest.TestCase, Vector):
    """
    Testing the Vector methods
    """
    #pylint: disable=missing-function-docstring

    # --------------------------------------------------------------------------------------------------
    #region Methods
    #
    def test_method_magnitude(self):
        v = Vector(1)
        self.assertEqual(v.magnitude(), 1.0)
        v = Vector(1, 2)
        self.assertEqual(v.magnitude(), 2.23606797749979)
        v = Vector(1, 2, 3)
        self.assertEqual(v.magnitude(), 3.7416573867739413)
        v = Vector(1, 2, 3, 4)
        self.assertEqual(v.magnitude(), 5.477225575051661)

    def test_method_normalized(self):
        v = Vector(1, 2, 3, 4)
        v1 = v.normalized()
        self.assertEqual(v1.x, 0.18257418583505536)
        self.assertEqual(v1.y, 0.3651483716701107)
        self.assertEqual(v1.z, 0.5477225575051661)
        self.assertEqual(v1.w, 0.7302967433402214)

    def test_method_normalize(self):
        v = Vector(1, 2, 3, 4)
        v.normalize()
        self.assertEqual(v.x, 0.18257418583505536)
        self.assertEqual(v.y, 0.3651483716701107)
        self.assertEqual(v.z, 0.5477225575051661)
        self.assertEqual(v.w, 0.7302967433402214)

    def test_method_round(self):
        r = Vector(1.1111, 2.8888, 3.5555, 4.0)
        v = r.round()
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 3.0)
        self.assertEqual(v.z, 4.0)
        self.assertEqual(v.w, 4.0)

    def test_method_round_two_decimal_places(self):
        r = Vector(1.1111, 2.8888, 3.5555, 4.0)
        v = r.round(2)
        self.assertEqual(v.x, 1.11)
        self.assertEqual(v.y, 2.89)
        self.assertEqual(v.z, 3.56)
        self.assertEqual(v.w, 4.0)

    def test_method_round_point5(self):
        r = Vector(1.1111, 2.8888, 3.5555, 4.4) * 2
        v = r.round() / 2
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 3.0)
        self.assertEqual(v.z, 3.5)
        self.assertEqual(v.w, 4.5)

    def test_method_rounded(self):
        v = Vector(1.1111, 2.8888, 3.5555, 4.0)
        v.rounded()
        self.assertEqual(v.x, 1.0)
        self.assertEqual(v.y, 3.0)
        self.assertEqual(v.z, 4.0)
        self.assertEqual(v.w, 4.0)

    def test_method_rounded_two_decimal_places(self):
        v = Vector(1.1111, 2.8888, 3.5555, 4.0)
        v.rounded(2)
        self.assertEqual(v.x, 1.11)
        self.assertEqual(v.y, 2.89)
        self.assertEqual(v.z, 3.56)
        self.assertEqual(v.w, 4.0)

    def test_method_to_list(self):
        v = Vector(1, 2, 3, 4)
        l = v.to_list()

        self.assertEqual(l[0], 1)
        self.assertEqual(l[1], 2)
        self.assertEqual(l[2], 3)
        self.assertEqual(l[3], 4)

    def test_method_to_list_reverse(self):
        v = Vector(1, 2, 3, 4)
        l = v.to_list(reverse=True)

        self.assertEqual(l[0], 4)
        self.assertEqual(l[1], 3)
        self.assertEqual(l[2], 2)
        self.assertEqual(l[3], 1)

    def test_method_to_list_sort(self):
        v = Vector(1, 4, 3, 2)
        l = v.to_list(sort=True)

        self.assertEqual(l[0], 1)
        self.assertEqual(l[1], 2)
        self.assertEqual(l[2], 3)
        self.assertEqual(l[3], 4)

    def test_method_to_list_sort_reverse(self):
        v = Vector(1, 4, 3, 2)
        l = v.to_list(sort=True, reverse=True)

        self.assertEqual(l[0], 4)
        self.assertEqual(l[1], 3)
        self.assertEqual(l[2], 2)
        self.assertEqual(l[3], 1)
    #endregion Methods

    # --------------------------------------------------------------------------------------------------
    #region Static Methods -
    #
    def test_static_method_cross(self):
        v1 = Vector(1, 2, 3)
        v2 = Vector(3, 2, 1)
        v = Vector.Cross(v1, v2)
        self.assertEqual(v.x, -4.0)
        self.assertEqual(v.y, 8.0)
        self.assertEqual(v.z, -4.0)

    def test_static_method_dot(self):
        v1 = Vector(1, 2, 3).normalized()
        v2 = Vector(3, 2, 1).normalized()
        d = Vector.Dot(v1, v2)
        self.assertEqual(d, 0.7142857142857143)

    def test_static_method_distance(self):
        o = Vector(0, 0, 0)
        v = Vector(5, 5, 5)
        d = Vector.Distance(o, v)
        self.assertEqual(d, 8.660254037844387)

    def test_static_method_min(self):
        vectors = []
        for i in range(10):
            vectors.append(Vector(i, i, i, i))
        v = Vector.Min(vectors)
        self.assertEqual(v, Vector(0, 0, 0, 0))

    def test_static_method_min_fail(self):
        vectors = []
        for i in range(10):
            vectors.append(Vector(i, i, i, i))
        vectors.append(Vector(5, 5))
        self.assertRaises(ValueError, Vector.Min, vectors)

    def test_static_method_max(self):
        vectors = []
        for i in range(10):
            vectors.append(Vector(i, i, i, i))
        v = Vector.Max(vectors)
        self.assertEqual(v, Vector(9, 9, 9, 9))

    def test_static_method_max_fail(self):
        vectors = []
        for i in range(10):
            vectors.append(Vector(i, i, i, i))
        vectors.append(Vector(5, 5))
        self.assertRaises(ValueError, Vector.Max, vectors)
    #endregion Static Methods

    # --------------------------------------------------------------------------------------------------
    #region Static Methods - Animation and Rotation
    #
    def test_static_method_lerp(self):
        a = Vector(0, 0, 0)
        b = Vector(8, 8, 8)
        v = Vector.Lerp(a, b, 0.5)
        self.assertEqual(v.x, 4.0)
        self.assertEqual(v.y, 4.0)
        self.assertEqual(v.z, 4.0)
        self.assertEqual(v.w, None)

    def test_static_method_rotatepoint(self):
        point = Vector(1, 0, 0)
        pitch = Vector(45, 0, 0)
        roll = Vector(0, 45, 0)
        yaw = Vector(0, 0, 60)

        # Vector.RotatePoint(<45.0, 0.0, 0.0>, <1.0, 0.0, 0.0>) = <1.0, 0.0, 0.0>
        v = Vector.RotatePoint(pitch, point)
        self.assertEqual(v, Vector(1.0, 0.0, 0.0))

        # Vector.RotatePoint(<0.0, 45.0, 0.0>, <1.0, 0.0, 0.0>) = <0.7071067811865475, 0.0, -0.7071067811865476>
        v = Vector.RotatePoint(roll, point)
        self.assertEqual(v, Vector(0.7071067811865475, 0.0, -0.7071067811865476))

        # Vector.RotatePoint(<0.0, 0.0, 60.0>, <1.0, 0.0, 0.0>) = <0.5000000000000001, 0.8660254037844386, 0.0>
        v = Vector.RotatePoint(yaw, point)
        self.assertEqual(v, Vector(0.5000000000000001, 0.8660254037844386, 0.0))

    def test_static_static_method_angle(self):
        f = Vector(1, 0, 0)
        t = Vector(1, 1, 0)
        a = Vector.Angle(f, t)
        self.assertEqual(a, 45.0)

    def test_static_method_angle_axis(self):
        f = Vector(1, 0, 0)
        t = Vector(1, 1, 0)
        a = Vector.Angle(f, t, Vector.up())
        self.assertEqual(a, 45.0)

    def test_static_method_angle_axis_reversed(self):
        f = Vector(1, 1, 0)
        t = Vector(1, 0, 0)
        a = Vector.Angle(f, t, Vector.up())
        self.assertEqual(a, -45.0)
    #endregion Static Methods

    # --------------------------------------------------------------------------------------------------
    #region Static Methods - Animation and Rotation
    #
    def test_static_method_linelineintersection(self):
        line1point = Vector(-1, 0, 0)
        line1vector = Vector(2, 0, 0)
        line2point = Vector(0, -1, 0)
        line2vector = Vector(0, 2, 0)
        point = Vector.LineLineIntersection(line1point, line1vector, line2point, line2vector)
        self.assertEqual(point, Vector(0, 0, 0))

    def test_static_method_closestpointintersection(self):
        line1point = Vector(-1, 0, 0)
        line1vector = Vector(2, 0, 0)
        line2point = Vector(0, -1, 0)
        line2vector = Vector(0, 2, 0)

        points = Vector.ClosestPointsOnTwoLines(line1point, line1vector, line2point, line2vector)
        self.assertEqual(points[0], Vector(0, 0, 0))
        self.assertEqual(points[1], Vector(0, 0, 0))

    #endregion Static Methods

#endregion Vector Unit Tests


# --------------------------------------------------------------------------------------------------
# Main
#
if __name__ == '__main__':
    unittest.main()
#endregion Main
