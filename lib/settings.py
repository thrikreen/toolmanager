"""
Container Module for holding global settings and variables.
"""
import os
import sys
import json
import argparse
import imp
import importlib
import inspect
import ctypes
import time
from datetime import datetime

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

try:
    from lib.Log import Log
except Exception as e: #pylint: disable=broad-except
    from Log import Log #type: ignore

# from lib.ToolDockWidget import ToolDockWidget # circular import

# ----------------------------------------------------------------------------------------------------------------------
#region Global Variables
#
APP_ROOT = None
SETTINGS = None
MODULE_PATH = None
MODULE_PATHS = []

QAPP = None

DCCPLATFORMS = ['STANDALONE', 'MAX', 'MAYA', 'BLENDER']
STANDALONE, MAX, MAYA, BLENDER = DCCPLATFORMS
DCCPLATFORM = None

USERPROFILE_PATH = os.path.expanduser("~")

DEFAULTTIMESTAMPFORMAT = '%Y-%m-%d %H:%M:%S.%f'

#endregion Global Variables

# ----------------------------------------------------------------------------------------------------------------------
#region General
#
def dir_path(string):
    """Checks if the supplied path is to a directory."""
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

def file_path(string):
    """Checks if the supplied path is pointing to an existing file."""
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)

def sanitize_path(s):
    """Sanitizes the supplied file path, removing attempts to go up a folder, making the slashes uniform and removing duplicates."""
    while ".." in s:
        s = s.replace("..", "")
    while "\\" in s:
        s = s.replace("\\", "/")
    while "//" in s:
        s = s.replace("//", "/")
    return s

def get_default_user_prefs(verbose=False):
    """
    Gets the default user .INI file path, defaults to c:/users/username/application/filename.ini

    Returns:
        [str]: The user prefs filepath
    """
    # Default values
    company = "company"
    application_name = "application"
    prefs_filename = "toolmanager.ini"

    # Customize for project
    company = "mycompany"
    application_name = "toolmanager"

    # user/app/
    userprefs_filepath = os.path.join(os.path.expanduser("~"), application_name, prefs_filename)

    # user/app/company/
    # userprefs_filepath = os.path.join(os.path.expanduser("~"), application_name, company, prefs_filename)

    # user/company/app/
    # userprefs_filepath = os.path.join(os.path.expanduser("~"), company, application_name, prefs_filename)

    if verbose:
        Log(userprefs_filepath, company, application_name, prefs_filename)
    return userprefs_filepath

def get_default_module_path():
    """
    Gets the default module path, in d:/app location/modules/

    Returns:
        str: Path to the modules folder.
    """
    moddir = os.path.join(APP_ROOT, "modules")
    return moddir

def Initialize(verbose=False):
    """
    Parses the commandline arguments passed to this program.
    Stores some values as globals for other modules to reference as needed.

    Returns:
        args: The argparse object.
    """
    output_text = []
    #pylint: disable=global-statement
    global APP_ROOT
    global SETTINGS
    global MODULE_PATH
    global MODULE_PATHS

    # Application Root
    APP_ROOT = sanitize_path(os.path.dirname(sys.argv[0])) # Default to the executed script file's location
    output_text += ["- App Root: {}".format(APP_ROOT)]

    # Commandline Arguments
    output_text += ["- Args:"]
    parser = argparse.ArgumentParser(description="Customizable Tool Manager for digital content creators (3dsMax, Maya, etc.)")
    parser.add_argument("-config", type=file_path, required=False, help="Specifies the location of the config file, otherwise it will default to the user's folder.")
    parser.add_argument("-module_path", type=dir_path, required=False, help="Specifies the location of the main module path, otherwise it defaults to this application's home folder.")
    parser.add_argument("-add_module_path", type=dir_path, required=False, help="Specifies the location of module paths to use, in addition to the main module path. Useful for keeping generic and project specific tool modules separate.")
    args = parser.parse_args()
    if args.config:
        output_text += ["\t-config={}".format(args.config)]
    if args.module_path:
        output_text += ["\t-module_path={}".format(args.module_path)]
    if args.add_module_path:
        output_text += ["\t-add_module_path={}".format(args.add_module_path)]

    # Config settings
    SETTINGS = None
    if args.config:
        args.config = sanitize_path(args.config)
        if os.path.isfile(args.config):
            SETTINGS = LoadSettings(overridesettingsfile=sanitize_path(args.config))
    if not SETTINGS:
        SETTINGS = LoadSettings()
    output_text += ["- Settings: {} -> {}".format(SETTINGS.fileName(), SETTINGS)]

    # Module Directory
    MODULE_PATH = None
    MODULE_PATHS = []
    if args.module_path:
        args.module_path = sanitize_path(args.module_path)
        if os.path.isdir(args.module_path):
            MODULE_PATH = args.module_path
            MODULE_PATHS.append(args.module_path)
    if not MODULE_PATH: # No paths set, default to the app root/modules/ folder
        MODULE_PATH = sanitize_path(get_default_module_path())
        MODULE_PATHS.append(sanitize_path(get_default_module_path()))

    if args.add_module_path:
        args.add_module_path = sanitize_path(args.add_module_path)
        if os.path.isdir(args.add_module_path):
            MODULE_PATHS.append(args.add_module_path)

    if verbose:
        output_text += ["- Module Root: {}".format(MODULE_PATH)]
        output_text += ["- Module Paths:"]
        for module_path in MODULE_PATHS:
            output_text += ["\t- {}".format(module_path)]

        # print("Initialize():\n{}".format("\n".join(output_text)))
        Log(output_text)

    return args
#endregion General

# ----------------------------------------------------------------------------------------------------------------------
#region Settings
#
def LoadSettings(overridesettingsfile=None, verbose=True):
    """
    Loads the QSettings file into a QSettings object and stores and returns it.
    See: https://doc.qt.io/qtforpython/PySide2/QtCore/QSettings.html

    Defaults to the current user directory, under username/company/app/app.ini file (might change it to user/company/app.ini?)

    Args:
        overridesettingsfile (path, optional): If set, uses that file instead of the default. Defaults to None.

    Returns:
        QSettings -- The QSettings object with the INI file loaded.
    """
    output = []
    settings = None
    userdir = get_default_user_prefs()

    if overridesettingsfile:
        userdir = overridesettingsfile

    try:
        settings = QtCore.QSettings(userdir, QtCore.QSettings.IniFormat)
    except Exception as e: #pylint: disable=broad-except
        Log("[ERROR] settings.LoadSettings({}): {}".format(userdir, e))
        import traceback
        traceback.print_exc()

    if verbose:
        Log(output, userdir)

    return settings

def GetSettings(group, key=None, default=None, verbose=False):
    """
    Reads the settings from the QSettings object

    Args:
        group (str, QMainWindow, ToolDockWidget): The group category to read the key/value pair under. If passed a QMainWindow, will use it's object name. If passed a ToolDockWidget, will use its class name.
        key (str, list, optional): If set, will return just those keys. If a QWidget, will default to using its objectName. Defaults to None.
        default (str, optional): If the key is not found, will use the specified default value.
        verbose (bool, optional): If True, will display the retrieved contents. Defaults to False.

    Returns:
        [dict]: Dict of the key/values, either all for that group, or only the specified ones.
    """
    output = []
    content = {}

    # Use class name or widget's object name?
    if isinstance(group, QtWidgets.QMainWindow): # ToolManager->QMainWindow
        group = group.objectName()
    elif isinstance(group, QtWidgets.QDockWidget): # ToolDockWidget->QDockWidget
        group = group.__class__.__name__

    if not SETTINGS or not isinstance(SETTINGS, QtCore.QSettings):
        output.append("- QSettings: {}".format(None if SETTINGS is None else "Invalid settings (not QSettings)."))
        Log(output, group, key)
        return content

    SETTINGS.beginGroup(group)

    if key and isinstance(key, QtWidgets.QWidget):
        content[key.objectName()] = SETTINGS.value(key.objectName())

    elif key and isinstance(key, str):
        if SETTINGS.contains(key):
            content[key] = SETTINGS.value(key)
            output.append("- {}: {}".format(key, content[key]))
        else:
            content[key] = default
            output.append("- {}: {} (default)".format(key, content[key]))

    elif key and isinstance(key, list):
        for k in key:
            if SETTINGS.contains(k):
                content[k] = SETTINGS.value(k)
                output.append("- {}: {}".format(k, content[k]))
            else:
                content[k] = default
                output.append("- {}: {} (default)".format(k, content[k]))

    else:
        keys = SETTINGS.allKeys()
        for k in keys:
            content[k] = SETTINGS.value(k)
            output.append("- {}: {}".format(k, content[k]))

    SETTINGS.endGroup()

    if verbose:
        Log(output, group, key)
    return content

def GetSettingsArray(group, arrayname, verbose=False):
    """
    Gets an array from the INI settings file.

    Args:
        groupname (str): The main group to get the values from.
        arrayname (str): The array name to get.
        verbose (bool, optional): If True, will display the contents from the INI file. Defaults to False.

    Returns:
        [list]: The array contents.
    """
    output_text = []
    content = []

    # Use class name or widget's object name?
    if isinstance(group, QtWidgets.QMainWindow): # ToolManager->QMainWindow
        group = group.objectName()
    elif isinstance(group, QtWidgets.QDockWidget): # ToolDockWidget->QDockWidget
        group = group.__class__.__name__

    # Check we have a value QSettings object
    if SETTINGS and isinstance(SETTINGS, QtCore.QSettings):
        output_text.append("- QSettings: {}".format(SETTINGS.fileName()))
        output_text.append("- Group: {}".format(group))
    else:
        output_text.append("- QSettings: {}".format(None if SETTINGS is None else "Invalid settings (not QSettings)."))
        return content

    SETTINGS.beginGroup(group)
    count = SETTINGS.beginReadArray(arrayname)
    output_text.append("- list count: {}".format(count))

    for i in range(count):
        SETTINGS.setArrayIndex(i)
        value = SETTINGS.value(arrayname)
        output_text.append("- {}: {}".format(i, value))
        content.append(value)

    SETTINGS.endArray()
    SETTINGS.endGroup()

    if verbose:
        Log(output_text, group)

    return content

def SetSettings(group, key, value, verbose=False):
    """
    SetSettings [summary]

    Args:
        group (str, ToolManager, ToolDockWidget): The group category to set the key/value pair under. If passed a ToolDockWidget, will use its class name, or the object name if a ToolManager.
        key (str, QWidget): The key to set the value of. If a QWidget, will default to using its objectName.
        value (str, int, float): The value to set the key to.
        verbose (bool, optional): [description]. Defaults to False.
    """
    output = []

    # Use class name or widget's object name?
    if isinstance(group, QtWidgets.QMainWindow): # ToolManager->QMainWindow
        group = group.objectName()
    elif isinstance(group, QtWidgets.QDockWidget): # ToolDockWidget->QDockWidget
        group = group.__class__.__name__

    if not SETTINGS or not isinstance(SETTINGS, QtCore.QSettings):
        output += ["- QSettings: {}".format(None if SETTINGS is None else "Invalid settings (not QSettings).")]
        Log(output, group, key, value)
        return

    SETTINGS.beginGroup(group)

    # Check what format the key is in
    if isinstance(key, str):
        SETTINGS.setValue(key, value)
        output += ["Key/Value: {} = {}".format(key, value)]
    elif isinstance(key, QtWidgets.QWidget):
        SETTINGS.setValue(key.objectName(), value)
        output += ["Key/Value: {} = {}".format(key.objectName(), value)]

    SETTINGS.endGroup()

    if verbose:
        Log(output, group, key, value)
#endregion Settings

# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
def GetModuleName(module_path):
    """
    Get the module filename from the file path.

    Args:
        module_path (string): File path of the module file. i.e. c:/somepath/application/modules/subfolder/module_name.py

    Returns:
        string: The Module filename portion minus extension. i.e. c:/somepath/application/modules/subfolder/module_name.py -> module_name
    """
    return os.path.splitext(os.path.basename(module_path))[0]

def GetRelativeModulePath(module_filepath):
    """
    Gets the path of the module, relative to the module_root path.
    i.e. if the module path is c:/users/username/company/app/modules/SomeCategory/SubCategory/module_name.py
    and the module root is c:/users/username/company/app/modules/
    it will return SomeCategory/SubCategory/module_name

    Args:
        module_filepath ([type]): [description]

    Returns:
        [type]: [description]
    """
    module_root = MODULE_PATH
    module_name = os.path.splitext(os.path.basename(module_filepath))[0]
    relative_path = os.path.dirname(module_filepath).replace(module_root, "")
    relative_module_path = os.path.join(relative_path, module_name).replace("\\", "/")
    return relative_module_path

def GetModulePaths(override_module_path=None, override_module_paths=None, verbose=False):
    """
    Gets a list of file paths to all the available modules in the folder.

    Args:
        override_module_path ([type], optional): If. Defaults to None.

    Returns:
        [type]: [description]
    """
    output_text = []
    module_paths = []

    # Get the root folders for where to look for modules
    module_root = override_module_path if override_module_path else MODULE_PATH
    output_text.append("- module_root: {}".format(module_root))

    if override_module_path:
        output_text.append("- override_module_path: {}".format(override_module_path))
    if override_module_paths:
        output_text.append("- override_module_paths: {}".format(override_module_paths))

    for module_root in MODULE_PATHS:
        # Walk the folder and subfolders
        for root, dirs, files in os.walk(module_root): #pylint: disable=unused-variable
            dirs[:] = [d for d in dirs if not d[0] in ['.', '_']] # filter out hidden or system folders
            for d in dirs:
                subpath = os.path.join(root, d).replace("\\", "/")
                output_text.append("- Sub Path: {}".format(subpath))

                for subroot, subdirs, subfiles in os.walk(subpath): #pylint: disable=unused-variable
                    subfiles = [f for f in subfiles if not (f[0] in ['.', '_'] or not os.path.splitext(f)[1].lower() == ".py")] # collect files if not hidden or not a .py file
                    for f in subfiles:
                        module_filepath = os.path.join(subroot, f) # get the abs path of the module
                        relative_path = os.path.dirname(module_filepath).replace(module_root, "") # relative path to the root

                        module_name = GetModuleName(module_filepath)
                        relative_module_path = GetRelativeModulePath(module_filepath)

                        output_text.append("\t- {}".format(module_filepath))
                        output_text.append("\t\t- Relative Module Path: {}, {} -> {}".format(relative_path, module_name, relative_module_path))

                        module_path = os.path.join(module_root, relative_module_path+".py")
                        output_text.append("\t\t- Recreated Path: {}".format(module_path))

                        module_filepath = module_filepath.replace("\\", "/").replace("//", "/") # Make path slashes uniform
                        if module_filepath not in module_paths: # avoid duplicates
                            module_paths.append(module_filepath)

    if verbose:
        Log(output_text)
    return module_paths

def LoadModule(module_path, verbose=False):
    """
    Loads the module specified at 'module_path', returns the loaded module or None if any failure.

    Args:
        module_path (string): The relative filepath of the module to load, will attempt to load based on the MODULE_ROOT path

    Returns:
        [module]: The module object if it was successfully loaded, otherwise any failure it returns None.
    """
    output_text = []
    module = None
    module_name = GetModuleName(module_path)

    # Load the module
    try:
        # Check which Python env we're working in
        if sys.version_info.major == 3 and sys.version_info.minor >= 5: # Python 3.5+
            spec = importlib.util.spec_from_file_location(module_name, module_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            output_text.append("3.5+: Loaded module: {}".format(module_name))

        elif sys.version_info.major == 3 and sys.version_info.minor >= 0: # Python 3.0+
            module = importlib.machinery.SourceFileLoader(module_name, module_path).load_module() #pylint: disable=deprecated-method,no-value-for-parameter
            output_text.append("<3.5: Loaded module: {}".format(module_name))

        elif sys.version_info.major == 2: # Python 2.x
            module = imp.load_source(module_name, module_path)
            # reload(module) #type: ignore #pylint: disable=undefined-variable
            output_text.append("2.x: Loaded module: {}".format(module_name))

    except Exception as e: #pylint: disable=broad-except
        import traceback
        output_text += ["[ ERROR ] {}:".format(e)]
        Log(output_text, module_name)
        traceback.print_exc()

    if verbose:
        Log(output_text, module_name)
    return module

def GetModuleWidget(parentWidget, module_path, verbose=False):
    """
    Gets the widget that loads the module.

    Args:
        parentWidget (ToolManager): The parent ToolManager window that this module will be a child of.
        module_path (str): The relative module path (MODULE_ROOT/modules/...)
        verbose (bool, optional): [description]. Defaults to False.

    Returns:
        [QWidget]: The module's widget, or None if failure.
    """
    output_text = []
    module = None
    module_widget = None

    # Check for a valid module path
    if not module_path:
        output_text += ["[ERROR] No module path supplied."]
        Log(output_text, parentWidget.objectName(), module_path)
        return None
    elif not module_path.lower().endswith(".py"): # Convert relative module path to absolute path
        module_path += ".py"
    if module_path.startswith("/"): # Strip the first /
        module_path = module_path[1:]

    # Get the absolute module path
    # Loops thru the module paths and finds the first instance of the module.py file, so it's a very good idea to avoid duplicate names and paths
    abs_module_filepath = None
    for m in MODULE_PATHS:
        if os.path.exists(sanitize_path(os.path.join(m, module_path))):
            abs_module_filepath = sanitize_path(os.path.join(m, module_path))
            output_text.append("- {}: {}, FOUND".format(module_path, abs_module_filepath))
            break

    # Load the module
    if abs_module_filepath:
        try:
            module = LoadModule(abs_module_filepath)
        except Exception as e: #pylint: disable=broad-except
            output_text.append("[ERROR] Error loading module: {}".format(e))

    # Get an instance of the module class as a QtWidget object
    if module:
        try:
            if hasattr(module, "get_widget"):
                module_widget = module.get_widget(parent=parentWidget)
        except Exception as e: #pylint: disable=broad-except
            output_text.append("[ERROR] Error getting widget: {}".format(e))

        # module_members = inspect.getmembers(module, inspect.isclass)
        # for m in module_members:
        #     #pylint: disable=unused-variable
        #     member_name = m[0]
        #     member_obj = m[1]

        #     output_text += ["{}: {}".format(member_name, member_obj)]

        #     # if issubclass(member_obj, ToolDockWidget) and member_obj != ToolDockWidget:
        #     #     module_widget = member_obj(parent=parentWidget, objectName=member_name)



    if module_widget:
        module_path = module_path.replace(".py", "")
        module_widget.SetModulePath(module_path)
        output_text.append("- Module Widget: {}".format(module_widget.objectName()))
    else:
        output_text.append("[ERROR] Unable to retrieve module widget object!")
        verbose = True

    if verbose:
        Log(output_text, parentWidget.objectName() if parentWidget else "None", module_path)

    return module_widget
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region 3dsMax temp
#
_gCachedQMaxWindow = None

def _PyCObjectToInt(obj):
    ctypes.pythonapi.PyCObject_AsVoidPtr.restype = ctypes.c_void_p
    ctypes.pythonapi.PyCObject_AsVoidPtr.argtypes = [ctypes.py_object]
    return ctypes.pythonapi.PyCObject_AsVoidPtr(obj)

def GetQMaxWindow():
    '''Get the 3ds Max internal QWidget window that could be used for parenting your QWidget.'''

    if _gCachedQMaxWindow:
        return _gCachedQMaxWindow

    maxHwnd = MaxPlus.QtHelpers.GetQMaxWindowWinId() #type: ignore #pylint: disable=undefined-variable
    for w in QtWidgets.QApplication.allWidgets():
        if _PyCObjectToInt(w.effectiveWinId()) == maxHwnd:
            globals()['_gCachedQMaxWindow'] = w
            break
    return _gCachedQMaxWindow
#endregion 3dsMax

# ----------------------------------------------------------------------------------------------------------------------
#region DCC
#
def QApplication():
    """
    Gets the QApplication

    Returns:
        QApplication: The QApplication object ()
    """
    #pylint: disable=global-statement
    global QAPP
    if QAPP is None:
        # Check platform
        QAPP = QtCompat.wrapInstance(QtCompat.getCppPointer(QtWidgets.QApplication.instance()), QtWidgets.QApplication)
    return QAPP

def GetDCCWindow(verbose=False):
    """
    Get the Digital Content Creator window handle (if operating under such an environment, i.e. 3dsmax or Maya).
    Also sets the DCCPLATFORM global for platform specific stuff.

    Returns:
        QMainWindow: The QMainWindow object of the application to parent under, if available.
    """
    output_text = []
    dcc_window = None

    #pylint: disable=global-statement
    global DCCPLATFORM
    DCCPLATFORM = None
    version = None

    # ----------------------------------------------------------------------------------------------------------------------
    #region Check DCC Platform
    #
    #region 3D Studio Max
    if not DCCPLATFORM:
        #pylint: disable=import-error,wildcard-import,wrong-import-position
        output_text.append("- Testing Max")
        try:
            #pylint: disable=import-outside-toplevel
            import MaxPlus #type: ignore
            from pymxs import runtime #type: ignore

            version = runtime.maxversion() # Returns an Array with nine items such as #(21000, 52, 0, 21, 2, 0, 2112, 2019, ".2 Update"), where the first three numbers are the 3ds Max release number, API version, and revision number of the SDK.
            version = version[0] # release
            version = (version / 1000) + 2008 - 10 # - 2 + 2000 #+1998? Convert to year number
            # version[1] # API version
            # version[2] # SDK revision

            if version >= 2020:
                # Shiboken
                main_window_qwdgt = QtWidgets.QWidget.find(runtime.windows.getMAXHWND())
                dcc_window = QtCompat.wrapInstance(QtCompat.getCppPointer(main_window_qwdgt), QtWidgets.QMainWindow)
                # output_text.append("- dcc_window:  {} -> {}".format(dcc_window, dcc_window.objectName()))
                # output_text.append("\t- Children: {}".format(len(dcc_window.children())))

                # MaxPlus.GetQMaxMainWindow
                # dcc_qwindow = MaxPlus.GetQMaxMainWindow()
                # output_text.append("- dcc_qwindow: {} -> {}".format(dcc_qwindow, dcc_qwindow.objectName()))
                # output_text.append("\t- Children: {}".format(len(dcc_qwindow.children())))
            elif version >= 2018:
                dcc_window = MaxPlus.GetQMaxMainWindow()
            elif version >= 2017:
                dcc_window = MaxPlus.GetQMaxWindow()
            else: # <= 2016
                dcc_window = None

            DCCPLATFORM = MAX
            if dcc_window:
                output_text.append("- dcc_window:  {} -> {}".format(dcc_window, dcc_window.objectName()))

        except ImportError as e:
            output_text.append("Unable to load MaxPlus: {}".format(e))
    #endregion 3D Studio Max

    #region Maya
    if not DCCPLATFORM:
        output_text.append("- Testing Maya")
        try:
            import pymel #type: ignore #pylint: disable=import-outside-toplevel
            DCCPLATFORM = MAYA
        except ImportError as e:
            output_text.append("Unable to load pymel: {}".format(e))
    #endregion Maya

    #region Blender
    if not DCCPLATFORM:
        output_text.append("- Testing Blender")
        try:
            DCCPLATFORM = BLENDER
        except ImportError as e:
            pass
    #endregion Blender

    #endregion Check DCC Platform

    output_text.append("- Final: {}, {}".format(DCCPLATFORM, version))

    if verbose:
        Log(output_text)

    return dcc_window
#endregion Qt and DCC

# ------------------------------------------------------------------------------
#region Functions - JSON
#
def LoadJson(filepath, template_config=None, verbose=False):
    """
    Loads the specified .JSON file, creates the file based on the contents of template if set.

    Args:
        filepath (str): Full filepath of the .JSON file to load.
        template (dictionary, optional): If the file is not found, create a new one using this as the template format. Defaults to None.

    Returns:
        dictionary: [description]
    """
    output = []
    config = {}

    if verbose:
        print("Config: {}".format(filepath))

    # ------------------------------------------------------------------------------
    # File not found at location, create
    if not os.path.exists(filepath):
        if template_config: # Template supplied, create it under a template_config header.
            if verbose:
                print("File not found, creating new template at: {}".format(filepath))

            # Make path if it doesn't exist
            if not os.path.exists(os.path.dirname(filepath)):
                os.makedirs(os.path.dirname(filepath))

            config = template_config
            with open(filepath, 'w') as f:
                json.dump(config, f, indent=4)
        else: # No template, skip
            if verbose:
                print("File not found, no template supplied.")
            config = template_config

    # ------------------------------------------------------------------------------
    # Load config
    else:
        try:
            with open(filepath) as f:
                config = json.load(f)
        except Exception as e: #pylint: disable=broad-except
            print("[ERROR] Error reading {}: {}".format(filepath, e))

    # ------------------------------------------------------------------------------
    # Display contents of read
    if verbose:
        output += ["Dumping {}:".format(filepath)]
        output += [json.dumps(config, indent=4, sort_keys=False)]
        print("\n".join(output))

    return config

def SaveJson(filepath, contents, indent=4, verbose=False):
    """
    Saves the dictionary contents as the specified file.

    Args:
        filepath (str): Full path to the target file to write to.
        contents (dict): The dictionary contents to write to file.
        indent (int, optional): JSON indent, use None for compact default. Defaults to 4 because we like being able to read it later.
        verbose (bool, optional): Show stuff. Defaults to False.

    Returns:
        [type]: [description]
    """
    output = []
    result = False

    # Create directory if not found
    if not os.path.exists(filepath):
        if verbose:
            print("File not found, creating new file at: {}".format(filepath))
        if not os.path.exists(os.path.dirname(filepath)):
            os.makedirs(os.path.dirname(filepath))

    # Save the JSON to file
    try:
        with open(filepath, 'w') as f:
            json.dump(contents, f, indent=indent)
            result = True
    except Exception as e: #pylint: disable=broad-except
        print("[ERROR] Saving to: {}, {}".format(filepath, e))

    if verbose:
        print("\n".join(output))

    return result
#endregion Functions - JSON


# ------------------------------------------------------------------------------
#region Main
#
if __name__ in ["__main__", "<module>"]:
    # Save
    template = {}
    template["count"] = "A"
    template["starttime"] = datetime.now().strftime(DEFAULTTIMESTAMPFORMAT)
    template["targetfolder"] = os.path.join(USERPROFILE_PATH, "Desktop", "AssetBrowser")

    # Load config file (create if not made)
    config_filepath = os.path.join(USERPROFILE_PATH, "Desktop", "AssetBrowser", "loopcounter.json")
    test_config = LoadJson(config_filepath, template_config=template, verbose=False)

    # Process Loop
    print("Loop Counter: {}".format(test_config["count"]))
    print("Start Time: {}".format(test_config["starttime"]))
    time.sleep(1)
    test_config["count"] = "J"
    if test_config["count"] == "J":
        elapsed_time = datetime.now() - datetime.strptime(test_config["starttime"], DEFAULTTIMESTAMPFORMAT)
        print("Loop Ended, took: {:02d}:{:02d}:{:02d}.{:02d}".format(int(elapsed_time.total_seconds()/3600), int((elapsed_time.total_seconds()/60)%60), int(elapsed_time.total_seconds()%60), int(elapsed_time.microseconds)))

        os.remove(config_filepath) # remove the file
        # sys.exit(1)
    else:
        test_config["count"] = chr(ord(test_config["count"])+1)
        print("Loop Counter++: {}".format(test_config["count"]))
        SaveJson(config_filepath, test_config)

# else:
#     print("__name__: {}".format(__name__))


#endregion Main
