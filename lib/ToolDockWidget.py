"""
QDockWidget Class for tool widgets to inherit from.
"""

# ----------------------------------------------------------------------------------------------------------------------
# Module Info
__description__ = "ToolDockWidget Template"
__version__ = 1.0
__author__ = 'Brian Chung'
__email__ = 'brian.chung@ubisoft.com'
__tags__ = [
]
#endregion Module Info

# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

# print("ToolDockWidget: {}".format(__name__))
if __name__ == "lib.ToolDockWidget":
    import lib.settings as settings
    from lib.DockTitleBar import DockTitleBar
    from lib.Log import Log
    from lib.Vector import Vector
else:
    import settings
    from DockTitleBar import DockTitleBar
    from Log import Log
    from Vector import Vector

# try:
#     import lib.settings as settings
#     from lib.Log import Log
#     from lib.Vector import Vector
#     from lib.DockTitleBar import DockTitleBar
# except ImportError:
#     import settings #type: ignore
#     from DockTitleBar import DockTitleBar #type: ignore
#endregion Modules


# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class ToolDockWidget(QtWidgets.QDockWidget):
    """
    QDockWidget for managing tools
    """
    VALID_QWIDGETS = [QtWidgets.QComboBox, QtWidgets.QFontComboBox, QtWidgets.QLineEdit, QtWidgets.QTextEdit, QtWidgets.QPlainTextEdit, QtWidgets.QSpinBox, QtWidgets.QDoubleSpinBox, QtWidgets.QTimeEdit, QtWidgets.QDateEdit, QtWidgets.QDateTimeEdit, QtWidgets.QSlider, QtWidgets.QScrollBar, QtWidgets.QDial, QtWidgets.QRadioButton, QtWidgets.QCheckBox]

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None, verbose=False):
        if sys.version_info[0] == 2:
            super(ToolDockWidget, self).__init__(parent=parent)
        else:
            super().__init__(parent=parent)

        output_text = []

        self.debug = True
        self.help_url = "http://intranet/dept/arttools/nameoftool"
        output_text += ["- URL: {}".format(self.help_url)]

        self.is_dragging = False
        self._module_path = None

        # UI
        self.setObjectName(objectName) # required to allow for window state save/restore
        self.setWindowTitle(objectName)
        output_text += ["- objectName: {}".format(self.objectName())]

        # QDockWidget Features - Disable the Close Window button (set this before applying the custom DockTitleBar)
        #     QtWidgets.QDockWidget.DockWidgetClosable
        #     QtWidgets.QDockWidget.DockWidgetMovable
        #     QtWidgets.QDockWidget.DockWidgetFloatable
        #     QtWidgets.QDockWidget.DockWidgetVerticalTitleBar
        #     QtWidgets.QDockWidget.AllDockWidgetFeatures
        #     QtWidgets.QDockWidget.NoDockWidgetFeatures
        self.setFeatures(QtWidgets.QDockWidget.DockWidgetMovable|QtWidgets.QDockWidget.DockWidgetFloatable)

        # Custom TitleBarWidget, for adding a dedicated Help button
        self.dock_titlebar_widget = None
        try:
            self.dock_titlebar_widget = DockTitleBar(self, titleName=objectName, features=DockTitleBar.EnableButtonDockable | DockTitleBar.EnableButtonHelp)
            self.setTitleBarWidget(self.dock_titlebar_widget) # Custom TitleBar
        except Exception as e: #pylint: disable=broad-except
            verbose = True
            output_text += ["[ERROR] Getting DockTitleBar: {}".format(e)]
            import traceback
            traceback.print_exc()

        self.widget = None
        # Copy the following into the derived class in a UI block and uncomment, has to be done
        # there to load and set the associated .UI file properly. Doing it from the inherited class won't work
        # self.load_ui(__file__)
        # self.setWidget(self.widget)

        # Connect events
        if self.widget:
            # self.widget.widget_element.event_name.connect(self.on_event)
            # self.widget.button_widget.clicked.connect(self.on_button_event)
            # self.widget.widget_element.clicked.connect(lambda: self.on_event_clicked(some_param))
            # self.widget.check_widget.clicked.connect(self.ToggleFields)
            pass

        # QDockWidget Events
        self.featuresChanged.connect(self.on_featuresChanged)
        # self.allowedAreasChanged.connect(self.on_allowedAreasChanged)
        # self.topLevelChanged.connect(self.on_topLevelChanged) # Floating/Not Floating
        # self.dockLocationChanged.connect(self.on_dockLocationChanged)
        # self.visibilityChanged.connect(self.on_visibilityChanged)

        # Window Drag Tracking
        self.installEventFilter(self)

        if isinstance(self.VALID_QWIDGETS, list):
            self.VALID_QWIDGETS = tuple(self.VALID_QWIDGETS)


        self.hover_widget = None

        if verbose:
            # output_text.append("File: {}".format(__file__))
            # output_text.append("Sys: {}".format(sys.modules[self.__module__].__file__))
            Log(output_text, parent.objectName() if parent else "None", objectName)

    def load_ui(self, class_file, verbose=False):
        """
        Loads the associted .UI file for the QWidget class. Assumes the originating class's /path/__file__.ui for ease of use.

        Arguments:
            class_file {string} -- [description]

        Returns:
            [QWidget] -- [description]
        """
        # https://doc.qt.io/qtforpython-5.12/PySide2/QtUiTools/QUiLoader.html
        output_text = []

        # Extract the name of the UI to load (based on the calling class' /path/filename.py -> /path/filename.ui)
        class_path = os.path.dirname(class_file)
        class_name = os.path.splitext(os.path.basename(class_file))[0]
        ui_filename = "{}.ui".format(class_name)
        ui_path = settings.sanitize_path(os.path.join(class_path, ui_filename)) #pylint: disable=no-member

        # output_text.append("- Module Name: {}, {}".format(__name__, __file__))
        # output_text.append("- Derived Class: {}".format(self.__class__.__name__))
        # output_text.append("- Class Name: {}, {}".format(class_name, class_path))
        # output_text.append("- UI filename: {}, {}".format(ui_filename, ui_path))

        # Loading UI file
        output_text.append("- Loading UI file: {}".format(ui_path))
        file = QtCore.QFile(ui_path)
        file.open(QtCore.QFile.ReadOnly)
        try:
            self.widget = QtCompat.loadUi(uifile=ui_path)
        except Exception as e: #pylint: disable=broad-except
            verbose = True
            output_text.append("[ERROR] Error loading .UI file: {}".format(e))

        file.close()

        # Update the tooltip on the titlebar Help button
        if self.dock_titlebar_widget:
            self.dock_titlebar_widget.updateHelpURL()

        if verbose:
            Log(output_text)

        return self.widget
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities
    #
    def GetAllChildrenWidgets(self, widget, widget_filter=None, verbose=False):
        """
        Gets all the children QWidgets for the specified parent widget. This is useful for getting the widgets for
        saving/restoring their field values.

        Arguments:
            widget {QWidget} -- The parent widget to get the children of.

        Returns:
            list -- A list of all the children QWidget items.
        """
        output_text = []
        widgets = []

        if widget_filter:
            if isinstance(widget_filter, list): # Convert to tuple, required for isinstance()
                widget_filter = tuple(widget_filter)

            for child in widget.children():
                if isinstance(child, widget_filter):
                    widgets.append(child)

                if len(widget.children()) > 0: # This widget has children
                    widgets.extend(self.GetAllChildrenWidgets(child, widget_filter=widget_filter))

        else: # No filter set
            for child in widget.children():
                widgets.append(child)

                if len(widget.children()) > 0: # This widget has children
                    widgets.extend(self.GetAllChildrenWidgets(child, widget_filter=widget_filter))

        if verbose:
            Log(output_text)

        return widgets

    def GetWidgets(self, pos, filter_classes=None):
        """
        Get all the QWidgets at the specified position.

        Args:
            pos {QPoint} -- The position to query.  Use QtGui.QCursor.pos() for where the mouse cursor is.
            filter_class ([QtWidgets], optional): List of QtWidget classes and only grab those that match. Defaults to None.

        Returns:
            list -- List of widgets at the coordinates, ordered by Z (top most to bottom)
        """

        widgets = []                # list of widgets fitting the class filter
        transparent_widgets = []    # all widgets that have been set transparent

        # Get the widgets at this screen position
        widget = QtWidgets.QApplication.widgetAt(pos)
        while widget:
            if filter_classes and isinstance(filter_classes, list): # Compare against the filter_classes and only grab the widgets that match the class(es) in there
                for c in filter_classes:
                    if isinstance(widget, c) and widget not in widgets:
                        widgets.append(widget)

            elif widget not in widgets: # no filter, grab everything
                widgets.append(widget)

            # Set this widget to be transparent for the next query, and store it for reverting later
            widget.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents)
            transparent_widgets.append(widget)
            widget = QtWidgets.QApplication.widgetAt(pos) #QtGui.qApp.widgetAt(QtGui.QCursor.pos()) # get the next widget under the previous one

        # Restore widget attributes so they can respond to mouse events again
        for widget in transparent_widgets:
            widget.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents, False)

        return widgets

    def GetWidgetsAt(self, position, filter_classes=None, verbose=False):
        """
        Returns all the widgets matching the filter_classes (if set) at the screen position.

        Args:
            position (QtCore.QPoint): The screen position (usually of the mouse cursor)
            filter_classes (list, tuple, optional): List of classes desired, anything not matching is skipped over. Defaults to None.
            verbose (bool, optional): Show output text. Defaults to False.

        Returns:
            list: List of objects, in order of top-most to bottom.
        """
        output = []
        widgets = []

        # Check inputs
        if not isinstance(position, QtCore.QPoint):
            output += ["Position is not a valid."]
            Log(output, position)
            return widgets
        if filter_classes:
            if not isinstance(filter_classes, (list, tuple)):
                output += ["Filter classes is not a valid list or tuple."]
                Log(output, position)
                return widgets
            elif isinstance(filter_classes, list):
                filter_classes = tuple(filter_classes)
            # else: # Already a tuple, skip
        if not self.parent():
            return widgets

        # Loop all ToolManager windows, their module widgets, and children widgets and check if the mouse pos is in their rect
        for window in self.parent().GetMasterParent().GetWindows():
            if window.geometry().contains(position):
                if filter_classes:
                    if isinstance(window, filter_classes):
                        output += ["- {} ({})".format(window.objectName(), window.__class__.__name__)]
                        widgets.append(window)
                else:
                    output += ["- {} ({})".format(window.objectName(), window.__class__.__name__)]
                    widgets.append(window)

            # Loop Module Widgets
            for widget in window.GetWidgets():
                if widget.rect().contains(widget.mapFromGlobal(position)):
                    if filter_classes:
                        if isinstance(widget, filter_classes):
                            output += ["\t- {} ({})".format(widget.objectName(), widget.__class__.__name__)]
                            widgets.append(widget)
                    else:
                        output += ["\t- {} ({})".format(widget.objectName(), widget.__class__.__name__)]
                        widgets.append(widget)

                # Loop widgets in Module
                for child in self.GetAllChildrenWidgets(widget):
                    try:
                        if isinstance(child, QtWidgets.QItemDelegate):
                            # This gets added when we set the stylesheet for the hover, but it breaks on the rect() call, so we skip over it.
                            output += ["\t+ {} ({}) <- !!!!!!!!".format(child.objectName(), child.__class__.__name__)]
                        elif hasattr(child, "rect") and child.rect().contains(child.mapFromGlobal(position)):
                            if filter_classes:
                                if isinstance(child, filter_classes):
                                    output += ["\t- {} ({})".format(child.objectName(), child.__class__.__name__)]
                                    widgets.append(child)
                            else:
                                output += ["\t- {} ({})".format(child.objectName(), child.__class__.__name__)]
                                widgets.append(child)
                    except TypeError as e:
                        verbose = True
                        output += ["[ERROR] {}".format(e)]
                        import traceback
                        traceback.print_exc()

        widgets.reverse()

        if verbose:
            position_text = "<{}, {}>".format(position.x(), position.y())
            Log(output, position_text)

        return widgets

    def GetModulePath(self):
        """
        Gets this module widget's relative module path

        Returns:
            str: The module path.
        """
        return self._module_path

    def SetModulePath(self, path):
        """
        Sets the module path for this tool dock widget.

        Args:
            path (str): The path to set, relative to the app's module path.
        """
        self._module_path = path
    #endregion Utilities

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #
    def eventFilter(self, source, event, verbose=False):
        """
        Filters events if this object has been installed as an event filter for the watched object.

        In your reimplementation of this function, if you want to filter the event out, i.e. stop it being handled further,
        return true; otherwise return false.

        https://doc.qt.io/qt-5/qobject.html#eventFilter
        https://doc.qt.io/qt-5/qevent.html
        https://doc.qt.io/qt-5/qevent.html#Type-enum

        Arguments:
            source {QWidget} -- [description]
            event {QEvent} -- [description]

        Returns:
            [type] -- [description]
        """
        output_text = []
        result = None

        # Mouse Button
        if event.type() == QtCore.QEvent.MouseButtonPress:
            self.OnMousePressed(event)
        elif event.type() == QtCore.QEvent.MouseButtonRelease:
            self.OnMouseReleased(event)

        # Window Focus, change the colour of the titlebar
        elif event.type() in [QtCore.QEvent.WindowActivate, QtCore.QEvent.WindowDeactivate]:
            if self.titleBarWidget() and hasattr(self.titleBarWidget(), "OnWidgetFocus"):
                output_text += ["WindowActivate" if event.type() == QtCore.QEvent.WindowActivate else "WindowDeactivate"]
                self.titleBarWidget().OnWidgetFocus(event)

        try:
            result = super(ToolDockWidget, self).eventFilter(source, event)
        except TypeError as e:
            verbose = True
            output_text += ["[ERROR] eventFilter: {}".format(e)]
            import traceback
            traceback.print_exc()

        if verbose:
            Log(output_text, source.__class__.__name__, event.Type(), result)
        return result

    def on_featuresChanged(self, features, verbose=True):
        """
        https://doc.qt.io/qt-5/qdockwidget.html#featuresChanged

        This is here for the ToolDockTitleBar to hook into, as it requires it for the titlebar widgets.

        Args:
            features (DockWidgetFeature): https://doc.qt.io/qt-5/qdockwidget.html#DockWidgetFeature-enum
                QtWidgets.QDockWidget.DockWidgetClosable
                QtWidgets.QDockWidget.DockWidgetMovable
                QtWidgets.QDockWidget.DockWidgetFloatable
                QtWidgets.QDockWidget.DockWidgetVerticalTitleBar
                QtWidgets.QDockWidget.AllDockWidgetFeatures
                QtWidgets.QDockWidget.NoDockWidgetFeatures
            verbose (bool, optional): [description]. Defaults to True.
        """
        output_text = []
        output_text += [self.GetFeatures()]
        # try:
        #     if features & QtWidgets.QDockWidget.DockWidgetClosable:
        #         output_text += ["+DockWidgetClosable"]
        #     if features & QtWidgets.QDockWidget.DockWidgetMovable:
        #         output_text += ["+DockWidgetMovable"]
        #     if features & QtWidgets.QDockWidget.DockWidgetFloatable:
        #         output_text += ["+DockWidgetFloatable"]
        #     if features & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
        #         output_text += ["+DockWidgetVerticalTitleBar"]
        #     # if features & QtWidgets.QDockWidget.AllDockWidgetFeatures:
        #     #     output_text += ["+AllDockWidgetFeatures"]
        #     if not features:
        #         output_text += ["+NoDockWidgetFeatures"]
        # super(ToolDockWidget, self).featuresChanged(features)
        # except Exception as e: #pylint: disable=broad-except
        #     output_text += ["[ERROR] on_featuresChanged {}".format(e)]
        #     verbose = True

        if verbose:
            Log(output_text, features)


    def GetFeatures(self):
        features = self.features()
        widget_features = []
        if features & QtWidgets.QDockWidget.DockWidgetClosable:
            widget_features += ["DockWidgetClosable"]
        if features & QtWidgets.QDockWidget.DockWidgetMovable:
            widget_features += ["DockWidgetMovable"]
        if features & QtWidgets.QDockWidget.DockWidgetFloatable:
            widget_features += ["DockWidgetFloatable"]
        if features & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
            widget_features += ["DockWidgetVerticalTitleBar"]
        if not features:
            widget_features += ["NoDockWidgetFeatures"]
        return widget_features

    def ToggleFields(self, verbose=False):
        """
        Enables or disables the associated spinner field based on the checkbox's state.
        """
        if self.widget:
            # self.widget.spin_field.setEnabled(self.widget.check_widget.isChecked())
            pass
        output = []

        if verbose:
            Log(output)

    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Settings
    #
    def parent(self):
        """Get the parent widget of this one."""
        return self.parentWidget()

    def SaveSettings(self, verbose=False):
        """
        Save the values of known QtWidgets to the current INI file.

        Args:
            verbose (bool, optional): [description]. Defaults to False.
        """
        #pylint: disable=no-member
        output_text = []

        if not settings.SETTINGS:
            output_text.append("No settings object!")
            Log(output_text)
            return

        settings.SETTINGS.beginGroup(self.__class__.__name__)
        for child in self.GetAllChildrenWidgets(self.widget, widget_filter=self.VALID_QWIDGETS):
            if isinstance(child, (QtWidgets.QComboBox, QtWidgets.QFontComboBox)):
                settings.SETTINGS.setValue(child.objectName(), child.currentIndex())
                settings.SETTINGS.setValue("{}_text".format(child.objectName()), child.currentText())

            # Text
            elif isinstance(child, QtWidgets.QLineEdit):
                settings.SETTINGS.setValue(child.objectName(), child.text())
            elif isinstance(child, QtWidgets.QTextEdit):
                settings.SETTINGS.setValue(child.objectName(), child.toHtml())
            elif isinstance(child, QtWidgets.QPlainTextEdit):
                settings.SETTINGS.setValue(child.objectName(), child.toPlainText())

            # SpinBox
            elif isinstance(child, QtWidgets.QSpinBox):
                settings.SETTINGS.setValue(child.objectName(), child.value())
            elif isinstance(child, QtWidgets.QDoubleSpinBox):
                settings.SETTINGS.setValue(child.objectName(), child.value())

            # DateTime
            elif isinstance(child, QtWidgets.QTimeEdit):
                settings.SETTINGS.setValue(child.objectName(), child.time())
            elif isinstance(child, QtWidgets.QDateEdit):
                settings.SETTINGS.setValue(child.objectName(), child.date())
            elif isinstance(child, QtWidgets.QDateTimeEdit):
                settings.SETTINGS.setValue(child.objectName(), child.dateTime())

            # Sliders
            elif isinstance(child, (QtWidgets.QSlider, QtWidgets.QScrollBar)):
                settings.SETTINGS.setValue(child.objectName(), child.value())
            elif isinstance(child, QtWidgets.QDial):
                settings.SETTINGS.setValue(child.objectName(), child.value())

            # Checkboxes
            elif isinstance(child, QtWidgets.QRadioButton):
                settings.SETTINGS.setValue(child.objectName(), child.isChecked())
            elif isinstance(child, QtWidgets.QCheckBox):
                value = 0
                if child.checkState() == QtCore.Qt.CheckState.Checked:
                    value = 1
                elif child.checkState() == QtCore.Qt.CheckState.PartiallyChecked:
                    value = 2
                settings.SETTINGS.setValue(child.objectName(), value)

        settings.SETTINGS.endGroup()

        if verbose:
            Log(output_text)

    def ReadSettings(self, verbose=False):
        """
        Loads the values of known QtWidgets from the current INI file.

        Args:
            verbose (bool, optional): [description]. Defaults to False.
        """
        #pylint: disable=no-member
        output_text = []
        error = False

        contents = settings.GetSettings(self.__class__.__name__)
        for child in self.GetAllChildrenWidgets(self.widget, widget_filter=self.VALID_QWIDGETS):
            if not isinstance(child, self.VALID_QWIDGETS):
                continue

            try:
                if child and child.objectName() != "" and child.objectName() in contents:
                    output_text.append("Widget: '{}' ({}): {}".format(child.objectName(), child.__class__.__name__, contents[child.objectName()]))
                else:
                    # error = True
                    # output_text.append("Widget: '{}' ({}): Key not found in contents".format(child.objectName(), child.__class__.__name__))
                    continue
            except Exception as e: #pylint: disable=broad-except
                error = True
                output_text.append("[ERROR] Widget: '{}' ({}): {}".format(child.objectName(), child.__class__.__name__, e))
                continue

            # ComboBox
            if isinstance(child, (QtWidgets.QComboBox, QtWidgets.QFontComboBox)):
                value = contents[child.objectName()]
                child.setCurrentIndex(int(value) if value else 0)

            # Text
            elif isinstance(child, QtWidgets.QLineEdit):
                child.setText(contents[child.objectName()])
            elif isinstance(child, QtWidgets.QTextEdit):
                child.setHtml(contents[child.objectName()])
            elif isinstance(child, QtWidgets.QPlainTextEdit):
                child.setPlainText(contents[child.objectName()])

            # SpinBox
            elif isinstance(child, QtWidgets.QSpinBox):
                value = contents[child.objectName()]
                child.setValue(int(value) if value else 0)
            elif isinstance(child, QtWidgets.QDoubleSpinBox):
                value = contents[child.objectName()]
                child.setValue(float(value) if value else 0)

            # DateTime
            elif isinstance(child, QtWidgets.QTimeEdit):
                child.setTime(contents[child.objectName()])
            elif isinstance(child, QtWidgets.QDateEdit):
                child.setDate(contents[child.objectName()])
            elif isinstance(child, QtWidgets.QDateTimeEdit):
                child.setDateTime(contents[child.objectName()])

            # Sliders
            elif isinstance(child, (QtWidgets.QSlider, QtWidgets.QScrollBar)):
                value = contents[child.objectName()]
                child.setValue(int(value) if value else 0)
            elif isinstance(child, QtWidgets.QDial):
                value = contents[child.objectName()]
                child.setValue(int(value) if value else 0)

            # Checkboxes
            elif isinstance(child, QtWidgets.QRadioButton):
                value = contents[child.objectName()]
                if isinstance(value, str):
                    value = value.lower()
                    child.setChecked(True if value == "true" else False)
                elif isinstance(value, bool):
                    child.setChecked(value)

            elif isinstance(child, QtWidgets.QCheckBox):
                value = contents[child.objectName()]
                if value:
                    value = int(value)
                    if value == 0:
                        child.setCheckState(QtCore.Qt.CheckState.Unchecked)
                    elif value == 1:
                        child.setCheckState(QtCore.Qt.CheckState.Checked)
                    elif value == 2:
                        child.setCheckState(QtCore.Qt.CheckState.PartiallyChecked)
                else:
                    value = 0

        if verbose or error:
            Log(output_text)

    def closeEvent(self, event, verbose=False):
        """closeEvent"""
        output_text = []

        output_text.append("Saving widget settings...")
        self.SaveSettings()

        if verbose:
            Log(output_text, event.Type())
    # endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Hover System
    #
    def GetHoverWidget(self, position, filter_classes=None, verbose=False):
        """
        Grab the QWidget this one is hovering over.

        Args:
            position (QPoint): The position of the screen to query for QWidgets.
            filter_classes (list, optional): List of object classes to get, will ignore objects not of those types. Defaults to None.
            verbose (bool, optional): Display the function process. Defaults to False.

        Returns:
            [type]: [description]
        """
        output_text = []

        # Check inputs
        if not isinstance(position, QtCore.QPoint):
            output_text += ["Position is not a valid "]
            Log(output_text, position, self.is_dragging)
            return None

        # Get all widgets, if any, under this screen position. Filter by classes as required.
        widgets = self.GetWidgets(position, filter_classes=filter_classes)
        output_text.append("- Widgets: {}".format(len(widgets)))

        top_widget = None
        for widget in widgets:
            if not top_widget and widget != self: # Store the next highest ToolDockWidget/ToolManager that isn't itself.
                top_widget = widget
            widget_class = widget.__class__.__name__
            widget_parent = widget.parent().__class__.__name__ if widget.parent() else "None"
            widget_parent_name = widget.parent().objectName() if widget.parent() else "None"
            if verbose:
                output_text.append("\t{} Name: {:25} Class: {:25} -> Parent: {} ({})".format("+" if widget == top_widget else "-", widget.objectName(), widget_class, widget_parent, widget_parent_name))
        widget = top_widget

        if verbose:
            position_text = "<{}, {}>".format(position.x(), position.y())
            Log(output_text, position_text, self.is_dragging)
        return top_widget

    def GetHoverWidgetAt(self, position, filter_classes=None, verbose=False):
        """
        Returns the top most widget matching the filter_classes (if set) at the screen position, ignoring the current one.

        Args:
            position (QtCore.QPoint): Screen position.
            filter_classes (list, tuple, optional): List of classes desired, anything not matching is skipped over. Defaults to None.
            verbose (bool, optional): Show output text. Defaults to False.

        Returns:
            QtWidgets.QWidget: The QWidget.
        """
        output = []
        widgets = []
        widget = None

        widgets = self.GetWidgetsAt(position, filter_classes=filter_classes)

        # Remove this widget from the list as it is the one doing the hovering.
        if self in widgets:
            widgets.remove(self)

        if widgets:
            widget = widgets[0] # first widget should be the top-most widget returned

        if verbose:
            position_text = "<{}, {}>".format(position.x(), position.y())

            tab = 0
            for w in widgets:
                output += ["{}- {}.{} ({})".format("\t" * tab, w.parentWidget().objectName() if w.parentWidget() else "", w.objectName(), w.__class__.__name__)]
                tab += 1

            if widget:
                output += ["- Widget: {} ({})".format(widget.objectName(), widget.__class__.__name__)]

            Log(output, position_text)
        return widget

    def OnMoveHover(self, verbose=False):
        """
        OnMoveHover [summary]

        Args:
            verbose (bool, optional): [description]. Defaults to False.
        """
        output = []

        if self.is_dragging:
            # Get the current mouse cursor position
            pos = Vector(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y())
            widget = self.GetHoverWidgetAt(QtGui.QCursor.pos(), filter_classes=[ToolDockWidget, self.parent().__class__])

            if widget:
                if isinstance(widget, ToolDockWidget): # is ToolDockWidget
                    if widget.parent() != self.parent(): # Over a ToolDockWidget of another parent window
                        # verbose = True
                        output += ["- {} ({})".format(widget.objectName(), widget.__class__.__name__)]

                        if self.hover_widget != widget: # Widget differs from stored widget, update
                            self.hover_widget = widget
                        # else: # Same widget, do nothing

                    else: # Same Parent Window
                        if widget.isTopLevel(): # The target is floating, should highlight
                            if self.hover_widget != widget: # Widget differs from stored widget, update
                                self.hover_widget = widget
                        elif self.hover_widget: # Over a ToolDockWidget of same parent window, not floating, revert existing highlight
                            self.hover_widget = None

                elif widget != self.parent(): # is ToolManager, but not its own parent window
                    # verbose = True
                    output += ["+ {} ({})".format(widget.objectName(), widget.__class__.__name__)]
                    self.hover_widget = widget

                elif self.hover_widget: # is ToolManager, parent window, if has stored hover_widget, revert
                    self.hover_widget = None

            elif self.hover_widget: # No Widget, if has stored hover_widget, revert
                self.hover_widget = None

            # Apply the hover overlay widget (or hide if not set)
            self.parent().GetMasterParent().SetHoverOverlay(self.hover_widget)

            # Set this window back to being on top
            self.raise_()

            if verbose:
                Log(output, pos, self.is_dragging)
    # endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events - Drag n Drop
    #
    def OnMousePressed(self, event, verbose=False):
        """OnMousePressed"""
        output_text = []

        pos = Vector(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y())
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.is_dragging = True

        if verbose:
            Log(output_text, pos, event.button(), self.is_dragging)

    def OnMouseReleased(self, event, verbose=False):
        """
        When this widget has stopped being dragged, check what other tool widgets it is over and merge them.

        Args:
            event ([type]): [description]
            verbose (bool, optional): [description]. Defaults to False.
        """
        output_text = []

        pos = Vector(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y())

        if event.button() == QtCore.Qt.MouseButton.LeftButton and self.is_dragging:
            # verbose = True
            output_text.append("- {}: {}".format(self.objectName(), "Floating" if self.isFloating() else "Docked"))

            widget = self.GetHoverWidget(QtGui.QCursor.pos(), filter_classes=[ToolDockWidget, self.parent().__class__])

            # Check state
            output_text += ["- Source: {}.{}: {}".format(self.parent().objectName(), self.objectName(), "Floating" if self.isTopLevel() else "Docked")]
            if widget and widget.parent():
                output_text += ["- Target: {}.{}: {}".format(widget.parent().objectName(), widget.objectName(), "Floating" if widget.isTopLevel() else "Docked")]
            elif widget:
                output_text += ["- Target: {}: {}".format(widget.objectName(), "Floating" if widget.isTopLevel() else "Docked")]
            else:
                output_text += ["- Target: None"]

            # komplikated part:
            # Over a ToolManager
            # - Reparent if not the same parent
            # - Do nothing if same parent
            # Over a ToolDockWidget:
            # - Same parent
            #   - Both Floating - redock
            #   - target docked, do nothing, regular dockwidget behaviour
            # - Different Parent
            #   - Reparent
            if isinstance(widget, self.parent().__class__): # is over a ToolManager class
                if self.parent() == widget: # same parent, do nothing - default QMainWindow/QDockWidget behaviour
                    output_text += ["- ToolManager, same parent - do nothing"]
                else: # different parent, reparent to that one, dock over ToolDockWidget
                    output_text += ["- ToolManager, different parent - reparent"]
                    # Should not occur however, as it means it didn't detect a ToolDockWidget or empty ToolManager
                    # (unless an empty master ToolMananger window)
                    # But right now, can happen with the main ToolManager window
                    try:
                        self.parent().MoveWidget(self, widget, dockarea=None, dockwidget=None, verbose=False)
                    except TypeError as e:
                        output_text += ["[ERROR] {}".format(e)]
                        Log(output_text, pos, event.button(), self.is_dragging)
                        import traceback
                        traceback.print_exc()

            elif isinstance(widget, ToolDockWidget): # is over a ToolDockWidget class
                if self.parent() == widget.parent():
                    if self.isTopLevel() and widget.isTopLevel(): # same parent, both floating, merge widgets into new window
                        # Use the window geometry of the bottom widget for the new window
                        output_text += ["- ToolDockWidget, same parent, both floating - Make new window"]
                        self.parent().CreateWindow(position=widget.pos(), size=widget.size(), widgets=[widget, self])

                    elif not widget.isTopLevel(): # same parent, target is docked, do nothing - default QMainWindow/QDockWidget behaviour
                        output_text += ["- ToolDockWidget, same parent, not floating - do nothing"]

                else: # Different parent
                    if widget.isTopLevel(): # Both floating, merge widgets into new window
                        # Use the window geometry of the bottom widget for the new window
                        output_text += ["- ToolDockWidget, different parent, floating - reparent"]
                        self.parent().CreateWindow(position=widget.pos(), size=widget.size(), widgets=[widget, self])

                    else: # Target docked, merge into that window
                        output_text += ["- ToolDockWidget, different parent, docked - parent to this ToolManager"]
                        dockarea = widget.parent().dockWidgetArea(widget)
                        self.parent().MoveWidget(self, widget.parent(), dockarea=dockarea, dockwidget=widget, verbose=False)

            else: # is over anything else not part of the ToolManager, do nothing
                output_text += ["- Not over a recognized widget, skipping"]

        self.is_dragging = False

        if self.hover_widget: # Revert highlight if any
            self.hover_widget = None
            self.parent().GetMasterParent().SetHoverOverlay(self.hover_widget)

        if verbose:
            Log(output_text, pos, event.button(), self.is_dragging)

    def moveEvent(self, event): #pylint: disable=unused-argument
        """moveEvent()"""
        if self.is_dragging:
            self.OnMoveHover()
    # endregion

#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    """Creates the ToolDockWidget and returns it."""
    widget = ToolDockWidget(parent=parent, objectName="ToolDockWidget")
    return widget
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region Main
#

def main():
    """This is for testing the look of the ToolDockWidget (and DockTitleBar)"""
    qapp = QtWidgets.QApplication.instance()
    standalone = False
    if qapp is None: # App is not running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)
        standalone = True
    else: # is running under a DCC, get the window
        dcc_window = settings.GetDCCWindow() #pylint: disable=unused-variable,no-member

    window = ToolDockWidget(None, "Tool Dock Widget", "http://www.google.com/")

    # Set window to center of screen
    screen = window.screen()
    center = screen.geometry().center()
    size = screen.availableGeometry().size() * 0.3
    window.move(center.x() - (size.width()/2), center.y() - (size.height()/2))
    window.resize(size)

    window.show()

    if standalone:
        sys.exit(qapp.exec_())

if __name__ in ["__main__", "<module>"]:
    main()
    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")
#endregion Main
