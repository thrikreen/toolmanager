"""
Custom TitleBar for QDockWidgets
"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import webbrowser

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

try:
    import lib.settings as settings
    from lib.Log import Log
    from lib.Vector import Vector
except Exception as e: #pylint: disable=broad-except
    import settings #type: ignore
    from Log import Log #type: ignore
    from Vector import Vector #type: ignore

#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Classes
#
class DockTitleBar(QtWidgets.QWidget):
    """
    Custom DockTitleBar for the QDockWidget
    https://stackoverflow.com/questions/40839899/pyqt-how-to-edit-the-title-of-qdockwidget-which-is-tabifyed-in-mainwindow

    Adds the help button on the titlebar.

    Args:
        parent (QWidget): Parent ToolDockWidget(QWidget)
        titleName (str): Name of widget to display
    """

    EnableButtonCloseable = 0x01
    EnableButtonMinMax = 0x02
    EnableButtonDockable = 0x04
    EnableButtonHelp = 0x08
    EnableAllButtons = EnableButtonCloseable | EnableButtonMinMax | EnableButtonDockable | EnableButtonHelp
    DisableAllButtons = 0x00

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent, titleName=None, features=EnableAllButtons, verbose=False):
        super(DockTitleBar, self).__init__(parent=parent)

        output = []
        self.features = features

        # Load QWidget .UI
        self.load_ui(__file__, verbose=verbose) # here for the template, should be enabled in the derivation class.
        self.init_UI(verbose=verbose)

        # Parent Widget
        output += ["- parent: {}".format(self.parent().objectName() if self.parent() else "None")]
        if self.parent():
            self.setObjectName("{}_DockTitleBar".format(self.parent().objectName()))

            # Parent QDockWidget Features hook
            # Connect to the parent QWidget's featuresChanged() event so we can update the DockTitleBar as needed
            output += ["- Connecting to {}.featuresChanged() event.".format(self.parent().objectName())]
            self.parent().featuresChanged.connect(self.on_featuresChanged)

            output += ["- Parent Features:"]
            if self.parent().features() & QtWidgets.QDockWidget.DockWidgetClosable:
                output += ["\t- DockWidgetClosable"]
            if self.parent().features() & QtWidgets.QDockWidget.DockWidgetMovable:
                output += ["\t- DockWidgetMovable"]
            if self.parent().features() & QtWidgets.QDockWidget.DockWidgetFloatable:
                output += ["\t- DockWidgetFloatable"]
            if self.parent().features() & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
                output += ["\t- DockWidgetVerticalTitleBar"]
            if self.parent().features() & QtWidgets.QDockWidget.AllDockWidgetFeatures:
                output += ["\t- AllDockWidgetFeatures"]
            if self.parent().features() & QtWidgets.QDockWidget.NoDockWidgetFeatures:
                output += ["\t- NoDockWidgetFeatures"]

            self.on_featuresChanged(self.parent().features()) # push changes from the parent to the DockTitleBar widget's

        # Titlebar Text
        self.setTitle(titleName)
        self.setWindowTitle(titleName)
        output += ["- Title: {}".format(self.windowTitle())]

        if verbose:
            Log(output, parent.objectName() if parent else "None", titleName)

    def init_UI(self, verbose=False):
        """
        Initializes the DockWidget TitleBar
        """
        output = []

        # iconSize = QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_TitleBarNormalButton).actualSize(QtCore.QSize(100, 100))
        # buttonSize = 32 #iconSize #+ QtCore.QSize(4, 4)
        # output += ["- Button Size: {}".format(buttonSize)]

        if hasattr(self, "titleLabel"):
            output += ["- Widget loaded from .UI"]

        else: # No .UI layout, create titlebar widget from code
            output += ["- No Widget, creating UI from code"]

            # Title Label
            self.titleLabel = QtWidgets.QLabel(self)
            self.titleLabel.setIndent(5)

            self.closeButton = QtWidgets.QToolButton(self)
            self.closeButton.setAutoRaise(True)
            self.closeButton.setToolTip("Close")

            self.minButton = QtWidgets.QToolButton(self)
            self.minButton.setAutoRaise(True)
            self.minButton.setToolTip("Minimize")

            self.maxButton = QtWidgets.QToolButton(self)
            self.maxButton.setAutoRaise(True)
            self.maxButton.setToolTip("Maximize")

            self.dockButton = QtWidgets.QToolButton(self)
            self.dockButton.setAutoRaise(True)
            self.dockButton.setToolTip("Dock")

            self.helpButton = QtWidgets.QToolButton(self)
            self.helpButton.setAutoRaise(True)

            # Layout
            boxLayout = QtWidgets.QHBoxLayout(self)
            boxLayout.setContentsMargins(0, 0, 0, 0)
            boxLayout.setSpacing(0)
            boxLayout.addWidget(self.titleLabel)
            boxLayout.addWidget(self.helpButton)
            boxLayout.addWidget(self.dockButton)
            boxLayout.addWidget(self.minButton)
            boxLayout.addWidget(self.maxButton)
            boxLayout.addWidget(self.closeButton)

            gridLayout = QtWidgets.QGridLayout(self)
            gridLayout.setSpacing(0)
            gridLayout.setContentsMargins(0, 0, 0, 0)
            gridLayout.addLayout(boxLayout, 0, 0)

            # Activate the widget
            self.setLayout(gridLayout)

        # Set style and connect events
        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setAutoFillBackground(True)
        self.setStyleSheet("background-color: palette(highlight)")
        # self.setStyleSheet("font-weight: bold")

        # Set button icons: https://doc.qt.io/qt-5/qstyle.html#StandardPixmap-enum
        self.helpButton.setIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_TitleBarContextHelpButton))
        self.dockButton.setIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_TitleBarNormalButton))
        self.minButton.setIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_TitleBarMinButton))
        self.maxButton.setIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_TitleBarMaxButton))
        self.closeButton.setIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_DockWidgetCloseButton))

        # Update help button tooltip
        self.updateHelpURL()

        # Connect Events
        self.helpButton.clicked.connect(self.help)
        self.dockButton.clicked.connect(self.toggleFloating)
        self.minButton.clicked.connect(self.on_minimize)
        self.maxButton.clicked.connect(self.on_maximize)
        self.closeButton.clicked.connect(self.closeParent)

        self.closeButton.setVisible(self.features & DockTitleBar.EnableButtonCloseable)
        self.minButton.setVisible(self.features & DockTitleBar.EnableButtonMinMax)
        self.maxButton.setVisible(self.features & DockTitleBar.EnableButtonMinMax)
        self.dockButton.setVisible(self.features & DockTitleBar.EnableButtonDockable)
        self.helpButton.setVisible(self.features & DockTitleBar.EnableButtonHelp)

        output_features = ""
        if self.features & DockTitleBar.EnableButtonCloseable:
            output_features += "EnableButtonCloseable"
        if self.features & DockTitleBar.EnableButtonMinMax:
            output_features += "EnableButtonMinMax"
        if self.features & DockTitleBar.EnableButtonDockable:
            output_features += "EnableButtonDockable"
        if self.features & DockTitleBar.EnableButtonHelp:
            output_features += "EnableButtonHelp"
        output += ["Features: {}".format(output_features)]

        if verbose:
            Log(output)

    def load_ui(self, class_file, verbose=False):
        """
        Loads the associated .UI file for the QWidget class. Assumes the originating class's /path/__file__.ui for ease of use.

        Arguments:
            class_file {string} -- [description]

        Returns:
            [QWidget] -- [description]
        """
        # https://doc.qt.io/qtforpython-5.12/PySide2/QtUiTools/QUiLoader.html
        output_text = []

        # Extract the name of the UI to load (based on the calling class' /path/filename.py -> /path/filename.ui)
        class_path = os.path.dirname(class_file)
        class_name = os.path.splitext(os.path.basename(class_file))[0]
        ui_filename = "{}.ui".format(class_name)
        ui_path = os.path.join(class_path, ui_filename)

        output_text.append("- Module Filename: {}, {}".format(__name__, __file__))
        output_text.append("- Derived Class: {}".format(self.__class__.__name__))
        output_text.append("- Class Name, Path: {}, {}".format(class_name, class_path))
        output_text.append("- .UI Filename, Path: {}, {}".format(ui_filename, ui_path))

        # Loading UI file
        output_text.append("- Loading .UI file: {}".format(ui_path))
        result = False
        try:
            file = QtCore.QFile(ui_path)
            file.open(QtCore.QFile.ReadOnly)
            QtCompat.loadUi(ui_path, self)
            file.close()
            output_text.append("- Loaded .UI file")
            result = True
        except Exception as e: #pylint: disable=broad-except
            output_text += ["[ERROR] Loading .UI file: {}".format(e)]
            verbose = True

        if verbose:
            Log(output_text, class_file)

        return result
    #endregion Init

    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities
    #
    def parent(self):
        """Gets the parent widget of this one."""
        return self.parentWidget()

    def setTitle(self, title):
        """
        Updates the titlebar QLabel object to reflect the parent object's

        Args:
            title (str): [description]
        """
        self.titleLabel.setText(title)

    def updateHelpURL(self):
        """
        Updates the help button tooltip.

        This is due to the titlebar being created before setting the help URL in the ToolDockWidget.
        """
        self.helpButton.setToolTip("Get help for this tool:\n-> {}".format(self.parent().help_url))
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Events - UI
    #
    # Titlebar Buttons
    def on_featuresChanged(self, features, verbose=False):
        """
        Event when features are changed on the widget. This should hook into the parent ToolDockWidget's
        featuresChanged() event too.

        Args:
            features (DockWidgetFeature): https://doc.qt.io/qt-5/qdockwidget.html#DockWidgetFeature-enum
                QtWidgets.QDockWidget.DockWidgetClosable
                QtWidgets.QDockWidget.DockWidgetMovable
                QtWidgets.QDockWidget.DockWidgetFloatable
                QtWidgets.QDockWidget.DockWidgetVerticalTitleBar
                QtWidgets.QDockWidget.AllDockWidgetFeatures
                QtWidgets.QDockWidget.NoDockWidgetFeatures

        Raises:
            ValueError: [description]
        """
        output_text = []

        if features & QtWidgets.QDockWidget.DockWidgetClosable:
            output_text += ["\t+DockWidgetClosable"]
        if features & QtWidgets.QDockWidget.DockWidgetMovable:
            output_text += ["\t+DockWidgetMovable"]
        if features & QtWidgets.QDockWidget.DockWidgetFloatable:
            output_text += ["\t+DockWidgetFloatable"]
        if features & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
            output_text += ["\t+DockWidgetVerticalTitleBar"]
        if not features:
            output_text += ["\t+NoDockWidgetFeatures"]

        # Close Button
        self.closeButton.setVisible(features & QtWidgets.QDockWidget.DockWidgetClosable)

        # Dock/Float Button
        self.dockButton.setVisible(features & QtWidgets.QDockWidget.DockWidgetFloatable)

        if verbose:
            Log(output_text, features)

    def GetFeatures(self):
        features = self.features()
        widget_features = []
        if features & QtWidgets.QDockWidget.DockWidgetClosable:
            widget_features += ["DockWidgetClosable"]
        if features & QtWidgets.QDockWidget.DockWidgetMovable:
            widget_features += ["DockWidgetMovable"]
        if features & QtWidgets.QDockWidget.DockWidgetFloatable:
            widget_features += ["DockWidgetFloatable"]
        if features & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
            widget_features += ["DockWidgetVerticalTitleBar"]
        if not features:
            widget_features += ["NoDockWidgetFeatures"]
        return widget_features

    def help(self):
        """
        Opens the tool widget's help URL.
        """
        webbrowser.open(self.parent().help_url)

    def toggleFloating(self):
        """
        Toggles the floating state of the parent QDockWidget.
        """
        if self.parent():
            self.parent().setFloating(not self.parent().isFloating())

    def on_minimize(self):
        """
        Toggles the minimize state of the parent QDockWidget.
        """
        output = []
        Log(output)

    def on_maximize(self):
        """
        Toggles the maximize state of the parent QDockWidget.
        """
        output = []
        Log(output)

    def closeParent(self):
        """
        Closes the parent QDockWidget.
        """
        self.parent().toggleViewAction().setChecked(False)
        self.parent().hide()
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region Events - These need to exist
    #
    def OnWidgetFocus(self, event):
        """
        When the parent ToolDockWidget has focus, chenges the titlebar colour from inactive grey to the active OS colour theme.

        Args:
            event (QEvent): [description]
        """
        # Use palette() to match the OS colour theme
        if event.type() == QtCore.QEvent.WindowActivate and self.parent().isTopLevel(): # Floating and in focus
            self.setStyleSheet("background-color: palette(highlight)")
        else: # Docked or not in focus
            self.setStyleSheet("background-color: palette(dark)")
    #endregion
#endregion Classes

# ----------------------------------------------------------------------------------------------------------------------
#region Main
#
def main():
    """
    This is for testing the look of the DockTitleBar.
    """
    qapp = QtWidgets.QApplication.instance()
    if qapp is None: # App is running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)
        standalone = True

    # print("QStyleFactory: {}".format(QtWidgets.QStyleFactory.keys())) # ['windowsvista', 'Windows', 'Fusion']
    # qapp.setStyle("Fusion")

    # features = DockTitleBar.EnableAllButtons
    # features = DockTitleBar.DisableAllButtons
    # features = DockTitleBar.EnableButtonCloseable | DockTitleBar.EnableButtonDockable | DockTitleBar.EnableButtonHelp
    features = DockTitleBar.EnableButtonDockable | DockTitleBar.EnableButtonHelp
    # features = DockTitleBar.EnableButtonMinMax
    window = DockTitleBar(None, "Dock Title Bar Test", features=features, verbose=True)
    # window.on_featuresChanged()

    window.show()

    if standalone:
        sys.exit(qapp.exec_())

if __name__ in ["__main__", "<module>"]:
    main()
    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")
#endregion Main
