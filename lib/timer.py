"""timer.py"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
import random
import time
from datetime import datetime
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Global
#
TIMERS = {}
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region Functions
#
# start_time = datetime.now()
# elapsed_time = datetime.now() - start_time
# display_time = "{:02d}:{:02d}:{:02d}".format(elapsed_time.seconds/3600, (elapsed_time.seconds/60)%60, elapsed_time.seconds%60)

def start(key):
    """
    Starts a timer of the current time with an identifier of 'key'.

    Args:
        key (str): Label key of this timer to use. Will overwrite an existing key.
    """
    TIMERS[key] = datetime.now()
    return TIMERS[key]

def end(key):
    """
    Gets the elapsed time for the timer 'key' (if found).

    Args:
        key (str): Label key of this timer to use.

    Returns:
        [timedelta]: The elapsed time from the start time, in seconds, if found. Returns None if that timer key was not found.
    """
    if key in TIMERS:
        return datetime.now() - TIMERS[key]
    return None

def clear(key):
    """
    Removes this timer key from the global list.

    Args:
        key (str): [description]
    """
    if key in TIMERS:
        del TIMERS[key]

def displaytime(key, show_ms=False):
    """
    Displays the elapsed time for the timer key, if found.

    Args:
        key (str): The timer key to retrieve, if found.
        show_ms (bool, optional): Display milliseconds or not. Defaults to False.

    Returns:
        [str]: The elapsed time in the format of HH:MM:SS.ms if the timer key was found. Otherwise returns None.
    """
    display_time = None
    if key in TIMERS:
        elapsed_time = datetime.now() - TIMERS[key]
        display_time = "{:02d}:{:02d}:{:02d}".format(int(elapsed_time.total_seconds()/3600), int((elapsed_time.total_seconds()/60)%60), int(elapsed_time.total_seconds()%60))
        if show_ms:
            display_time += ".{:02d}".format(int(elapsed_time.microseconds))

    return display_time

#endregion Functions

# ----------------------------------------------------------------------------------------------------------------------
#region Main
#
def main():
    """main"""
    mytimer = "mytimer"
    timerange = 5

    start_time = start(mytimer)
    time.sleep(random.randrange(1, timerange))
    elapsed_time = end(mytimer)

    print("Elapsed Time: {} -> {}".format(start_time, elapsed_time))
    print("convertdisplaytime(): {}".format(convertdisplaytime(elapsed_time)))

    time.sleep(random.randrange(1, timerange))
    print("displaytime(): {}".format(displaytime(mytimer)))

    clear(mytimer)
    print("clear(): {}".format(displaytime(mytimer)))

if __name__ in ["__main__"]:
    main()

#endregion Main
