"""
Custom TitleBar for QDockWidgets
"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat #pylint: disable=no-name-in-module

try:
    from lib.Log import Log
    from lib.Vector import Vector
except Exception as e: #pylint: disable=broad-except
    from Log import Log #type: ignore
    from Vector import Vector #type: ignore
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Classes
#
class DockOverlay(QtWidgets.QMainWindow):
    """Frameless transparent overlay for applying for hover effects."""

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, geometry=None, verbose=False):
        super(DockOverlay, self).__init__(parent=parent)

        output = []

        self.widget = None
        self.init_UI()

        if geometry:
            self.setGeometry(geometry)

        if verbose:
            Log(output, parent.objectName() if parent else "None")

    def init_UI(self, verbose=False):
        """
        Initializes the DockWidget TitleBar
        """
        output = []

        self.use_ui = False
        if self.use_ui:
            self.load_ui(__file__)
        else:
            # Body
            self.body = QtWidgets.QWidget(self)
            # Need to figure out how to extract the actual highlight RGBA value from Qt
            # qapp.style().standardPalette().highlight() -> (ARGB 1, 0, 0.470588, 0.843137) -> 0, 120, 215
            self.body.setStyleSheet("background-color: rgba(0, 120, 215, 32);")

            # Border
            self.border = QtWidgets.QWidget(self.body)
            self.border.setStyleSheet("""
                border-style: solid;
                border-width: 1px;
                border-color: rgb(0, 100, 179);
            """)

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setWindowFlag(QtCore.Qt.Tool)
        # self.setWindowOpacity(0.5)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        if verbose:
            Log(output)

    def load_ui(self, class_file, verbose=False):
        """
        Loads the associated .UI file for the QWidget class. Assumes the originating class's /path/__file__.ui for ease of use.

        Arguments:
            class_file {string} -- [description]

        Returns:
            [QWidget] -- [description]
        """
        # https://doc.qt.io/qtforpython-5.12/PySide2/QtUiTools/QUiLoader.html
        output_text = []

        # Extract the name of the UI to load (based on the calling class' /path/filename.py -> /path/filename.ui)
        class_path = os.path.dirname(class_file)
        class_name = os.path.splitext(os.path.basename(class_file))[0]
        ui_filename = "{}.ui".format(class_name)
        ui_path = os.path.join(class_path, ui_filename)

        output_text.append("- Module Filename: {}, {}".format(__name__, __file__))
        output_text.append("- Derived Class: {}".format(self.__class__.__name__))
        output_text.append("- Class Name, Path: {}, {}".format(class_name, class_path))
        output_text.append("- .UI Filename, Path: {}, {}".format(ui_filename, ui_path))

        # Loading UI file
        output_text.append("- Loading .UI file: {}".format(ui_path))
        result = False
        try:
            file = QtCore.QFile(ui_path)
            file.open(QtCore.QFile.ReadOnly)
            QtCompat.loadUi(ui_path, self)
            file.close()
            output_text.append("- Loaded .UI file")
            result = True
        except Exception as e: #pylint: disable=broad-except
            output_text += ["[ERROR] Loading .UI file: {}".format(e)]
            verbose = True

        if verbose:
            Log(output_text, class_file)

        return result
    #endregion Init

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #
    def resizeEvent(self, event):
        """resizeEvent()"""
        super(DockOverlay, self).resizeEvent(event)
        self.body.setGeometry(0, 0, event.size().width(), event.size().height())
        self.border.setGeometry(0, 0, event.size().width(), event.size().height())

    def mousePressEvent(self, event):
        """mousePressEvent()"""
        super(DockOverlay, self).mousePressEvent(event)
        self.close()
    #endregion UI Events

#endregion Classes

# ----------------------------------------------------------------------------------------------------------------------
#region
#
def DisplayPalette():
    """
    Display some info from the specified palette:

    qapp.style().standardPalette():
    - alternateBase       : #e9e7e3  -> rgba(233, 231, 227, 255)
    - background          : #f0f0f0  -> rgba(240, 240, 240, 255)
    - base                : #ffffff  -> rgba(255, 255, 255, 255)
    - brightText          : #ffffff  -> rgba(255, 255, 255, 255)
    - button              : #f0f0f0  -> rgba(240, 240, 240, 255)
    - buttonText          : #000000  -> rgba(0, 0, 0, 255)
    - dark                : #a0a0a0  -> rgba(160, 160, 160, 255)
    - foreground          : #000000  -> rgba(0, 0, 0, 255)
    - highlight           : #0078d7  -> rgba(0, 120, 215, 255)
    - highlightedText     : #ffffff  -> rgba(255, 255, 255, 255)
    - light               : #ffffff  -> rgba(255, 255, 255, 255)
    - link                : #0000ff  -> rgba(0, 0, 255, 255)
    - linkVisited         : #ff00ff  -> rgba(255, 0, 255, 255)
    - mid                 : #a0a0a0  -> rgba(160, 160, 160, 255)
    - midlight            : #e3e3e3  -> rgba(227, 227, 227, 255)
    - placeholderText     : #000000  -> rgba(0, 0, 0, 128)
    - shadow              : #696969  -> rgba(105, 105, 105, 255)
    - text                : #000000  -> rgba(0, 0, 0, 255)
    - toolTipBase         : #ffffdc  -> rgba(255, 255, 220, 255)
    - toolTipText         : #000000  -> rgba(0, 0, 0, 255)
    - window              : #f0f0f0  -> rgba(240, 240, 240, 255)
    - windowText          : #000000  -> rgba(0, 0, 0, 255)
    """

    output = []
    # qapp.style().standardPalette().highlight() -> (ARGB 1, 0, 0.470588, 0.843137) -> 0, 120, 215
    obj = QtWidgets.QApplication(sys.argv).style().standardPalette()
    for a in [a for a in dir(obj) if not a.startswith('__')]:
        attr = getattr(obj, a)
        attr_name = "{}".format(attr)
        if "ColorRole" in attr_name:
            pass
        elif "ColorGroup" in attr_name:
            pass
        elif "QPalette" in attr_name:
            try:
                c = None
                c = eval("obj.{}().color()".format(a)) # pylint: disable=eval-used
                r = c.red()
                g = c.green()
                b = c.blue()
                alpha = c.alpha()
                output += ["- {:20}: {:8} -> rgba({}, {}, {}, {})".format(a, c.name(), r, g, b, alpha)]
            except TypeError:
                pass
            except AttributeError:
                pass
    Log(output)
#endregion


# ----------------------------------------------------------------------------------------------------------------------
#region Main
#
def main():
    """
    This is for testing the look of the DockOverlay.
    """
    qapp = QtWidgets.QApplication.instance()
    if qapp is None: # App is running under another app environment, i.e. 3dsMax or Maya
        qapp = QtWidgets.QApplication(sys.argv)
        standalone = True

    window = DockOverlay()

    # Set window to center of screen
    screen = window.screen()
    center = screen.geometry().center()
    size = screen.availableGeometry().size() * 0.3
    window.move(center.x() - (size.width()/2), center.y() - (size.height()/2))
    window.resize(size)

    window.show()

    # Debug:
    DisplayPalette()

    if standalone:
        sys.exit(qapp.exec_())

if __name__ in ["__main__", "<module>"]:
    main()
    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")
#endregion Main
