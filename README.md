# ToolManager

Python-based tool manager for use in various supported 3D modeling applications. Presents a unified UI and delivery system, where the user can customize the type of tools loaded based on their discipline and preferences.

Adds some features to extend the behaviour of the default QMainWindow and QDockWidgets system for usability.

ToolManager:
- Automatically saves and restores settings
    - Prefs default to being stored in the user folder
    - Avoids a common problem where a tool's prefs are saved in the same path as the tool's files, and if it happens to be in your project's version controlled path, it gets reverted on every project update.
- Can redirect the modules location to point to a version controlled path instead, if you want to control what is used i.e. project/company specific
    - To Do: Add support to append to the module paths instead of just an override.
- To Do: Add in a way to report back to a central server for metrics (how often a tool is used, which functions, errors, etc.)

Tool Modules:
- Users can customize and select which tool modules to load
    - i.e. Environment artists probably don't need animation tools and vice versa, so why bother with always loading everything up?
- Allows for rapid tool creation:
    - Tool modules can be set to automatically use a QtDesigner .UI file of the same filename.
    - That way, developers can focus more on the core tool code itself instead of fiddling with the UI
    - But can just bypass that and design the UI by hand
- Each tool widget incorporates a help button on the titlebar
    - On click, opens to a URL set by the tool developer
    - Often happens, where one inherits maintenance of a tool, with no way to find documentation on it, so make the UI default to showing this, so it forces the dev to at least put in a stub page somewhere.
- Automatically saves and restores most input fields, cutting down on having to code it yourself.
- Should be app agnostic
    - Aside from Python, Qt/PySide, and Qt.py, it should not rely on any other external libraries
    - You can check for which environment you're running under, so when a user hits the GO button, check and run the appropriate code as needed (MaxScript, MEL, Python, etc.).
    - If there are project specific libraries to use, it can use them as needed.

- Modules themselves:
    - Manage Modules (wishlist)
        - Modules should have a version and search tags attribute, to allow for searching and updates
            - or filter out, like if a tool doesn't support a particular platform yet
        - To Do: Eventually expand to check a repo, making it easy to push updated versions to everyone.


Currently developing with/Tested on (as of 2021/03/25):
- Visual Studio Code
    - Python 3.6.8
    - PySide2 5.14.1
    - Qt.py 1.3.3 - https://github.com/mottosso/Qt.py
- 3d Studio Max 2020
    - Python 2.7
    - PySide2
